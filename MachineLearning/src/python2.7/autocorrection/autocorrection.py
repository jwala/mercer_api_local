from flask import Flask, render_template, request
from flaskext.mysql import MySQL
from flask_cache import Cache
APP = Flask(__name__)
import sys

reload(sys)
sys.setdefaultencoding('utf8')
import os
import json
import pandas
import numpy as np
import re
from datetime import datetime, timedelta
from pandas.io.json import json_normalize
pandas.options.mode.chained_assignment = None
import string
from fuzzywuzzy import fuzz, process
from pathos.multiprocessing import ProcessingPool
import dill
import logging
from logging.handlers import RotatingFileHandler

if not os.path.exists("./JsonLogging/"):
    os.makedirs("./JsonLogging/")

CACHE = Cache(APP, config={'CACHE_TYPE': 'simple'})

API_HOME = os.getenv('API_HOME')
CONFIG_FILE = '{}/conf/config.properties'.format(API_HOME)
with open(CONFIG_FILE) as f:
    CONFIG = f.read().split('\n')
    for item in CONFIG:
        if item.startswith('MERCER_DB_ADDRESS'):
            MERCER_DB_ADDRESS = item
        if item.startswith('MERCER_DB_NAME'):
            MERCER_DB_NAME = item
        if item.startswith('MERCER_DB_USERNAME'):
            MERCER_DB_USERNAME = item
        if item.startswith('MERCER_DB_PASSWORD'):
            MERCER_DB_PASSWORD = item
        if item.startswith('MERCER_DB_HOST_PORT'):
            MERCER_DB_HOST_PORT = item

MYSQL = MySQL()
APP = Flask(__name__)
APP.config['MYSQL_DATABASE_USER'] = MERCER_DB_USERNAME.split(
    "=")[-1].replace('\r', '')
APP.config['MYSQL_DATABASE_PASSWORD'] = MERCER_DB_PASSWORD.split(
    "=")[-1].replace('\r', '')
APP.config['MYSQL_DATABASE_DB'] = MERCER_DB_NAME.split(
    "=")[-1].replace('\r', '')
APP.config['MYSQL_DATABASE_HOST'] = MERCER_DB_ADDRESS.split(
    "=")[-1].replace('\r', '')
APP.config['MYSQL_DATABASE_PORT'] = int(
    MERCER_DB_HOST_PORT.split("=")[-1].replace('\r', ''))
MYSQL.init_app(APP)


@CACHE.cached(timeout=3600, key_prefix='Yes_No_Update')
def get_yes_no_update():
    APP.logger.info('Info Yes no Cache Update')
    cursor = MYSQL.connect().cursor()
    cursor.execute("SELECT * from yes_no_lookup")
    yes_no_lookup = pandas.DataFrame(
        list(cursor.fetchall()), columns=map(lambda x: x[0], cursor.description))
    return yes_no_lookup


@CACHE.cached(timeout=3600, key_prefix='Garbage_Update')
def get_garbage_update():
    APP.logger.info('Info Garbage Cache Update')
    cursor = MYSQL.connect().cursor()
    cursor.execute("SELECT * from garbage_lookup")
    garbage_lookup = pandas.DataFrame(
        list(cursor.fetchall()), columns=map(lambda x: x[0], cursor.description))
    return garbage_lookup


@CACHE.cached(timeout=3600, key_prefix='Region Update')
def get_region_update():
    APP.logger.info('Info Region Cache Update')
    cursor = MYSQL.connect().cursor()
    cursor.execute("SELECT * from region_lookup")
    region_lookup = pandas.DataFrame(
        list(cursor.fetchall()), columns=map(lambda x: x[0], cursor.description))
    return region_lookup


@APP.route('/', methods=['GET', 'POST'])
def hello():
    return "AUTOCORRECTION OK"


@APP.route('/upload', methods=['POST'])
def upload():
    try:
        if request.method == 'POST':
            tflag1 = datetime.now()
            import pdb
            # pdb.set_trace()
            incoming_json = request.get_json()
            incoming_json1 = json.dumps(incoming_json)
            incoming_json1 = incoming_json1.replace(': "",', ': "@#$it is an empty String@#$",').replace('\n',
                                                                                                         '').replace(
                '\r', '').encode('utf-8')
            mjson_raw = json.loads(incoming_json1)

            section_structure = json_normalize(mjson_raw)['sectionStructure.columns']

            section_structure_list = filter(None, section_structure.tolist())
            section_structure2 = json_normalize(section_structure_list[0])

            def json_flatten(option):
                try:
                    if isinstance(option, list):
                        return_value = {'items': option}
                    else:
                        return_value = option
                    return return_value
                except Exception as exception:
                    APP.logger.warning('Warning json_flatten failed')
                    APP.logger.warning(exception)
                    return option

            if section_structure2.columns.tolist().count('options.items') > 0:
                section_structure2['options.items'] = section_structure2.apply(
                    lambda x: json_flatten(x['options.items']), axis=1)
                section_structure2 = section_structure2.rename(
                    columns={'options.items': 'options'})

            incoming_data = mjson_raw['data']
            incoming_datalist = filter(None, incoming_data)
            incoming_data = json_normalize(incoming_datalist)
            incoming_data = incoming_data.astype(str)
            incoming_data = incoming_data.replace('None', '')
            incoming_data = incoming_data.replace('nan', '')
            APP.logger.info(incoming_data.shape)
            APP.logger.info('Info' + str(datetime.now() - tflag1) +
                            'Incoming Json read')

            question_master = pandas.DataFrame(
                columns=['code', 'dataType', 'questionType'])
            question_master_temp = section_structure2[[
                'code', 'dataType', 'questionType']]
            question_master = question_master.append(question_master_temp)
            question_master = question_master.dropna(axis=0, how='all')

            APP.logger.info('Info' + str(datetime.now() - tflag1) +
                            'Column categorization Checkpoint1')

            if section_structure2.columns.tolist().count('options') > 0:
                drop_down1 = section_structure2[(section_structure2.questionType == 'dropdown') | (
                            section_structure2.questionType == 'radio_buttons') | (
                                                            section_structure2.questionType == 'checkboxes')][
                    ['code', 'options', 'dataType', 'questionType']]

            else:
                drop_down1 = section_structure2[['code', 'dataType', 'questionType']]
                drop_down1['options'] = 'nondd'

            APP.logger.info('Info' + str(datetime.now() - tflag1) +
                            'Column categorization Checkpoint2')

            if drop_down1.shape[0] > 0:
                drop_down1['DD_Status'] = drop_down1.apply(
                    lambda x: dd_status_flag(x['options']), axis=1)
                drop_down_master = dd_label_value1(
                    drop_down1[drop_down1.options != 'nondd'])

            else:
                drop_down_master = pandas.DataFrame(
                    columns=[
                        'code',
                        'DD_Flag1',
                        'value',
                        'displayName',
                        'dataType',
                        'questionType'])

            APP.logger.info('Info' + str(datetime.now() - tflag1) +
                            'Column categorization Checkpoint3')

            question_master['Flag1'] = question_master.apply(lambda x: flag_1(
                x['code'], x['dataType'], x['questionType'], drop_down_master), axis=1)

            question_master['Flag2'] = question_master.apply(
                lambda x: flag_2(
                    x['code'],
                    x['Flag1'],
                    drop_down_master),
                axis=1)

            APP.logger.info('Info' + str(datetime.now() - tflag1) +
                            'Column categorization Checkpoint4')

            dd_large_columns = sorted(
                question_master[question_master.Flag2 == 'DD_Large']['code'].unique().tolist())
            dd_medium_columns = sorted(
                question_master[question_master.Flag2 == 'DD_Medium']['code'].unique().tolist())
            dd_small_columns = sorted(
                question_master[question_master.Flag2 == 'DD_Small']['code'].unique().tolist())
            dd_yes_no_columns = sorted(
                question_master[question_master.Flag2 == 'DD_Yes_No']['code'].unique().tolist())
            dd_region_columns = sorted(
                question_master[question_master.Flag2 == 'AC_Region']['code'].unique().tolist())

            dd_large_columns = list(
                set(dd_large_columns) & set(
                    incoming_data.columns))
            dd_medium_columns = list(
                set(dd_medium_columns) & set(
                    incoming_data.columns))
            dd_small_columns = list(
                set(dd_small_columns) & set(
                    incoming_data.columns))
            dd_yes_no_columns = list(
                set(dd_yes_no_columns) & set(
                    incoming_data.columns))
            dd_region_columns = list(
                set(dd_region_columns) & set(
                    incoming_data.columns))

            APP.logger.info('Info' + str(datetime.now() - tflag1) +
                            'Column Categorization')

            yes_no_lookup = get_yes_no_update()
            yes_no_dict = yes_no_lookup[['MATCH', 'REPLACEMENT']].set_index('MATCH')[
                'REPLACEMENT'].to_dict()

            garbage_lookup = get_garbage_update()
            garbage_dict = garbage_lookup[['MATCH', 'REPLACEMENT']].set_index('MATCH')[
                'REPLACEMENT'].to_dict()

            region_lookup = get_region_update()

            # Get Country Code from Contrext Data
            countryCode = list()

            if 'ctryCode' in mjson_raw['contextData']:
                countryCode.append(mjson_raw['contextData']['ctryCode'])

            APP.logger.info('Info', "countryCode : " , countryCode)

            # Filtering df based on Country Code
            if (countryCode.__len__() > 0):
                region_lookup_filtered = region_lookup[(region_lookup['CTRY_CODE'].isin(countryCode))]

            region_dict = region_lookup_filtered[['MATCH', 'REPLACEMENT']].set_index('MATCH')[
                'REPLACEMENT'].to_dict()

            #Encoding to Unicode
            region_dict = {k.decode('latin1'): v.decode('latin1') for k, v in region_dict.items()}

            APP.logger.info('Info', "Region Size : ", region_dict.__len__())

            incoming_cols = incoming_data.columns
            incoming_cols_flags = [x + '_ACflag' for x in incoming_cols]
            incoming_cols_replaces = [x + '_Replace' for x in incoming_cols]
            dd_large_columns_flags = [x + '_ACflag' for x in dd_large_columns]
            dd_large_columns_replaces = [x + '_Replace' for x in dd_large_columns]
            dd_medium_columns_flags = [x + '_ACflag' for x in dd_medium_columns]
            dd_medium_columns_replaces = [x + '_Replace' for x in dd_medium_columns]
            dd_small_columns_flags = [x + '_ACflag' for x in dd_small_columns]
            dd_small_columns_replaces = [x + '_Replace' for x in dd_small_columns]
            dd_yes_no_columns_flags = [x + '_ACflag' for x in dd_yes_no_columns]
            dd_yes_no_columns_replaces = [x + '_Replace' for x in dd_yes_no_columns]
            dd_region_columns_flags = [x + '_ACflag' for x in dd_region_columns]
            dd_region_columns_replaces = [x + '_Replace' for x in dd_region_columns]

            all_dd_columns = dd_large_columns + dd_medium_columns + \
                             dd_small_columns + dd_yes_no_columns + dd_region_columns
            all_dd_columns_flags = dd_large_columns_flags + dd_medium_columns_flags + \
                                   dd_small_columns_flags + dd_yes_no_columns_flags + dd_region_columns_flags
            all_dd_columns_replaces = dd_large_columns_replaces + dd_medium_columns_replaces + \
                                      dd_small_columns_replaces + dd_yes_no_columns_replaces + dd_region_columns_replaces

            all_non_dd_columns = list(set(incoming_cols) - set(all_dd_columns))
            all_non_dd_columns_flags = [x + '_ACflag' for x in all_non_dd_columns]
            all_non_dd_columns_replaces = [x + '_Replace' for x in all_non_dd_columns]

            for cols in dd_large_columns + dd_medium_columns + \
                        dd_small_columns + dd_yes_no_columns + dd_region_columns:
                incoming_data[cols] = incoming_data[cols].str.strip()

            APP.logger.info('Info' + str(datetime.now() - tflag1) +
                            'Drop Down space stripper')

            incoming_data[incoming_cols] = incoming_data[incoming_cols].replace(garbage_dict)

            if len(dd_yes_no_columns) > 0:
                processing_df = incoming_data[dd_yes_no_columns]
                non_processing_df = incoming_data.drop(
                    dd_yes_no_columns, axis=1)
                processing_df[dd_yes_no_columns] = processing_df[dd_yes_no_columns].replace(
                    yes_no_dict)
                incoming_data = pandas.concat(
                    [processing_df, non_processing_df], axis=1)

            incoming_data_replace = incoming_data.copy()
            incoming_data_replace.columns = [x + '_Replace' for x in incoming_data_replace.columns]
            incoming_data_acflag = pandas.DataFrame(
                'O', index=incoming_data.index, columns=map(
                    lambda x: x + '_ACflag', incoming_cols))
            incoming_data3 = pandas.concat(
                [incoming_data, incoming_data_replace, incoming_data_acflag], axis=1)

            def dd_mp(incoming_data2):
                if len(dd_yes_no_columns) > 0:
                    for col in dd_yes_no_columns:
                        ref_dict = drop_down_master[drop_down_master.code == col][[
                            'displayName', 'value']].set_index('displayName')['value'].to_dict()
                        ref_dict = {
                            str(k).lower(): v for k,
                                                  v in ref_dict.items()}
                        ref_data1 = ref_dict
                        incoming_data2[''.join([col, "_Replace"])] = incoming_data2.apply(
                            lambda x: field_resolver_yes_no(
                                x[''.join([col, "_Replace"])], ref_data1, x[''.join([col, "_ACflag"])]), axis=1)
                        incoming_data2[''.join([col, "_ACflag"])] = incoming_data2.apply(lambda x: layer_flag_update_v1(
                            x[''.join([col, "_Replace"])], x[''.join([col, "_ACflag"])]), axis=1)
                        incoming_data2[''.join([col, "_Replace"])] = incoming_data2.apply(
                            lambda x: field_flag_chopper(x[''.join([col, "_Replace"])]), axis=1)

                if len(dd_small_columns + dd_medium_columns) > 0:
                    for col in dd_small_columns + dd_medium_columns:
                        ref_dict = drop_down_master[drop_down_master.code == col][[
                            'displayName', 'value']].set_index('displayName')['value'].to_dict()
                        ref_dict = {
                            unicode(str(k).lower()): v for k,
                                                           v in ref_dict.items()}
                        ref_dict1 = {
                            k.replace(" ", ""): v for k,
                                                      v in ref_dict.items()}
                        ref_dict1.update(ref_dict)
                        ref_data1 = ref_dict1
                        incoming_data2[''.join([col, "_Replace"])] = incoming_data2.apply(
                            lambda x: field_resolver_dict_based(x[''.join([col, "_Replace"])], ref_data1), axis=1)
                        incoming_data2[''.join([col, "_ACflag"])] = incoming_data2.apply(lambda x: layer_flag_update_v1(
                            x[''.join([col, "_Replace"])], x[''.join([col, "_ACflag"])]), axis=1)
                        incoming_data2[''.join([col, "_Replace"])] = incoming_data2.apply(
                            lambda x: field_flag_chopper(x[''.join([col, "_Replace"])]), axis=1)

                if len(dd_region_columns) > 0:
                    for col in dd_region_columns:
                        ref_dict_drop_down = drop_down_master[drop_down_master.code == col][[
                            'displayName', 'value']].set_index('displayName')['value'].to_dict()
                        ref_dict_drop_down = {
                            unicode(str(k).lower()): v for k,
                                                           v in ref_dict_drop_down.items()}
                        ref_data_drop_down = ref_dict_drop_down

                        ref_dict_lookup = region_dict
                        ref_dict_lookup = {
                            unicode(str(k).lower()): v for k,
                                                  v in ref_dict_lookup.items()}
                        ref_data_lookup = ref_dict_lookup

                        incoming_data2[''.join([col, "_Replace"])] = incoming_data2.apply(
                            lambda x: field_resolver_region(x[''.join([col, "_Replace"])], ref_data_drop_down, ref_data_lookup), axis=1)
                        incoming_data2[''.join([col, "_ACflag"])] = incoming_data2.apply(lambda x: layer_flag_update_v1(
                            x[''.join([col, "_Replace"])], x[''.join([col, "_ACflag"])]), axis=1)
                        incoming_data2[''.join([col, "_Replace"])] = incoming_data2.apply(
                            lambda x: field_flag_chopper(x[''.join([col, "_Replace"])]), axis=1)

                if len(dd_large_columns) > 0:
                    for col in dd_large_columns:
                        ref_dict = drop_down_master[drop_down_master.code == col][[
                            'displayName', 'value']].set_index('displayName')['value'].to_dict()
                        ref_dict = {
                            unicode(str(k).lower()): v for k,
                                                           v in ref_dict.items()}
                        ref_data1 = ref_dict
                        incoming_data2[''.join([col, "_Replace"])] = incoming_data2.apply(
                            lambda x: field_resolver_large_dd(x[''.join([col, "_Replace"])], ref_data1), axis=1)
                        incoming_data2[''.join([col, "_ACflag"])] = incoming_data2.apply(lambda x: layer_flag_update_v1(
                            x[''.join([col, "_Replace"])], x[''.join([col, "_ACflag"])]), axis=1)
                        incoming_data2[''.join([col, "_Replace"])] = incoming_data2.apply(
                            lambda x: field_flag_chopper(x[''.join([col, "_Replace"])]), axis=1)

                garbage_processing_df1 = incoming_data2[all_dd_columns + \
                                                        all_dd_columns_flags + all_dd_columns_replaces]
                garbage_processing_df2 = incoming_data2[all_non_dd_columns + \
                                                        all_non_dd_columns_flags + all_non_dd_columns_replaces]
                garbage_processing_df1[all_dd_columns_replaces] = garbage_processing_df1[
                    all_dd_columns_replaces].replace(
                    garbage_dict)
                incoming_data2 = pandas.concat(
                    [garbage_processing_df1, garbage_processing_df2], axis=1)

                for col in incoming_cols:
                    incoming_data2[col][incoming_data2[''.join(
                        [col, "_ACflag"])] == 'O'] = incoming_data2[''.join([col, "_Replace"])]

                return incoming_data2

            df_size = len(incoming_data3)
            if df_size < 25:
                num_partitions = 1
            elif df_size < 50:
                num_partitions = 4
            else:
                num_partitions = 8

            df_split = np.array_split(incoming_data3, num_partitions)
            incoming_data = pandas.concat(POOL.map(dd_mp, df_split))

            APP.logger.info(
                'Info' + str(datetime.now() - tflag1) + 'DD Corrections')
            b = datetime.now()
            APP.logger.info('Info' + str(datetime.now() - tflag1) + "ML SUCCEED")
            print 'ML SUCCEED'
            incoming_data = incoming_data.replace('', np.nan)
            APP.logger.info(incoming_data.shape)
            return (incoming_data[sorted(incoming_data.columns)].to_json(orient='records').replace(
                ':"@#$it is an empty String@#$",', ': "",'))

    except Exception as exception:
        APP.logger.error('Error ML_Failed')
        APP.logger.error(exception)
        text_file = open(
            "./JsonLogging/Incoming" +
            datetime.now().strftime('%Y%m%d%H%M%S%f') +
            ".json",
            "w")
        text_file.write(incoming_json1)
        text_file.close()
        print 'ML FAILED'
        return 'ML FAILED'


# Import Fucntions
def dd_label_value1(drop_down1):
    try:
        drop_down_structure1 = pandas.DataFrame(
            columns=[
                'code',
                'DD_Flag1',
                'value',
                'displayName',
                'dataType',
                'questionType'])
        counter = 0
        for index, row in drop_down1.iterrows():
            if row['options'].keys()[0] == 'items':
                df1 = pandas.DataFrame(row['options'].values()[0])
                df1['code'] = row['code']
                df1['DD_Flag1'] = row['DD_Status']
                df1['dataType'] = row['dataType']
                df1['questionType'] = row['questionType']
                drop_down_structure1 = drop_down_structure1.append(df1)
                counter = counter + 1
            else:
                temp = pandas.DataFrame([[drop_down1.iloc[counter].code,
                                          drop_down1.iloc[counter].DD_Status,
                                          '',
                                          '',
                                          drop_down1.iloc[counter].dataType,
                                          drop_down1.iloc[counter].questionType]],
                                        columns=['code',
                                                 'DD_Flag1',
                                                 'value',
                                                 'displayName',
                                                 'dataType',
                                                 'questionType'])
                drop_down_structure1 = drop_down_structure1.append(temp)
                counter = counter + 1
        return drop_down_structure1

    except Exception as exception:
        APP.logger.warning('Warning DD_Label_Value Exception')
        APP.logger.warning(exception)


def flag_1(code, data_type, question_type, drop_down_master):
    try:
        if (data_type == 'string') & (question_type == 'text'):
            return_value = 'Freetext'
        elif (data_type == 'string') & (
                (question_type == 'dropdown') | (question_type == 'radio_buttons') | (question_type == 'checkboxes')):
            temp_list = drop_down_master[drop_down_master.code == code]['DD_Flag1'].unique()
            if len(temp_list) > 0:
                return_value = temp_list[0]
            else:
                return_value = 'false'
        elif (data_type == 'double') & (question_type == 'double'):
            return_value = 'double'
        elif (data_type == 'double') & (question_type == 'percentage'):
            return_value = 'percentage'
        elif (data_type == 'int') & (question_type == 'integer'):
            return_value = 'integer'
        elif (data_type == 'date') & (question_type == 'date'):
            return_value = 'date'
        return return_value
    except Exception as exception:
        APP.logger.warning('Warning' + code + 'exception Flag_1')
        APP.logger.warning(exception)
        return 'False'


def flag_2(code, flag1, drop_down_master):
    try:
        if flag1 not in 'Local':
            return 'False'
        dd_value_list = sorted(
            drop_down_master[drop_down_master.code == code]['value'].unique().tolist())
        dd_value_list_len = len(
            drop_down_master[drop_down_master.code == code]['value'].unique().tolist())
        if (code == 'EMP_047'):
            return 'AC_Region'
        if ('Y' in dd_value_list) & ('N' in dd_value_list) & (dd_value_list_len == 3):
            return 'DD_Yes_No'
        if dd_value_list_len < 18:
            return 'DD_Small'
        if dd_value_list_len < 50:
            return 'DD_Medium'
        return 'DD_Large'
    except Exception as exception:
        APP.logger.warning('Warning' + code + 'exception Flag 2')
        APP.logger.warning(exception)
        return 'DD_Others'


def dd_status_flag(options):
    try:
        if str(options) == 'nondd':
            return 'False'
        elif str(options) == 'nan':
            return 'False'
        elif str(options).find('To be aligned with MJL') > 0:
            return 'MJL'
        else:
            return 'Local'
    except Exception as exception:
        APP.logger.warning('Warning DD_Status_Flag')
        APP.logger.warning(exception)
        return 'False'


def is_abbrevation(abbrev, text):
    try:
        abbrev = abbrev.lower()
        text = text.lower()
        words = text.split()
        if not abbrev:
            return True
        elif abbrev and not text:
            return False
        elif abbrev[0] != text[0]:
            return False
        else:
            return (is_abbrevation(abbrev[1:], ' '.join(words[1:])) or
                    any(is_abbrevation(abbrev[1:], text[i + 1:])
                        for i in range(len(words[0]))))
    except Exception as exception:
        APP.logger.warning('Warning is abbrev')
        APP.logger.warning(exception)
        return abbrev


def field_resolver_dict_based(field_replace, dict_str):
    try:
        dictt = dict_str
        threshold1 = 50
        threshold2 = 95
        dd_display_names = dictt.keys()
        dd_value = dictt.values()
        if field_replace is None or field_replace == "" or field_replace == "@#$it is an empty String@#$":
            return ''.join(["%O%", ""])
        field_replace = field_replace.strip()
        if field_replace.lower() in (m.lower() for m in dd_value):
            return ''.join(["%O%", str(field_replace)])
        if field_replace.lower() in dd_display_names:
            return ''.join(["%O%", dictt[str(field_replace.lower())]])
        if field_replace.isdigit():
            return ''.join(["%X%", str(field_replace)])
        dd_string = (' '.join(dictt.keys()))
        tokens_incumbentdata = field_replace.encode('utf-8').split(' ')
        tokens_ref_data1 = dd_string.encode('utf-8').split(' ')
        m = 0
        for i in tokens_incumbentdata:
            iter1 = filter(lambda x, y=i: is_abbrevation(y, x), tokens_ref_data1)
            if len(iter1) == 0:
                tokens_incumbentdata[m] = tokens_incumbentdata[m]
            else:
                tokens_incumbentdata[m] = filter(lambda x: len(
                    x) == min(map(lambda x: len(x), iter1)), iter1)[0]
            m = m + 1
        field_replace = ' '.join(tokens_incumbentdata)
        dd_dist = map(
            lambda x, y=field_replace: fuzz.token_sort_ratio(re.sub('[^a-zA-Z0-9 \n\.]', ' ', x).lower(), y.lower()),
            dd_display_names)
        dd_dict = dict(zip(dd_dist, dd_display_names))
        if max(dd_dict.keys()) <= threshold1:
            return ''.join(["%X%", str(field_replace)])
        else:
            return ''.join(["%R%", dictt[str(dd_dict.get(max(dd_dict.keys())))]])
    except Exception as exception:
        APP.logger.warning('Warning Field_Resolver')
        APP.logger.warning(exception)
        return ''.join(["%X%", str(field_replace)])


def field_flag_chopper(replace_string):
    try:
        if replace_string is None:
            return replace_string
        elif ((replace_string.startswith("%R%")) |
              (replace_string.startswith("%O%")) |
              (replace_string.startswith("%X%"))):
            return replace_string[3:]
        else:
            return replace_string
    except Exception as exception:
        APP.logger.warning("Warning Field Flag chopper")
        APP.logger.warning(exception)
        return replace_string


def layer_flag_update_v1(replace_string, acflag):
    try:
        if replace_string is None:
            return_val = "X"
        elif replace_string.startswith("%R%"):
            return_val = "R"
        elif replace_string.startswith("%O%"):
            return_val = "O"
        elif replace_string.startswith("%X%"):
            return_val = "X"
        else:
            return_val = acflag
        return return_val
    except Exception as exception:
        APP.logger.warning("Warning Layer Flag Update")
        APP.logger.warning(exception)
        return "X"


def field_resolver_large_dd(field_replace, dict_str):
    try:
        dictt = dict_str
        threshold1 = 65
        threshold2 = 95

        dd_display_names = dictt.keys()
        dd_value = dictt.values()

        # decode text
        field_replace = field_replace.decode('utf8')

        if field_replace is None or field_replace == "" or field_replace == "@#$it is an empty String@#$":
            return ''.join(["%O%", ""])
        field_replace = field_replace.strip()
        if field_replace.lower() in (m.lower() for m in dd_value):
            return_value = ''.join(["%O%", str(field_replace)])
        elif field_replace.lower() in dd_display_names:
            return_value = ''.join(["%O%", dictt[str(field_replace.lower())]])
        elif field_replace.isdigit():
            return_value = ''.join(["%X%", str(field_replace)])
        else:
            list1 = process.extract(field_replace, dd_display_names, limit=25, scorer=fuzz.partial_ratio)
            # print(list1)
            if max(zip(*list1)[1]) > 95:
                return_value = ''.join(["%R%", dictt[zip(*list1)[0][0]]])
            else:
                filtered = zip(*list1)[0]
                # print(filtered)
                list3 = process.extract(
                    field_replace,
                    filtered,
                    limit=15,
                    scorer=fuzz.token_sort_ratio)
                stage2 = dict(zip(zip(*list3)[0], zip(*list3)[1]))
                if max(stage2.values()) <= threshold1:
                    return_value = ''.join(["%X%", str(field_replace)])
                else:
                    return_value = ''.join(["%R%", dictt[[k for k in stage2 if stage2[k] == max(stage2.values())][0]]])

        return return_value

    except Exception as exception:
        APP.logger.warning('Warning Field_Resolver large DD')
        APP.logger.warning(exception)
        print(exception)
        return ''.join(["%X%", str(field_replace)])

def field_resolver_region(field_replace, dict_dropdown, dict_lookup):
    try:
        dictt = dict_dropdown
        threshold1 = 65
        threshold2 = 95

        dd_display_names = dictt.keys()
        dd_value = dictt.values()

        # decode text
        field_replace = field_replace.decode('utf8')

        if field_replace is None or field_replace == "" or field_replace == "@#$it is an empty String@#$":
            return ''.join(["%O%", ""])
        field_replace = field_replace.strip()
        if field_replace.lower() in (m.lower() for m in dd_value):
            return_value = ''.join(["%O%", str(field_replace)])
        elif field_replace.lower() in dd_display_names:
            return_value = ''.join(["%O%", dictt[str(field_replace.lower())]])
        elif field_replace.isdigit():
            return_value = ''.join(["%X%", str(field_replace)])
        else:
            # Find from Region_lookup_dictionary
            list1 = process.extract(field_replace,  dict_lookup.keys(), limit=25, scorer=fuzz.partial_ratio)
            # print(list1)
            if max(zip(*list1)[1]) > 95:
                replacement_from_region_lookup = zip(*list1)[0][0]
                valueToCompare = str(dict_lookup.get(replacement_from_region_lookup)).lower()
                if valueToCompare in dd_display_names:
                    return_value = ''.join(["%R%", dictt[valueToCompare]])
                else :
                    return_value = ''.join(["%O%", str(field_replace)])
            else:
                filtered = zip(*list1)[0]
                # print(filtered)
                list3 = process.extract(
                    field_replace,
                    filtered,
                    limit=15,
                    scorer=fuzz.token_sort_ratio)
                stage2 = dict(zip(zip(*list3)[0], zip(*list3)[1]))
                if max(stage2.values()) <= threshold1:
                    return_value = ''.join(["%X%", str(field_replace)])
                else:
                    replacement_from_region_lookup = [k for k in stage2 if stage2[k] == max(stage2.values())][0]
                    valueToCompare = str(dict_lookup.get(replacement_from_region_lookup)).lower()
                    if valueToCompare in dd_display_names:
                        return_value = ''.join(["%R%", dictt[valueToCompare]])
                    else:
                        return_value = ''.join(["%O%", str(field_replace)])

        return return_value

    except Exception as exception:
        APP.logger.warning('Warning Field_Resolver EMP_047')
        APP.logger.warning(exception)
        print(exception)
        return ''.join(["%X%", str(field_replace)])


def field_resolver_yes_no(field_replace, dict_str, flag):
    try:
        dictt = dict_str
        threshold1 = 45
        threshold2 = 95
        dd_display_names = dictt.keys()
        dd_value = dictt.values()
        field_replace = field_replace.strip()
        if field_replace is None or field_replace == "" or field_replace == "@#$it is an empty String@#$":
            return ''.join(["%O%", ""])
        field_replace = field_replace.strip()
        if flag == 'R':
            return_value = ''.join(["%R%", str(field_replace)])
        elif field_replace.lower() in (m.lower() for m in dd_value):
            return_value = ''.join(["%O%", str(field_replace)])
        elif field_replace.lower() in dd_display_names:
            return_value = ''.join(["%O%", dictt[str(field_replace.lower())]])
        elif field_replace.isdigit():
            return_value = ''.join(["%X%", str(field_replace)])
        else:
            dd_dist = map(lambda x, y=field_replace: fuzz.partial_ratio(y.lower(), x.lower()), dd_display_names)
            dd_dict = dict(zip(dd_dist, dd_display_names))
            if max(dd_dict.keys()) <= threshold1:
                return_value = ''.join(["%X%", str(field_replace)])
            else:
                return_value = ''.join(["%R%", dictt[str(dd_dict.get(max(dd_dict.keys())))]])
        return return_value
    except Exception as exception:
        APP.logger.warning('Warning Yes_No_Field Resolver Exception')
        APP.logger.warning(exception)
        return ''.join(["%X%", str(field_replace)])


if __name__ == '__main__':
    print"In the Flask"
    logging.debug("In the flask")
    POOL = ProcessingPool(nodes=8)  # this is important part- We
    try:
        HANDLER = RotatingFileHandler('flask.log', maxBytes=20000000, backupCount=10)
        HANDLER.setLevel(logging.INFO)
        APP.logger.addHandler(HANDLER)
        APP.run(host='0.0.0.0', port=5000, debug=True, use_reloader=False)
    except KeyboardInterrupt:
        APP.logger.info('Info Flask Stoped Manually')
        POOL.close()
        POOL.join()
