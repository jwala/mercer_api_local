package in.lnt.validtion;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.http.HttpStatus;

/**
 * 
 * @author Sarang Gandhi
 *
 */
public class CustomAPIError extends Exception {

	private static final long serialVersionUID = 1L;

	private HttpStatus _httpStatus; // status: the HTTP status code
	private String _message; // message: the error message associated with exception
	private List<String> _errors = new ArrayList<String>(); // error: List of constructed error messages

	public CustomAPIError() {

	}

	/**
	 * 
	 * @param badRequest
	 * @param message
	 * @param errors
	 */
	public CustomAPIError(HttpStatus httpStatus, String message, List<String> errors) {
		super();
		_httpStatus = httpStatus;
		_message = message;
		_errors = errors;
	}

	public CustomAPIError(HttpStatus httpStatus, String message, String error) {
		super();
		_httpStatus = httpStatus;
		_message = message;
		_errors = Arrays.asList(error);
	}

	public CustomAPIError(HttpStatus httpStatus, String message) {
		_httpStatus = httpStatus;
		_message = message;

	}

	/**
	 * 
	 * @return http Status code
	 */
	public HttpStatus getHttpStatus() {
		return _httpStatus;
	}

	/**
	 * @param _httpStatus
	 *            the _httpStatus to set
	 */
	public void setHttpStatus(HttpStatus httpStatus) {
		_httpStatus = httpStatus;
	}

	/**
	 * @return Error message
	 * 
	 */
	@Override
	public String getMessage() {
		return _message;
	}

	/**
	 * @param _message
	 *            the _message to set
	 */
	public void setMessage(String message) {
		_message = message;
	}

	/**
	 * @return the _errors
	 */
	public List<String> getErrors() {
		return _errors;
	}

	/**
	 * @param _errors
	 *            the _errors to set
	 */
	public void setErrors(List<String> errors) {
		_errors = errors;
	}

	@Override
	public synchronized Throwable fillInStackTrace() {
		return this;
	}
}
