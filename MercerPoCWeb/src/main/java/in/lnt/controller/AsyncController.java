package in.lnt.controller;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.FileItem;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.lti.mosaic.derby.MercerDBCP;

import in.lnt.exceptions.CustomStatusException;
import in.lnt.utility.constants.ErrorCacheConstant;
import in.lnt.utility.constants.LoggerConstants;
import in.lnt.utility.general.Cache;
import in.lnt.utility.general.JsonUtils;
import in.lnt.validations.FormValidator;
import in.lti.mosaic.api.base.beans.Status;
import in.lti.mosaic.api.base.bso.AsyncValidatorBSO;
import in.lti.mosaic.api.base.bso.StatusBSO;
import in.lti.mosaic.api.base.constants.Constants;
import in.lti.mosaic.api.base.exceptions.SystemException;
import in.lti.mosaic.api.base.serializer.ObjectSerializationHandler;

/**
 * @author rushikesh
 *
 */
@Controller
public class AsyncController {

	private static final Logger logger = LoggerFactory.getLogger(AsyncController.class);
	public static ObjectMapper mapper = new ObjectMapper();
	@RequestMapping(value = "/healthCheck", method = RequestMethod.GET)
	public @ResponseBody String healthCheck(HttpServletRequest request,
			HttpServletResponse response) {
		return "OK";
	}

	//This is a temp endpoint for dbcp pool
	// can be removed after successfull testing of dbcp
	@RequestMapping(value = "/getDbcp", method = RequestMethod.GET)
	public @ResponseBody String getDbcpStatus(
			HttpServletRequest request, HttpServletResponse response) throws IOException, SQLException  {
		Map<String , String> map = MercerDBCP.getDataSource().printStatus();

		return ObjectSerializationHandler.toString(map);
	}

	//This is a temp endpoint
	//Should be remoced after testing of derby is completed
	@RequestMapping(value = "/getDerbyCount", method = RequestMethod.GET)
	public @ResponseBody String getDerbyCount(@RequestParam String tableName,HttpServletRequest request,
			HttpServletResponse response) throws SQLException, IOException {

		StringBuilder query = new StringBuilder();
		query.append(" select count(*) as rowcount from ");
		query.append(tableName);

		int count = 0;
		try(
				Connection connection = MercerDBCP.getDataSource().getConnection();
				PreparedStatement countPreparedStatement = connection.prepareStatement(query.toString());
				ResultSet rs = countPreparedStatement.executeQuery();){
			    rs.next();
			    count = rs.getInt("rowcount");
		}

		return count+"";
	}

	//This is a temp endpoint
	//Should be remoced after testing of derby is completed
	@RequestMapping(value = "/runDerby",method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody String runDerbyQuery(@RequestParam String query,HttpServletRequest request,
			HttpServletResponse response) throws SQLException, IOException {
		
		String message="Query is fine";
		try(Connection connection = MercerDBCP.getDataSource().getConnection();

		PreparedStatement countPreparedStatement = connection.prepareStatement(query);
		ResultSet rs = countPreparedStatement.executeQuery()){
			message="Query is fine";
		}
		return message;
	}

	@RequestMapping(value = "/performL0", method = RequestMethod.POST)
	public @ResponseBody void performL0Validations(HttpServletRequest request, HttpServletResponse response) {

		logger.debug(LoggerConstants.LOG_MAXIQAPI , ">> performL0 {} ");


		FileUploadController fileUploadController = new FileUploadController();

		Map<String, Object> l0Response = new HashMap<>();
		ObjectNode errNode = null;
		try {
			Map<String, Object> parsedData = fileUploadController.parseCsvRequest(request, response);

			@SuppressWarnings("unchecked")
			List<InputStream> inputStreams = (List<InputStream>) parsedData.get("inputStreams");
			String entitiesJson = (String) parsedData.get("entitiesJson");
			FileItem fileJSON = (FileItem) parsedData.get("fileJSON");

			l0Response = fileUploadController.performL0ValidationForFileRequest(response,
					parsedData, inputStreams, entitiesJson, fileJSON);

			JSONObject validationDataObject = new JSONObject(fileJSON.getString());

			String entities = validationDataObject.get("entities").toString();

			@SuppressWarnings("unchecked")
			HashMap<String, Object> compareFieldsMap =
					(HashMap<String, Object>) l0Response.get("compareFieldsMap");
			if (compareFieldsMap != null && compareFieldsMap.get(in.lnt.constants.Constants.RESULTNODE) != null) {
				if (compareFieldsMap.get(in.lnt.constants.Constants.RESULTNODE).equals(ErrorCacheConstant.ERR_055))
					throw new CustomStatusException(in.lnt.constants.Constants.HTTPSTATUS_400,
							Cache.getPropertyFromError(ErrorCacheConstant.ERR_055));
			}

			FormValidator formvalidator = new FormValidator();

			@SuppressWarnings("unchecked")
			JsonNode entitiesNodeResponse =
					formvalidator.preparingL1Input(entities, (String) l0Response.get("csvJsonString"), false,
							(LinkedHashMap<String, String>) l0Response.get("xslJsonMap"),
							(String) compareFieldsMap.get("sheetMatchingFlag"), true,
							(l0Response.containsKey(in.lnt.constants.Constants.MISSING_EEID)
									? (boolean) l0Response.get(in.lnt.constants.Constants.MISSING_EEID)
											: false));

			FileUploadController.downLoadData(response, entitiesNodeResponse.toString().getBytes(),
					Constants.ResponseStatus.HTTPSTATUS_200);


		} 
		catch(CustomStatusException cse) {

			errNode = mapper.createObjectNode();			
			errNode = JsonUtils.updateErrorNode(cse,errNode);
			try {
				FileUploadController.downLoadData(response, errNode.toString().getBytes(in.lnt.constants.Constants.UTF_8), cse.getHttpStatus());
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				logger.error(LoggerConstants.LOG_MAXIQAPI , ">> performL0 {} ",cse.getMessage());
			}
		}
		catch (Exception e) {

			logger.error(" General exception caught {}",e.getMessage());

			errNode = mapper.createObjectNode();
			
			errNode = JsonUtils.updateErrorNodeForGenericException(errNode,e);

			FileUploadController.downLoadData(response, errNode.toString().getBytes(),
					Constants.ResponseStatus.HTTPSTATUS_500);

		}

		logger.debug(LoggerConstants.LOG_MAXIQAPI , "<< performL0 {}");

	}

	@RequestMapping(value = "/validateAsync", method = RequestMethod.POST, consumes = "application/json")
	public void validateAsync(@RequestBody String validationData,
			HttpServletRequest request, HttpServletResponse response) {

		logger.debug(LoggerConstants.LOG_MAXIQAPI , ">> validateAsync {}");

		AsyncValidatorBSO.processRequest(validationData);

		logger.debug(LoggerConstants.LOG_MAXIQAPI ,"<< validateAsync {}");
	}


	@RequestMapping(value = "/statusCheck", method = RequestMethod.POST, consumes = "application/json")
	public @ResponseBody String getStatus(@RequestBody String validationData,
			HttpServletRequest request, HttpServletResponse response) throws SystemException  {
		logger.debug(LoggerConstants.LOG_MAXIQAPI  ,">> getStatusOfRequest {}");

		@SuppressWarnings("unchecked")
		Map<String, String> requestMap =
				(Map<String, String>) ObjectSerializationHandler.toObject(validationData, Map.class);

		try {
			Status status = StatusBSO.getStatusFromRequestIdAndEnvironmentName(
					requestMap.get(Constants.Request.REQUESTID),
					requestMap.get(Constants.Request.ENVIRONMENTNAME));
			if(null != status) {
				return ObjectSerializationHandler.toString(status);
			}
		} catch (SystemException e) {
			logger.error(LoggerConstants.LOG_MAXIQAPI ,"<< getStatusOfRequest {}", e.getMessage());
			throw (e);
		}
		throw new SystemException("Request Id not found");

	}

}
