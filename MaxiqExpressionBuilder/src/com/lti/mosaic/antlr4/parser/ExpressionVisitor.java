// Generated from Expression.g4 by ANTLR 4.7.1

package com.lti.mosaic.antlr4.parser;

import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link ExpressionParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface ExpressionVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link ExpressionParser#parse}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParse(ExpressionParser.ParseContext ctx);
	/**
	 * Visit a parse tree produced by {@link ExpressionParser#block}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBlock(ExpressionParser.BlockContext ctx);
	/**
	 * Visit a parse tree produced by the {@code assignmentExpr}
	 * labeled alternative in {@link ExpressionParser#statements}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssignmentExpr(ExpressionParser.AssignmentExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ifStatExpr}
	 * labeled alternative in {@link ExpressionParser#statements}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIfStatExpr(ExpressionParser.IfStatExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code whileStatExpr}
	 * labeled alternative in {@link ExpressionParser#statements}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWhileStatExpr(ExpressionParser.WhileStatExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code statementExpr}
	 * labeled alternative in {@link ExpressionParser#statements}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStatementExpr(ExpressionParser.StatementExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code otherExpr}
	 * labeled alternative in {@link ExpressionParser#statements}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOtherExpr(ExpressionParser.OtherExprContext ctx);
	/**
	 * Visit a parse tree produced by {@link ExpressionParser#assignment}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssignment(ExpressionParser.AssignmentContext ctx);
	/**
	 * Visit a parse tree produced by {@link ExpressionParser#ifStat}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIfStat(ExpressionParser.IfStatContext ctx);
	/**
	 * Visit a parse tree produced by {@link ExpressionParser#conditionBlock}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConditionBlock(ExpressionParser.ConditionBlockContext ctx);
	/**
	 * Visit a parse tree produced by {@link ExpressionParser#statBlock}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStatBlock(ExpressionParser.StatBlockContext ctx);
	/**
	 * Visit a parse tree produced by {@link ExpressionParser#whileStat}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWhileStat(ExpressionParser.WhileStatContext ctx);
	/**
	 * Visit a parse tree produced by {@link ExpressionParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStatement(ExpressionParser.StatementContext ctx);
	/**
	 * Visit a parse tree produced by the {@code notExpr}
	 * labeled alternative in {@link ExpressionParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNotExpr(ExpressionParser.NotExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code unaryMinusExpr}
	 * labeled alternative in {@link ExpressionParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnaryMinusExpr(ExpressionParser.UnaryMinusExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code multiplicationExpr}
	 * labeled alternative in {@link ExpressionParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMultiplicationExpr(ExpressionParser.MultiplicationExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code atomExpr}
	 * labeled alternative in {@link ExpressionParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAtomExpr(ExpressionParser.AtomExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code orExpr}
	 * labeled alternative in {@link ExpressionParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOrExpr(ExpressionParser.OrExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code additiveExpr}
	 * labeled alternative in {@link ExpressionParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAdditiveExpr(ExpressionParser.AdditiveExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code powExpr}
	 * labeled alternative in {@link ExpressionParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPowExpr(ExpressionParser.PowExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code relationalExpr}
	 * labeled alternative in {@link ExpressionParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRelationalExpr(ExpressionParser.RelationalExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code equalityExpr}
	 * labeled alternative in {@link ExpressionParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEqualityExpr(ExpressionParser.EqualityExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code functionExpr}
	 * labeled alternative in {@link ExpressionParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctionExpr(ExpressionParser.FunctionExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code andExpr}
	 * labeled alternative in {@link ExpressionParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAndExpr(ExpressionParser.AndExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code zeroParamterFunctions}
	 * labeled alternative in {@link ExpressionParser#functions}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitZeroParamterFunctions(ExpressionParser.ZeroParamterFunctionsContext ctx);
	/**
	 * Visit a parse tree produced by the {@code singleParamterFunctions}
	 * labeled alternative in {@link ExpressionParser#functions}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSingleParamterFunctions(ExpressionParser.SingleParamterFunctionsContext ctx);
	/**
	 * Visit a parse tree produced by the {@code twoParamterFunctions}
	 * labeled alternative in {@link ExpressionParser#functions}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTwoParamterFunctions(ExpressionParser.TwoParamterFunctionsContext ctx);
	/**
	 * Visit a parse tree produced by the {@code threeParamterFunctions}
	 * labeled alternative in {@link ExpressionParser#functions}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitThreeParamterFunctions(ExpressionParser.ThreeParamterFunctionsContext ctx);
	/**
	 * Visit a parse tree produced by the {@code fourParamterFunctions}
	 * labeled alternative in {@link ExpressionParser#functions}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFourParamterFunctions(ExpressionParser.FourParamterFunctionsContext ctx);
	/**
	 * Visit a parse tree produced by the {@code NParametersFunctions}
	 * labeled alternative in {@link ExpressionParser#functions}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNParametersFunctions(ExpressionParser.NParametersFunctionsContext ctx);
	/**
	 * Visit a parse tree produced by {@link ExpressionParser#functionParam0}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctionParam0(ExpressionParser.FunctionParam0Context ctx);
	/**
	 * Visit a parse tree produced by {@link ExpressionParser#functionParam1}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctionParam1(ExpressionParser.FunctionParam1Context ctx);
	/**
	 * Visit a parse tree produced by {@link ExpressionParser#functionParam2}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctionParam2(ExpressionParser.FunctionParam2Context ctx);
	/**
	 * Visit a parse tree produced by {@link ExpressionParser#functionParam3}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctionParam3(ExpressionParser.FunctionParam3Context ctx);
	/**
	 * Visit a parse tree produced by {@link ExpressionParser#functionParam4}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctionParam4(ExpressionParser.FunctionParam4Context ctx);
	/**
	 * Visit a parse tree produced by {@link ExpressionParser#functionParamN}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctionParamN(ExpressionParser.FunctionParamNContext ctx);
	/**
	 * Visit a parse tree produced by {@link ExpressionParser#argumentsN}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArgumentsN(ExpressionParser.ArgumentsNContext ctx);
	/**
	 * Visit a parse tree produced by {@link ExpressionParser#arguments1}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArguments1(ExpressionParser.Arguments1Context ctx);
	/**
	 * Visit a parse tree produced by {@link ExpressionParser#arguments2}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArguments2(ExpressionParser.Arguments2Context ctx);
	/**
	 * Visit a parse tree produced by {@link ExpressionParser#arguments3}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArguments3(ExpressionParser.Arguments3Context ctx);
	/**
	 * Visit a parse tree produced by {@link ExpressionParser#arguments4}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArguments4(ExpressionParser.Arguments4Context ctx);
	/**
	 * Visit a parse tree produced by the {@code parExpr}
	 * labeled alternative in {@link ExpressionParser#atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParExpr(ExpressionParser.ParExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code arrayAtom}
	 * labeled alternative in {@link ExpressionParser#atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArrayAtom(ExpressionParser.ArrayAtomContext ctx);
	/**
	 * Visit a parse tree produced by the {@code numberAtom}
	 * labeled alternative in {@link ExpressionParser#atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNumberAtom(ExpressionParser.NumberAtomContext ctx);
	/**
	 * Visit a parse tree produced by the {@code booleanAtom}
	 * labeled alternative in {@link ExpressionParser#atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBooleanAtom(ExpressionParser.BooleanAtomContext ctx);
	/**
	 * Visit a parse tree produced by the {@code idAtom}
	 * labeled alternative in {@link ExpressionParser#atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIdAtom(ExpressionParser.IdAtomContext ctx);
	/**
	 * Visit a parse tree produced by the {@code stringAtom}
	 * labeled alternative in {@link ExpressionParser#atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStringAtom(ExpressionParser.StringAtomContext ctx);
	/**
	 * Visit a parse tree produced by the {@code nilAtom}
	 * labeled alternative in {@link ExpressionParser#atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNilAtom(ExpressionParser.NilAtomContext ctx);
	/**
	 * Visit a parse tree produced by {@link ExpressionParser#value_list}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitValue_list(ExpressionParser.Value_listContext ctx);
	/**
	 * Visit a parse tree produced by the {@code arrayValues}
	 * labeled alternative in {@link ExpressionParser#array}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArrayValues(ExpressionParser.ArrayValuesContext ctx);
	/**
	 * Visit a parse tree produced by the {@code arrayElementTypes}
	 * labeled alternative in {@link ExpressionParser#arrayElement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArrayElementTypes(ExpressionParser.ArrayElementTypesContext ctx);
}