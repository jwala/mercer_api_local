// Generated from Expression.g4 by ANTLR 4.7.1

package com.lti.mosaic.antlr4.parser;

import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link ExpressionParser}.
 */
public interface ExpressionListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link ExpressionParser#parse}.
	 * @param ctx the parse tree
	 */
	void enterParse(ExpressionParser.ParseContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExpressionParser#parse}.
	 * @param ctx the parse tree
	 */
	void exitParse(ExpressionParser.ParseContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExpressionParser#block}.
	 * @param ctx the parse tree
	 */
	void enterBlock(ExpressionParser.BlockContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExpressionParser#block}.
	 * @param ctx the parse tree
	 */
	void exitBlock(ExpressionParser.BlockContext ctx);
	/**
	 * Enter a parse tree produced by the {@code assignmentExpr}
	 * labeled alternative in {@link ExpressionParser#statements}.
	 * @param ctx the parse tree
	 */
	void enterAssignmentExpr(ExpressionParser.AssignmentExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code assignmentExpr}
	 * labeled alternative in {@link ExpressionParser#statements}.
	 * @param ctx the parse tree
	 */
	void exitAssignmentExpr(ExpressionParser.AssignmentExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ifStatExpr}
	 * labeled alternative in {@link ExpressionParser#statements}.
	 * @param ctx the parse tree
	 */
	void enterIfStatExpr(ExpressionParser.IfStatExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ifStatExpr}
	 * labeled alternative in {@link ExpressionParser#statements}.
	 * @param ctx the parse tree
	 */
	void exitIfStatExpr(ExpressionParser.IfStatExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code whileStatExpr}
	 * labeled alternative in {@link ExpressionParser#statements}.
	 * @param ctx the parse tree
	 */
	void enterWhileStatExpr(ExpressionParser.WhileStatExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code whileStatExpr}
	 * labeled alternative in {@link ExpressionParser#statements}.
	 * @param ctx the parse tree
	 */
	void exitWhileStatExpr(ExpressionParser.WhileStatExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code statementExpr}
	 * labeled alternative in {@link ExpressionParser#statements}.
	 * @param ctx the parse tree
	 */
	void enterStatementExpr(ExpressionParser.StatementExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code statementExpr}
	 * labeled alternative in {@link ExpressionParser#statements}.
	 * @param ctx the parse tree
	 */
	void exitStatementExpr(ExpressionParser.StatementExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code otherExpr}
	 * labeled alternative in {@link ExpressionParser#statements}.
	 * @param ctx the parse tree
	 */
	void enterOtherExpr(ExpressionParser.OtherExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code otherExpr}
	 * labeled alternative in {@link ExpressionParser#statements}.
	 * @param ctx the parse tree
	 */
	void exitOtherExpr(ExpressionParser.OtherExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExpressionParser#assignment}.
	 * @param ctx the parse tree
	 */
	void enterAssignment(ExpressionParser.AssignmentContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExpressionParser#assignment}.
	 * @param ctx the parse tree
	 */
	void exitAssignment(ExpressionParser.AssignmentContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExpressionParser#ifStat}.
	 * @param ctx the parse tree
	 */
	void enterIfStat(ExpressionParser.IfStatContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExpressionParser#ifStat}.
	 * @param ctx the parse tree
	 */
	void exitIfStat(ExpressionParser.IfStatContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExpressionParser#conditionBlock}.
	 * @param ctx the parse tree
	 */
	void enterConditionBlock(ExpressionParser.ConditionBlockContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExpressionParser#conditionBlock}.
	 * @param ctx the parse tree
	 */
	void exitConditionBlock(ExpressionParser.ConditionBlockContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExpressionParser#statBlock}.
	 * @param ctx the parse tree
	 */
	void enterStatBlock(ExpressionParser.StatBlockContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExpressionParser#statBlock}.
	 * @param ctx the parse tree
	 */
	void exitStatBlock(ExpressionParser.StatBlockContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExpressionParser#whileStat}.
	 * @param ctx the parse tree
	 */
	void enterWhileStat(ExpressionParser.WhileStatContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExpressionParser#whileStat}.
	 * @param ctx the parse tree
	 */
	void exitWhileStat(ExpressionParser.WhileStatContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExpressionParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterStatement(ExpressionParser.StatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExpressionParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitStatement(ExpressionParser.StatementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code notExpr}
	 * labeled alternative in {@link ExpressionParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterNotExpr(ExpressionParser.NotExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code notExpr}
	 * labeled alternative in {@link ExpressionParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitNotExpr(ExpressionParser.NotExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code unaryMinusExpr}
	 * labeled alternative in {@link ExpressionParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterUnaryMinusExpr(ExpressionParser.UnaryMinusExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code unaryMinusExpr}
	 * labeled alternative in {@link ExpressionParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitUnaryMinusExpr(ExpressionParser.UnaryMinusExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code multiplicationExpr}
	 * labeled alternative in {@link ExpressionParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterMultiplicationExpr(ExpressionParser.MultiplicationExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code multiplicationExpr}
	 * labeled alternative in {@link ExpressionParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitMultiplicationExpr(ExpressionParser.MultiplicationExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code atomExpr}
	 * labeled alternative in {@link ExpressionParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterAtomExpr(ExpressionParser.AtomExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code atomExpr}
	 * labeled alternative in {@link ExpressionParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitAtomExpr(ExpressionParser.AtomExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code orExpr}
	 * labeled alternative in {@link ExpressionParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterOrExpr(ExpressionParser.OrExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code orExpr}
	 * labeled alternative in {@link ExpressionParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitOrExpr(ExpressionParser.OrExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code additiveExpr}
	 * labeled alternative in {@link ExpressionParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterAdditiveExpr(ExpressionParser.AdditiveExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code additiveExpr}
	 * labeled alternative in {@link ExpressionParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitAdditiveExpr(ExpressionParser.AdditiveExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code powExpr}
	 * labeled alternative in {@link ExpressionParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterPowExpr(ExpressionParser.PowExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code powExpr}
	 * labeled alternative in {@link ExpressionParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitPowExpr(ExpressionParser.PowExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code relationalExpr}
	 * labeled alternative in {@link ExpressionParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterRelationalExpr(ExpressionParser.RelationalExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code relationalExpr}
	 * labeled alternative in {@link ExpressionParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitRelationalExpr(ExpressionParser.RelationalExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code equalityExpr}
	 * labeled alternative in {@link ExpressionParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterEqualityExpr(ExpressionParser.EqualityExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code equalityExpr}
	 * labeled alternative in {@link ExpressionParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitEqualityExpr(ExpressionParser.EqualityExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code functionExpr}
	 * labeled alternative in {@link ExpressionParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterFunctionExpr(ExpressionParser.FunctionExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code functionExpr}
	 * labeled alternative in {@link ExpressionParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitFunctionExpr(ExpressionParser.FunctionExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code andExpr}
	 * labeled alternative in {@link ExpressionParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterAndExpr(ExpressionParser.AndExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code andExpr}
	 * labeled alternative in {@link ExpressionParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitAndExpr(ExpressionParser.AndExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code zeroParamterFunctions}
	 * labeled alternative in {@link ExpressionParser#functions}.
	 * @param ctx the parse tree
	 */
	void enterZeroParamterFunctions(ExpressionParser.ZeroParamterFunctionsContext ctx);
	/**
	 * Exit a parse tree produced by the {@code zeroParamterFunctions}
	 * labeled alternative in {@link ExpressionParser#functions}.
	 * @param ctx the parse tree
	 */
	void exitZeroParamterFunctions(ExpressionParser.ZeroParamterFunctionsContext ctx);
	/**
	 * Enter a parse tree produced by the {@code singleParamterFunctions}
	 * labeled alternative in {@link ExpressionParser#functions}.
	 * @param ctx the parse tree
	 */
	void enterSingleParamterFunctions(ExpressionParser.SingleParamterFunctionsContext ctx);
	/**
	 * Exit a parse tree produced by the {@code singleParamterFunctions}
	 * labeled alternative in {@link ExpressionParser#functions}.
	 * @param ctx the parse tree
	 */
	void exitSingleParamterFunctions(ExpressionParser.SingleParamterFunctionsContext ctx);
	/**
	 * Enter a parse tree produced by the {@code twoParamterFunctions}
	 * labeled alternative in {@link ExpressionParser#functions}.
	 * @param ctx the parse tree
	 */
	void enterTwoParamterFunctions(ExpressionParser.TwoParamterFunctionsContext ctx);
	/**
	 * Exit a parse tree produced by the {@code twoParamterFunctions}
	 * labeled alternative in {@link ExpressionParser#functions}.
	 * @param ctx the parse tree
	 */
	void exitTwoParamterFunctions(ExpressionParser.TwoParamterFunctionsContext ctx);
	/**
	 * Enter a parse tree produced by the {@code threeParamterFunctions}
	 * labeled alternative in {@link ExpressionParser#functions}.
	 * @param ctx the parse tree
	 */
	void enterThreeParamterFunctions(ExpressionParser.ThreeParamterFunctionsContext ctx);
	/**
	 * Exit a parse tree produced by the {@code threeParamterFunctions}
	 * labeled alternative in {@link ExpressionParser#functions}.
	 * @param ctx the parse tree
	 */
	void exitThreeParamterFunctions(ExpressionParser.ThreeParamterFunctionsContext ctx);
	/**
	 * Enter a parse tree produced by the {@code fourParamterFunctions}
	 * labeled alternative in {@link ExpressionParser#functions}.
	 * @param ctx the parse tree
	 */
	void enterFourParamterFunctions(ExpressionParser.FourParamterFunctionsContext ctx);
	/**
	 * Exit a parse tree produced by the {@code fourParamterFunctions}
	 * labeled alternative in {@link ExpressionParser#functions}.
	 * @param ctx the parse tree
	 */
	void exitFourParamterFunctions(ExpressionParser.FourParamterFunctionsContext ctx);
	/**
	 * Enter a parse tree produced by the {@code NParametersFunctions}
	 * labeled alternative in {@link ExpressionParser#functions}.
	 * @param ctx the parse tree
	 */
	void enterNParametersFunctions(ExpressionParser.NParametersFunctionsContext ctx);
	/**
	 * Exit a parse tree produced by the {@code NParametersFunctions}
	 * labeled alternative in {@link ExpressionParser#functions}.
	 * @param ctx the parse tree
	 */
	void exitNParametersFunctions(ExpressionParser.NParametersFunctionsContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExpressionParser#functionParam0}.
	 * @param ctx the parse tree
	 */
	void enterFunctionParam0(ExpressionParser.FunctionParam0Context ctx);
	/**
	 * Exit a parse tree produced by {@link ExpressionParser#functionParam0}.
	 * @param ctx the parse tree
	 */
	void exitFunctionParam0(ExpressionParser.FunctionParam0Context ctx);
	/**
	 * Enter a parse tree produced by {@link ExpressionParser#functionParam1}.
	 * @param ctx the parse tree
	 */
	void enterFunctionParam1(ExpressionParser.FunctionParam1Context ctx);
	/**
	 * Exit a parse tree produced by {@link ExpressionParser#functionParam1}.
	 * @param ctx the parse tree
	 */
	void exitFunctionParam1(ExpressionParser.FunctionParam1Context ctx);
	/**
	 * Enter a parse tree produced by {@link ExpressionParser#functionParam2}.
	 * @param ctx the parse tree
	 */
	void enterFunctionParam2(ExpressionParser.FunctionParam2Context ctx);
	/**
	 * Exit a parse tree produced by {@link ExpressionParser#functionParam2}.
	 * @param ctx the parse tree
	 */
	void exitFunctionParam2(ExpressionParser.FunctionParam2Context ctx);
	/**
	 * Enter a parse tree produced by {@link ExpressionParser#functionParam3}.
	 * @param ctx the parse tree
	 */
	void enterFunctionParam3(ExpressionParser.FunctionParam3Context ctx);
	/**
	 * Exit a parse tree produced by {@link ExpressionParser#functionParam3}.
	 * @param ctx the parse tree
	 */
	void exitFunctionParam3(ExpressionParser.FunctionParam3Context ctx);
	/**
	 * Enter a parse tree produced by {@link ExpressionParser#functionParam4}.
	 * @param ctx the parse tree
	 */
	void enterFunctionParam4(ExpressionParser.FunctionParam4Context ctx);
	/**
	 * Exit a parse tree produced by {@link ExpressionParser#functionParam4}.
	 * @param ctx the parse tree
	 */
	void exitFunctionParam4(ExpressionParser.FunctionParam4Context ctx);
	/**
	 * Enter a parse tree produced by {@link ExpressionParser#functionParamN}.
	 * @param ctx the parse tree
	 */
	void enterFunctionParamN(ExpressionParser.FunctionParamNContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExpressionParser#functionParamN}.
	 * @param ctx the parse tree
	 */
	void exitFunctionParamN(ExpressionParser.FunctionParamNContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExpressionParser#argumentsN}.
	 * @param ctx the parse tree
	 */
	void enterArgumentsN(ExpressionParser.ArgumentsNContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExpressionParser#argumentsN}.
	 * @param ctx the parse tree
	 */
	void exitArgumentsN(ExpressionParser.ArgumentsNContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExpressionParser#arguments1}.
	 * @param ctx the parse tree
	 */
	void enterArguments1(ExpressionParser.Arguments1Context ctx);
	/**
	 * Exit a parse tree produced by {@link ExpressionParser#arguments1}.
	 * @param ctx the parse tree
	 */
	void exitArguments1(ExpressionParser.Arguments1Context ctx);
	/**
	 * Enter a parse tree produced by {@link ExpressionParser#arguments2}.
	 * @param ctx the parse tree
	 */
	void enterArguments2(ExpressionParser.Arguments2Context ctx);
	/**
	 * Exit a parse tree produced by {@link ExpressionParser#arguments2}.
	 * @param ctx the parse tree
	 */
	void exitArguments2(ExpressionParser.Arguments2Context ctx);
	/**
	 * Enter a parse tree produced by {@link ExpressionParser#arguments3}.
	 * @param ctx the parse tree
	 */
	void enterArguments3(ExpressionParser.Arguments3Context ctx);
	/**
	 * Exit a parse tree produced by {@link ExpressionParser#arguments3}.
	 * @param ctx the parse tree
	 */
	void exitArguments3(ExpressionParser.Arguments3Context ctx);
	/**
	 * Enter a parse tree produced by {@link ExpressionParser#arguments4}.
	 * @param ctx the parse tree
	 */
	void enterArguments4(ExpressionParser.Arguments4Context ctx);
	/**
	 * Exit a parse tree produced by {@link ExpressionParser#arguments4}.
	 * @param ctx the parse tree
	 */
	void exitArguments4(ExpressionParser.Arguments4Context ctx);
	/**
	 * Enter a parse tree produced by the {@code parExpr}
	 * labeled alternative in {@link ExpressionParser#atom}.
	 * @param ctx the parse tree
	 */
	void enterParExpr(ExpressionParser.ParExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code parExpr}
	 * labeled alternative in {@link ExpressionParser#atom}.
	 * @param ctx the parse tree
	 */
	void exitParExpr(ExpressionParser.ParExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code arrayAtom}
	 * labeled alternative in {@link ExpressionParser#atom}.
	 * @param ctx the parse tree
	 */
	void enterArrayAtom(ExpressionParser.ArrayAtomContext ctx);
	/**
	 * Exit a parse tree produced by the {@code arrayAtom}
	 * labeled alternative in {@link ExpressionParser#atom}.
	 * @param ctx the parse tree
	 */
	void exitArrayAtom(ExpressionParser.ArrayAtomContext ctx);
	/**
	 * Enter a parse tree produced by the {@code numberAtom}
	 * labeled alternative in {@link ExpressionParser#atom}.
	 * @param ctx the parse tree
	 */
	void enterNumberAtom(ExpressionParser.NumberAtomContext ctx);
	/**
	 * Exit a parse tree produced by the {@code numberAtom}
	 * labeled alternative in {@link ExpressionParser#atom}.
	 * @param ctx the parse tree
	 */
	void exitNumberAtom(ExpressionParser.NumberAtomContext ctx);
	/**
	 * Enter a parse tree produced by the {@code booleanAtom}
	 * labeled alternative in {@link ExpressionParser#atom}.
	 * @param ctx the parse tree
	 */
	void enterBooleanAtom(ExpressionParser.BooleanAtomContext ctx);
	/**
	 * Exit a parse tree produced by the {@code booleanAtom}
	 * labeled alternative in {@link ExpressionParser#atom}.
	 * @param ctx the parse tree
	 */
	void exitBooleanAtom(ExpressionParser.BooleanAtomContext ctx);
	/**
	 * Enter a parse tree produced by the {@code idAtom}
	 * labeled alternative in {@link ExpressionParser#atom}.
	 * @param ctx the parse tree
	 */
	void enterIdAtom(ExpressionParser.IdAtomContext ctx);
	/**
	 * Exit a parse tree produced by the {@code idAtom}
	 * labeled alternative in {@link ExpressionParser#atom}.
	 * @param ctx the parse tree
	 */
	void exitIdAtom(ExpressionParser.IdAtomContext ctx);
	/**
	 * Enter a parse tree produced by the {@code stringAtom}
	 * labeled alternative in {@link ExpressionParser#atom}.
	 * @param ctx the parse tree
	 */
	void enterStringAtom(ExpressionParser.StringAtomContext ctx);
	/**
	 * Exit a parse tree produced by the {@code stringAtom}
	 * labeled alternative in {@link ExpressionParser#atom}.
	 * @param ctx the parse tree
	 */
	void exitStringAtom(ExpressionParser.StringAtomContext ctx);
	/**
	 * Enter a parse tree produced by the {@code nilAtom}
	 * labeled alternative in {@link ExpressionParser#atom}.
	 * @param ctx the parse tree
	 */
	void enterNilAtom(ExpressionParser.NilAtomContext ctx);
	/**
	 * Exit a parse tree produced by the {@code nilAtom}
	 * labeled alternative in {@link ExpressionParser#atom}.
	 * @param ctx the parse tree
	 */
	void exitNilAtom(ExpressionParser.NilAtomContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExpressionParser#value_list}.
	 * @param ctx the parse tree
	 */
	void enterValue_list(ExpressionParser.Value_listContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExpressionParser#value_list}.
	 * @param ctx the parse tree
	 */
	void exitValue_list(ExpressionParser.Value_listContext ctx);
	/**
	 * Enter a parse tree produced by the {@code arrayValues}
	 * labeled alternative in {@link ExpressionParser#array}.
	 * @param ctx the parse tree
	 */
	void enterArrayValues(ExpressionParser.ArrayValuesContext ctx);
	/**
	 * Exit a parse tree produced by the {@code arrayValues}
	 * labeled alternative in {@link ExpressionParser#array}.
	 * @param ctx the parse tree
	 */
	void exitArrayValues(ExpressionParser.ArrayValuesContext ctx);
	/**
	 * Enter a parse tree produced by the {@code arrayElementTypes}
	 * labeled alternative in {@link ExpressionParser#arrayElement}.
	 * @param ctx the parse tree
	 */
	void enterArrayElementTypes(ExpressionParser.ArrayElementTypesContext ctx);
	/**
	 * Exit a parse tree produced by the {@code arrayElementTypes}
	 * labeled alternative in {@link ExpressionParser#arrayElement}.
	 * @param ctx the parse tree
	 */
	void exitArrayElementTypes(ExpressionParser.ArrayElementTypesContext ctx);
}