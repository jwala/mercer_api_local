# Generated from Expression.g4 by ANTLR 4.7.1
# encoding: utf-8
from __future__ import print_function
from antlr4 import *
from io import StringIO
import sys

def serializedATN():
    with StringIO() as buf:
        buf.write(u"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3")
        buf.write(u"-\u0104\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t")
        buf.write(u"\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r")
        buf.write(u"\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22\4")
        buf.write(u"\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30")
        buf.write(u"\t\30\4\31\t\31\4\32\t\32\4\33\t\33\3\2\3\2\3\2\3\3\7")
        buf.write(u"\3;\n\3\f\3\16\3>\13\3\3\4\3\4\3\4\3\4\3\4\3\4\5\4F\n")
        buf.write(u"\4\3\5\3\5\3\5\3\5\3\5\3\6\3\6\3\6\3\6\3\6\7\6R\n\6\f")
        buf.write(u"\6\16\6U\13\6\3\6\3\6\5\6Y\n\6\3\7\3\7\3\7\3\b\3\b\3")
        buf.write(u"\b\3\b\3\b\5\bc\n\b\3\t\3\t\3\t\3\t\3\n\3\n\3\n\3\13")
        buf.write(u"\3\13\3\13\3\13\3\13\3\13\3\13\5\13s\n\13\3\13\3\13\3")
        buf.write(u"\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13")
        buf.write(u"\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\7\13\u008a\n")
        buf.write(u"\13\f\13\16\13\u008d\13\13\3\f\3\f\3\f\3\f\3\f\3\f\5")
        buf.write(u"\f\u0095\n\f\3\r\3\r\3\r\3\r\3\16\3\16\3\16\3\16\3\16")
        buf.write(u"\3\17\3\17\3\17\3\17\3\17\3\20\3\20\3\20\3\20\3\20\3")
        buf.write(u"\21\3\21\3\21\3\21\3\21\3\22\3\22\3\22\3\22\3\22\3\23")
        buf.write(u"\5\23\u00b5\n\23\3\23\3\23\5\23\u00b9\n\23\7\23\u00bb")
        buf.write(u"\n\23\f\23\16\23\u00be\13\23\3\24\5\24\u00c1\n\24\3\25")
        buf.write(u"\5\25\u00c4\n\25\3\25\3\25\5\25\u00c8\n\25\3\26\5\26")
        buf.write(u"\u00cb\n\26\3\26\3\26\5\26\u00cf\n\26\3\26\3\26\5\26")
        buf.write(u"\u00d3\n\26\3\27\5\27\u00d6\n\27\3\27\3\27\5\27\u00da")
        buf.write(u"\n\27\3\27\3\27\5\27\u00de\n\27\3\27\3\27\5\27\u00e2")
        buf.write(u"\n\27\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3")
        buf.write(u"\30\5\30\u00ee\n\30\3\31\3\31\5\31\u00f2\n\31\3\31\3")
        buf.write(u"\31\3\32\5\32\u00f7\n\32\3\32\3\32\5\32\u00fb\n\32\7")
        buf.write(u"\32\u00fd\n\32\f\32\16\32\u0100\13\32\3\33\3\33\3\33")
        buf.write(u"\2\3\24\34\2\4\6\b\n\f\16\20\22\24\26\30\32\34\36 \"")
        buf.write(u"$&(*,.\60\62\64\2\b\3\2\23\25\3\2\21\22\3\2\r\20\3\2")
        buf.write(u"\13\f\3\2()\3\2!\"\2\u0117\2\66\3\2\2\2\4<\3\2\2\2\6")
        buf.write(u"E\3\2\2\2\bG\3\2\2\2\nL\3\2\2\2\fZ\3\2\2\2\16b\3\2\2")
        buf.write(u"\2\20d\3\2\2\2\22h\3\2\2\2\24r\3\2\2\2\26\u0094\3\2\2")
        buf.write(u"\2\30\u0096\3\2\2\2\32\u009a\3\2\2\2\34\u009f\3\2\2\2")
        buf.write(u"\36\u00a4\3\2\2\2 \u00a9\3\2\2\2\"\u00ae\3\2\2\2$\u00b4")
        buf.write(u"\3\2\2\2&\u00c0\3\2\2\2(\u00c3\3\2\2\2*\u00ca\3\2\2\2")
        buf.write(u",\u00d5\3\2\2\2.\u00ed\3\2\2\2\60\u00ef\3\2\2\2\62\u00f6")
        buf.write(u"\3\2\2\2\64\u0101\3\2\2\2\66\67\5\4\3\2\678\7\2\2\38")
        buf.write(u"\3\3\2\2\29;\5\6\4\2:9\3\2\2\2;>\3\2\2\2<:\3\2\2\2<=")
        buf.write(u"\3\2\2\2=\5\3\2\2\2><\3\2\2\2?F\5\b\5\2@F\5\n\6\2AF\5")
        buf.write(u"\20\t\2BF\5\22\n\2CD\7-\2\2DF\b\4\1\2E?\3\2\2\2E@\3\2")
        buf.write(u"\2\2EA\3\2\2\2EB\3\2\2\2EC\3\2\2\2F\7\3\2\2\2GH\7\'\2")
        buf.write(u"\2HI\7\32\2\2IJ\5\24\13\2JK\7\30\2\2K\t\3\2\2\2LM\7$")
        buf.write(u"\2\2MS\5\f\7\2NO\7%\2\2OP\7$\2\2PR\5\f\7\2QN\3\2\2\2")
        buf.write(u"RU\3\2\2\2SQ\3\2\2\2ST\3\2\2\2TX\3\2\2\2US\3\2\2\2VW")
        buf.write(u"\7%\2\2WY\5\16\b\2XV\3\2\2\2XY\3\2\2\2Y\13\3\2\2\2Z[")
        buf.write(u"\5\24\13\2[\\\5\16\b\2\\\r\3\2\2\2]^\7\35\2\2^_\5\4\3")
        buf.write(u"\2_`\7\36\2\2`c\3\2\2\2ac\5\6\4\2b]\3\2\2\2ba\3\2\2\2")
        buf.write(u"c\17\3\2\2\2de\7&\2\2ef\5\24\13\2fg\5\16\b\2g\21\3\2")
        buf.write(u"\2\2hi\5\24\13\2ij\7\30\2\2j\23\3\2\2\2kl\b\13\1\2ls")
        buf.write(u"\5\26\f\2mn\7\22\2\2ns\5\24\13\13op\7\27\2\2ps\5\24\13")
        buf.write(u"\nqs\5.\30\2rk\3\2\2\2rm\3\2\2\2ro\3\2\2\2rq\3\2\2\2")
        buf.write(u"s\u008b\3\2\2\2tu\f\f\2\2uv\7\26\2\2v\u008a\5\24\13\r")
        buf.write(u"wx\f\t\2\2xy\t\2\2\2y\u008a\5\24\13\nz{\f\b\2\2{|\t\3")
        buf.write(u"\2\2|\u008a\5\24\13\t}~\f\7\2\2~\177\t\4\2\2\177\u008a")
        buf.write(u"\5\24\13\b\u0080\u0081\f\6\2\2\u0081\u0082\t\5\2\2\u0082")
        buf.write(u"\u008a\5\24\13\7\u0083\u0084\f\5\2\2\u0084\u0085\7\n")
        buf.write(u"\2\2\u0085\u008a\5\24\13\6\u0086\u0087\f\4\2\2\u0087")
        buf.write(u"\u0088\7\t\2\2\u0088\u008a\5\24\13\5\u0089t\3\2\2\2\u0089")
        buf.write(u"w\3\2\2\2\u0089z\3\2\2\2\u0089}\3\2\2\2\u0089\u0080\3")
        buf.write(u"\2\2\2\u0089\u0083\3\2\2\2\u0089\u0086\3\2\2\2\u008a")
        buf.write(u"\u008d\3\2\2\2\u008b\u0089\3\2\2\2\u008b\u008c\3\2\2")
        buf.write(u"\2\u008c\25\3\2\2\2\u008d\u008b\3\2\2\2\u008e\u0095\5")
        buf.write(u"\30\r\2\u008f\u0095\5\32\16\2\u0090\u0095\5\34\17\2\u0091")
        buf.write(u"\u0095\5\36\20\2\u0092\u0095\5 \21\2\u0093\u0095\5\"")
        buf.write(u"\22\2\u0094\u008e\3\2\2\2\u0094\u008f\3\2\2\2\u0094\u0090")
        buf.write(u"\3\2\2\2\u0094\u0091\3\2\2\2\u0094\u0092\3\2\2\2\u0094")
        buf.write(u"\u0093\3\2\2\2\u0095\27\3\2\2\2\u0096\u0097\7\3\2\2\u0097")
        buf.write(u"\u0098\7\33\2\2\u0098\u0099\7\34\2\2\u0099\31\3\2\2\2")
        buf.write(u"\u009a\u009b\7\4\2\2\u009b\u009c\7\33\2\2\u009c\u009d")
        buf.write(u"\5&\24\2\u009d\u009e\7\34\2\2\u009e\33\3\2\2\2\u009f")
        buf.write(u"\u00a0\7\5\2\2\u00a0\u00a1\7\33\2\2\u00a1\u00a2\5(\25")
        buf.write(u"\2\u00a2\u00a3\7\34\2\2\u00a3\35\3\2\2\2\u00a4\u00a5")
        buf.write(u"\7\6\2\2\u00a5\u00a6\7\33\2\2\u00a6\u00a7\5*\26\2\u00a7")
        buf.write(u"\u00a8\7\34\2\2\u00a8\37\3\2\2\2\u00a9\u00aa\7\7\2\2")
        buf.write(u"\u00aa\u00ab\7\33\2\2\u00ab\u00ac\5,\27\2\u00ac\u00ad")
        buf.write(u"\7\34\2\2\u00ad!\3\2\2\2\u00ae\u00af\7\b\2\2\u00af\u00b0")
        buf.write(u"\7\33\2\2\u00b0\u00b1\5$\23\2\u00b1\u00b2\7\34\2\2\u00b2")
        buf.write(u"#\3\2\2\2\u00b3\u00b5\5\24\13\2\u00b4\u00b3\3\2\2\2\u00b4")
        buf.write(u"\u00b5\3\2\2\2\u00b5\u00bc\3\2\2\2\u00b6\u00b8\7\31\2")
        buf.write(u"\2\u00b7\u00b9\5\24\13\2\u00b8\u00b7\3\2\2\2\u00b8\u00b9")
        buf.write(u"\3\2\2\2\u00b9\u00bb\3\2\2\2\u00ba\u00b6\3\2\2\2\u00bb")
        buf.write(u"\u00be\3\2\2\2\u00bc\u00ba\3\2\2\2\u00bc\u00bd\3\2\2")
        buf.write(u"\2\u00bd%\3\2\2\2\u00be\u00bc\3\2\2\2\u00bf\u00c1\5\24")
        buf.write(u"\13\2\u00c0\u00bf\3\2\2\2\u00c0\u00c1\3\2\2\2\u00c1\'")
        buf.write(u"\3\2\2\2\u00c2\u00c4\5\24\13\2\u00c3\u00c2\3\2\2\2\u00c3")
        buf.write(u"\u00c4\3\2\2\2\u00c4\u00c5\3\2\2\2\u00c5\u00c7\7\31\2")
        buf.write(u"\2\u00c6\u00c8\5\24\13\2\u00c7\u00c6\3\2\2\2\u00c7\u00c8")
        buf.write(u"\3\2\2\2\u00c8)\3\2\2\2\u00c9\u00cb\5\24\13\2\u00ca\u00c9")
        buf.write(u"\3\2\2\2\u00ca\u00cb\3\2\2\2\u00cb\u00cc\3\2\2\2\u00cc")
        buf.write(u"\u00ce\7\31\2\2\u00cd\u00cf\5\24\13\2\u00ce\u00cd\3\2")
        buf.write(u"\2\2\u00ce\u00cf\3\2\2\2\u00cf\u00d0\3\2\2\2\u00d0\u00d2")
        buf.write(u"\7\31\2\2\u00d1\u00d3\5\24\13\2\u00d2\u00d1\3\2\2\2\u00d2")
        buf.write(u"\u00d3\3\2\2\2\u00d3+\3\2\2\2\u00d4\u00d6\5\24\13\2\u00d5")
        buf.write(u"\u00d4\3\2\2\2\u00d5\u00d6\3\2\2\2\u00d6\u00d7\3\2\2")
        buf.write(u"\2\u00d7\u00d9\7\31\2\2\u00d8\u00da\5\24\13\2\u00d9\u00d8")
        buf.write(u"\3\2\2\2\u00d9\u00da\3\2\2\2\u00da\u00db\3\2\2\2\u00db")
        buf.write(u"\u00dd\7\31\2\2\u00dc\u00de\5\24\13\2\u00dd\u00dc\3\2")
        buf.write(u"\2\2\u00dd\u00de\3\2\2\2\u00de\u00df\3\2\2\2\u00df\u00e1")
        buf.write(u"\7\31\2\2\u00e0\u00e2\5\24\13\2\u00e1\u00e0\3\2\2\2\u00e1")
        buf.write(u"\u00e2\3\2\2\2\u00e2-\3\2\2\2\u00e3\u00e4\7\33\2\2\u00e4")
        buf.write(u"\u00e5\5\24\13\2\u00e5\u00e6\7\34\2\2\u00e6\u00ee\3\2")
        buf.write(u"\2\2\u00e7\u00ee\5\60\31\2\u00e8\u00ee\t\6\2\2\u00e9")
        buf.write(u"\u00ee\t\7\2\2\u00ea\u00ee\7\'\2\2\u00eb\u00ee\7*\2\2")
        buf.write(u"\u00ec\u00ee\7#\2\2\u00ed\u00e3\3\2\2\2\u00ed\u00e7\3")
        buf.write(u"\2\2\2\u00ed\u00e8\3\2\2\2\u00ed\u00e9\3\2\2\2\u00ed")
        buf.write(u"\u00ea\3\2\2\2\u00ed\u00eb\3\2\2\2\u00ed\u00ec\3\2\2")
        buf.write(u"\2\u00ee/\3\2\2\2\u00ef\u00f1\7\37\2\2\u00f0\u00f2\5")
        buf.write(u"\62\32\2\u00f1\u00f0\3\2\2\2\u00f1\u00f2\3\2\2\2\u00f2")
        buf.write(u"\u00f3\3\2\2\2\u00f3\u00f4\7 \2\2\u00f4\61\3\2\2\2\u00f5")
        buf.write(u"\u00f7\5\64\33\2\u00f6\u00f5\3\2\2\2\u00f6\u00f7\3\2")
        buf.write(u"\2\2\u00f7\u00fe\3\2\2\2\u00f8\u00fa\7\31\2\2\u00f9\u00fb")
        buf.write(u"\5\64\33\2\u00fa\u00f9\3\2\2\2\u00fa\u00fb\3\2\2\2\u00fb")
        buf.write(u"\u00fd\3\2\2\2\u00fc\u00f8\3\2\2\2\u00fd\u0100\3\2\2")
        buf.write(u"\2\u00fe\u00fc\3\2\2\2\u00fe\u00ff\3\2\2\2\u00ff\63\3")
        buf.write(u"\2\2\2\u0100\u00fe\3\2\2\2\u0101\u0102\5.\30\2\u0102")
        buf.write(u"\65\3\2\2\2\35<ESXbr\u0089\u008b\u0094\u00b4\u00b8\u00bc")
        buf.write(u"\u00c0\u00c3\u00c7\u00ca\u00ce\u00d2\u00d5\u00d9\u00dd")
        buf.write(u"\u00e1\u00ed\u00f1\u00f6\u00fa\u00fe")
        return buf.getvalue()


class ExpressionParser ( Parser ):

    grammarFileName = "Expression.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ u"<INVALID>", u"<INVALID>", u"<INVALID>", u"<INVALID>", 
                     u"<INVALID>", u"<INVALID>", u"<INVALID>", u"<INVALID>", 
                     u"<INVALID>", u"'=='", u"'!='", u"'>'", u"'<'", u"'>='", 
                     u"'<='", u"'+'", u"'-'", u"'*'", u"'/'", u"'%'", u"'^'", 
                     u"'!'", u"';'", u"','", u"'='", u"'('", u"')'", u"'{'", 
                     u"'}'", u"'['", u"']'", u"<INVALID>", u"<INVALID>", 
                     u"'null'", u"<INVALID>", u"<INVALID>", u"'while'" ]

    symbolicNames = [ u"<INVALID>", u"FUNCTIONNAME_PARAM0", u"FUNCTIONNAME_PARAM1", 
                      u"FUNCTIONNAME_PARAM2", u"FUNCTIONNAME_PARAM3", u"FUNCTIONNAME_PARAM4", 
                      u"FUNCTIONNAME_PARAM_N", u"MOSAIC_OR", u"MOSAIC_AND", 
                      u"EQ", u"NEQ", u"GT", u"LT", u"GTEQ", u"LTEQ", u"PLUS", 
                      u"MINUS", u"MULT", u"DIV", u"MOD", u"POW", u"NOT", 
                      u"SCOL", u"COMMA", u"ASSIGN", u"LPAREN", u"RPAREN", 
                      u"OBRACE", u"CBRACE", u"BEGL", u"ENDL", u"MOSAIC_TRUE", 
                      u"MOSAIC_FALSE", u"NIL", u"MOSAIC_IF", u"MOSAIC_ELSE", 
                      u"WHILE", u"ID", u"INT", u"DOUBLE", u"STRING", u"COMMENT", 
                      u"SPACE", u"OTHER" ]

    RULE_parse = 0
    RULE_block = 1
    RULE_statements = 2
    RULE_assignment = 3
    RULE_ifStat = 4
    RULE_conditionBlock = 5
    RULE_statBlock = 6
    RULE_whileStat = 7
    RULE_statement = 8
    RULE_expr = 9
    RULE_functions = 10
    RULE_functionParam0 = 11
    RULE_functionParam1 = 12
    RULE_functionParam2 = 13
    RULE_functionParam3 = 14
    RULE_functionParam4 = 15
    RULE_functionParamN = 16
    RULE_argumentsN = 17
    RULE_arguments1 = 18
    RULE_arguments2 = 19
    RULE_arguments3 = 20
    RULE_arguments4 = 21
    RULE_atom = 22
    RULE_value_list = 23
    RULE_array = 24
    RULE_arrayElement = 25

    ruleNames =  [ u"parse", u"block", u"statements", u"assignment", u"ifStat", 
                   u"conditionBlock", u"statBlock", u"whileStat", u"statement", 
                   u"expr", u"functions", u"functionParam0", u"functionParam1", 
                   u"functionParam2", u"functionParam3", u"functionParam4", 
                   u"functionParamN", u"argumentsN", u"arguments1", u"arguments2", 
                   u"arguments3", u"arguments4", u"atom", u"value_list", 
                   u"array", u"arrayElement" ]

    EOF = Token.EOF
    FUNCTIONNAME_PARAM0=1
    FUNCTIONNAME_PARAM1=2
    FUNCTIONNAME_PARAM2=3
    FUNCTIONNAME_PARAM3=4
    FUNCTIONNAME_PARAM4=5
    FUNCTIONNAME_PARAM_N=6
    MOSAIC_OR=7
    MOSAIC_AND=8
    EQ=9
    NEQ=10
    GT=11
    LT=12
    GTEQ=13
    LTEQ=14
    PLUS=15
    MINUS=16
    MULT=17
    DIV=18
    MOD=19
    POW=20
    NOT=21
    SCOL=22
    COMMA=23
    ASSIGN=24
    LPAREN=25
    RPAREN=26
    OBRACE=27
    CBRACE=28
    BEGL=29
    ENDL=30
    MOSAIC_TRUE=31
    MOSAIC_FALSE=32
    NIL=33
    MOSAIC_IF=34
    MOSAIC_ELSE=35
    WHILE=36
    ID=37
    INT=38
    DOUBLE=39
    STRING=40
    COMMENT=41
    SPACE=42
    OTHER=43

    def __init__(self, input, output=sys.stdout):
        super(ExpressionParser, self).__init__(input, output=output)
        self.checkVersion("4.7.1")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None



    class ParseContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(ExpressionParser.ParseContext, self).__init__(parent, invokingState)
            self.parser = parser

        def block(self):
            return self.getTypedRuleContext(ExpressionParser.BlockContext,0)


        def EOF(self):
            return self.getToken(ExpressionParser.EOF, 0)

        def getRuleIndex(self):
            return ExpressionParser.RULE_parse

        def enterRule(self, listener):
            if hasattr(listener, "enterParse"):
                listener.enterParse(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitParse"):
                listener.exitParse(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitParse"):
                return visitor.visitParse(self)
            else:
                return visitor.visitChildren(self)




    def parse(self):

        localctx = ExpressionParser.ParseContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_parse)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 52
            self.block()
            self.state = 53
            self.match(ExpressionParser.EOF)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class BlockContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(ExpressionParser.BlockContext, self).__init__(parent, invokingState)
            self.parser = parser

        def statements(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(ExpressionParser.StatementsContext)
            else:
                return self.getTypedRuleContext(ExpressionParser.StatementsContext,i)


        def getRuleIndex(self):
            return ExpressionParser.RULE_block

        def enterRule(self, listener):
            if hasattr(listener, "enterBlock"):
                listener.enterBlock(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitBlock"):
                listener.exitBlock(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitBlock"):
                return visitor.visitBlock(self)
            else:
                return visitor.visitChildren(self)




    def block(self):

        localctx = ExpressionParser.BlockContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_block)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 58
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << ExpressionParser.FUNCTIONNAME_PARAM0) | (1 << ExpressionParser.FUNCTIONNAME_PARAM1) | (1 << ExpressionParser.FUNCTIONNAME_PARAM2) | (1 << ExpressionParser.FUNCTIONNAME_PARAM3) | (1 << ExpressionParser.FUNCTIONNAME_PARAM4) | (1 << ExpressionParser.FUNCTIONNAME_PARAM_N) | (1 << ExpressionParser.MINUS) | (1 << ExpressionParser.NOT) | (1 << ExpressionParser.LPAREN) | (1 << ExpressionParser.BEGL) | (1 << ExpressionParser.MOSAIC_TRUE) | (1 << ExpressionParser.MOSAIC_FALSE) | (1 << ExpressionParser.NIL) | (1 << ExpressionParser.MOSAIC_IF) | (1 << ExpressionParser.WHILE) | (1 << ExpressionParser.ID) | (1 << ExpressionParser.INT) | (1 << ExpressionParser.DOUBLE) | (1 << ExpressionParser.STRING) | (1 << ExpressionParser.OTHER))) != 0):
                self.state = 55
                self.statements()
                self.state = 60
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class StatementsContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(ExpressionParser.StatementsContext, self).__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return ExpressionParser.RULE_statements

     
        def copyFrom(self, ctx):
            super(ExpressionParser.StatementsContext, self).copyFrom(ctx)



    class AssignmentExprContext(StatementsContext):

        def __init__(self, parser, ctx): # actually a ExpressionParser.StatementsContext)
            super(ExpressionParser.AssignmentExprContext, self).__init__(parser)
            self.copyFrom(ctx)

        def assignment(self):
            return self.getTypedRuleContext(ExpressionParser.AssignmentContext,0)


        def enterRule(self, listener):
            if hasattr(listener, "enterAssignmentExpr"):
                listener.enterAssignmentExpr(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitAssignmentExpr"):
                listener.exitAssignmentExpr(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitAssignmentExpr"):
                return visitor.visitAssignmentExpr(self)
            else:
                return visitor.visitChildren(self)


    class WhileStatExprContext(StatementsContext):

        def __init__(self, parser, ctx): # actually a ExpressionParser.StatementsContext)
            super(ExpressionParser.WhileStatExprContext, self).__init__(parser)
            self.copyFrom(ctx)

        def whileStat(self):
            return self.getTypedRuleContext(ExpressionParser.WhileStatContext,0)


        def enterRule(self, listener):
            if hasattr(listener, "enterWhileStatExpr"):
                listener.enterWhileStatExpr(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitWhileStatExpr"):
                listener.exitWhileStatExpr(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitWhileStatExpr"):
                return visitor.visitWhileStatExpr(self)
            else:
                return visitor.visitChildren(self)


    class IfStatExprContext(StatementsContext):

        def __init__(self, parser, ctx): # actually a ExpressionParser.StatementsContext)
            super(ExpressionParser.IfStatExprContext, self).__init__(parser)
            self.copyFrom(ctx)

        def ifStat(self):
            return self.getTypedRuleContext(ExpressionParser.IfStatContext,0)


        def enterRule(self, listener):
            if hasattr(listener, "enterIfStatExpr"):
                listener.enterIfStatExpr(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitIfStatExpr"):
                listener.exitIfStatExpr(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitIfStatExpr"):
                return visitor.visitIfStatExpr(self)
            else:
                return visitor.visitChildren(self)


    class OtherExprContext(StatementsContext):

        def __init__(self, parser, ctx): # actually a ExpressionParser.StatementsContext)
            super(ExpressionParser.OtherExprContext, self).__init__(parser)
            self._OTHER = None # Token
            self.copyFrom(ctx)

        def OTHER(self):
            return self.getToken(ExpressionParser.OTHER, 0)

        def enterRule(self, listener):
            if hasattr(listener, "enterOtherExpr"):
                listener.enterOtherExpr(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitOtherExpr"):
                listener.exitOtherExpr(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitOtherExpr"):
                return visitor.visitOtherExpr(self)
            else:
                return visitor.visitChildren(self)


    class StatementExprContext(StatementsContext):

        def __init__(self, parser, ctx): # actually a ExpressionParser.StatementsContext)
            super(ExpressionParser.StatementExprContext, self).__init__(parser)
            self.copyFrom(ctx)

        def statement(self):
            return self.getTypedRuleContext(ExpressionParser.StatementContext,0)


        def enterRule(self, listener):
            if hasattr(listener, "enterStatementExpr"):
                listener.enterStatementExpr(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitStatementExpr"):
                listener.exitStatementExpr(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitStatementExpr"):
                return visitor.visitStatementExpr(self)
            else:
                return visitor.visitChildren(self)



    def statements(self):

        localctx = ExpressionParser.StatementsContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_statements)
        try:
            self.state = 67
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,1,self._ctx)
            if la_ == 1:
                localctx = ExpressionParser.AssignmentExprContext(self, localctx)
                self.enterOuterAlt(localctx, 1)
                self.state = 61
                self.assignment()
                pass

            elif la_ == 2:
                localctx = ExpressionParser.IfStatExprContext(self, localctx)
                self.enterOuterAlt(localctx, 2)
                self.state = 62
                self.ifStat()
                pass

            elif la_ == 3:
                localctx = ExpressionParser.WhileStatExprContext(self, localctx)
                self.enterOuterAlt(localctx, 3)
                self.state = 63
                self.whileStat()
                pass

            elif la_ == 4:
                localctx = ExpressionParser.StatementExprContext(self, localctx)
                self.enterOuterAlt(localctx, 4)
                self.state = 64
                self.statement()
                pass

            elif la_ == 5:
                localctx = ExpressionParser.OtherExprContext(self, localctx)
                self.enterOuterAlt(localctx, 5)
                self.state = 65
                localctx._OTHER = self.match(ExpressionParser.OTHER)
                System.err.println("unknown char: " + (None if localctx._OTHER is None else localctx._OTHER.text));
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class AssignmentContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(ExpressionParser.AssignmentContext, self).__init__(parent, invokingState)
            self.parser = parser

        def ID(self):
            return self.getToken(ExpressionParser.ID, 0)

        def ASSIGN(self):
            return self.getToken(ExpressionParser.ASSIGN, 0)

        def expr(self):
            return self.getTypedRuleContext(ExpressionParser.ExprContext,0)


        def SCOL(self):
            return self.getToken(ExpressionParser.SCOL, 0)

        def getRuleIndex(self):
            return ExpressionParser.RULE_assignment

        def enterRule(self, listener):
            if hasattr(listener, "enterAssignment"):
                listener.enterAssignment(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitAssignment"):
                listener.exitAssignment(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitAssignment"):
                return visitor.visitAssignment(self)
            else:
                return visitor.visitChildren(self)




    def assignment(self):

        localctx = ExpressionParser.AssignmentContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_assignment)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 69
            self.match(ExpressionParser.ID)
            self.state = 70
            self.match(ExpressionParser.ASSIGN)
            self.state = 71
            self.expr(0)
            self.state = 72
            self.match(ExpressionParser.SCOL)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class IfStatContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(ExpressionParser.IfStatContext, self).__init__(parent, invokingState)
            self.parser = parser

        def MOSAIC_IF(self, i=None):
            if i is None:
                return self.getTokens(ExpressionParser.MOSAIC_IF)
            else:
                return self.getToken(ExpressionParser.MOSAIC_IF, i)

        def conditionBlock(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(ExpressionParser.ConditionBlockContext)
            else:
                return self.getTypedRuleContext(ExpressionParser.ConditionBlockContext,i)


        def MOSAIC_ELSE(self, i=None):
            if i is None:
                return self.getTokens(ExpressionParser.MOSAIC_ELSE)
            else:
                return self.getToken(ExpressionParser.MOSAIC_ELSE, i)

        def statBlock(self):
            return self.getTypedRuleContext(ExpressionParser.StatBlockContext,0)


        def getRuleIndex(self):
            return ExpressionParser.RULE_ifStat

        def enterRule(self, listener):
            if hasattr(listener, "enterIfStat"):
                listener.enterIfStat(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitIfStat"):
                listener.exitIfStat(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitIfStat"):
                return visitor.visitIfStat(self)
            else:
                return visitor.visitChildren(self)




    def ifStat(self):

        localctx = ExpressionParser.IfStatContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_ifStat)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 74
            self.match(ExpressionParser.MOSAIC_IF)
            self.state = 75
            self.conditionBlock()
            self.state = 81
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,2,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 76
                    self.match(ExpressionParser.MOSAIC_ELSE)
                    self.state = 77
                    self.match(ExpressionParser.MOSAIC_IF)
                    self.state = 78
                    self.conditionBlock() 
                self.state = 83
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,2,self._ctx)

            self.state = 86
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,3,self._ctx)
            if la_ == 1:
                self.state = 84
                self.match(ExpressionParser.MOSAIC_ELSE)
                self.state = 85
                self.statBlock()


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class ConditionBlockContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(ExpressionParser.ConditionBlockContext, self).__init__(parent, invokingState)
            self.parser = parser

        def expr(self):
            return self.getTypedRuleContext(ExpressionParser.ExprContext,0)


        def statBlock(self):
            return self.getTypedRuleContext(ExpressionParser.StatBlockContext,0)


        def getRuleIndex(self):
            return ExpressionParser.RULE_conditionBlock

        def enterRule(self, listener):
            if hasattr(listener, "enterConditionBlock"):
                listener.enterConditionBlock(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitConditionBlock"):
                listener.exitConditionBlock(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitConditionBlock"):
                return visitor.visitConditionBlock(self)
            else:
                return visitor.visitChildren(self)




    def conditionBlock(self):

        localctx = ExpressionParser.ConditionBlockContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_conditionBlock)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 88
            self.expr(0)
            self.state = 89
            self.statBlock()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class StatBlockContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(ExpressionParser.StatBlockContext, self).__init__(parent, invokingState)
            self.parser = parser

        def OBRACE(self):
            return self.getToken(ExpressionParser.OBRACE, 0)

        def block(self):
            return self.getTypedRuleContext(ExpressionParser.BlockContext,0)


        def CBRACE(self):
            return self.getToken(ExpressionParser.CBRACE, 0)

        def statements(self):
            return self.getTypedRuleContext(ExpressionParser.StatementsContext,0)


        def getRuleIndex(self):
            return ExpressionParser.RULE_statBlock

        def enterRule(self, listener):
            if hasattr(listener, "enterStatBlock"):
                listener.enterStatBlock(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitStatBlock"):
                listener.exitStatBlock(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitStatBlock"):
                return visitor.visitStatBlock(self)
            else:
                return visitor.visitChildren(self)




    def statBlock(self):

        localctx = ExpressionParser.StatBlockContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_statBlock)
        try:
            self.state = 96
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [ExpressionParser.OBRACE]:
                self.enterOuterAlt(localctx, 1)
                self.state = 91
                self.match(ExpressionParser.OBRACE)
                self.state = 92
                self.block()
                self.state = 93
                self.match(ExpressionParser.CBRACE)
                pass
            elif token in [ExpressionParser.FUNCTIONNAME_PARAM0, ExpressionParser.FUNCTIONNAME_PARAM1, ExpressionParser.FUNCTIONNAME_PARAM2, ExpressionParser.FUNCTIONNAME_PARAM3, ExpressionParser.FUNCTIONNAME_PARAM4, ExpressionParser.FUNCTIONNAME_PARAM_N, ExpressionParser.MINUS, ExpressionParser.NOT, ExpressionParser.LPAREN, ExpressionParser.BEGL, ExpressionParser.MOSAIC_TRUE, ExpressionParser.MOSAIC_FALSE, ExpressionParser.NIL, ExpressionParser.MOSAIC_IF, ExpressionParser.WHILE, ExpressionParser.ID, ExpressionParser.INT, ExpressionParser.DOUBLE, ExpressionParser.STRING, ExpressionParser.OTHER]:
                self.enterOuterAlt(localctx, 2)
                self.state = 95
                self.statements()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class WhileStatContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(ExpressionParser.WhileStatContext, self).__init__(parent, invokingState)
            self.parser = parser

        def WHILE(self):
            return self.getToken(ExpressionParser.WHILE, 0)

        def expr(self):
            return self.getTypedRuleContext(ExpressionParser.ExprContext,0)


        def statBlock(self):
            return self.getTypedRuleContext(ExpressionParser.StatBlockContext,0)


        def getRuleIndex(self):
            return ExpressionParser.RULE_whileStat

        def enterRule(self, listener):
            if hasattr(listener, "enterWhileStat"):
                listener.enterWhileStat(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitWhileStat"):
                listener.exitWhileStat(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitWhileStat"):
                return visitor.visitWhileStat(self)
            else:
                return visitor.visitChildren(self)




    def whileStat(self):

        localctx = ExpressionParser.WhileStatContext(self, self._ctx, self.state)
        self.enterRule(localctx, 14, self.RULE_whileStat)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 98
            self.match(ExpressionParser.WHILE)
            self.state = 99
            self.expr(0)
            self.state = 100
            self.statBlock()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class StatementContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(ExpressionParser.StatementContext, self).__init__(parent, invokingState)
            self.parser = parser

        def expr(self):
            return self.getTypedRuleContext(ExpressionParser.ExprContext,0)


        def SCOL(self):
            return self.getToken(ExpressionParser.SCOL, 0)

        def getRuleIndex(self):
            return ExpressionParser.RULE_statement

        def enterRule(self, listener):
            if hasattr(listener, "enterStatement"):
                listener.enterStatement(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitStatement"):
                listener.exitStatement(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitStatement"):
                return visitor.visitStatement(self)
            else:
                return visitor.visitChildren(self)




    def statement(self):

        localctx = ExpressionParser.StatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 16, self.RULE_statement)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 102
            self.expr(0)
            self.state = 103
            self.match(ExpressionParser.SCOL)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class ExprContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(ExpressionParser.ExprContext, self).__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return ExpressionParser.RULE_expr

     
        def copyFrom(self, ctx):
            super(ExpressionParser.ExprContext, self).copyFrom(ctx)


    class NotExprContext(ExprContext):

        def __init__(self, parser, ctx): # actually a ExpressionParser.ExprContext)
            super(ExpressionParser.NotExprContext, self).__init__(parser)
            self.copyFrom(ctx)

        def NOT(self):
            return self.getToken(ExpressionParser.NOT, 0)
        def expr(self):
            return self.getTypedRuleContext(ExpressionParser.ExprContext,0)


        def enterRule(self, listener):
            if hasattr(listener, "enterNotExpr"):
                listener.enterNotExpr(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitNotExpr"):
                listener.exitNotExpr(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitNotExpr"):
                return visitor.visitNotExpr(self)
            else:
                return visitor.visitChildren(self)


    class UnaryMinusExprContext(ExprContext):

        def __init__(self, parser, ctx): # actually a ExpressionParser.ExprContext)
            super(ExpressionParser.UnaryMinusExprContext, self).__init__(parser)
            self.copyFrom(ctx)

        def MINUS(self):
            return self.getToken(ExpressionParser.MINUS, 0)
        def expr(self):
            return self.getTypedRuleContext(ExpressionParser.ExprContext,0)


        def enterRule(self, listener):
            if hasattr(listener, "enterUnaryMinusExpr"):
                listener.enterUnaryMinusExpr(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitUnaryMinusExpr"):
                listener.exitUnaryMinusExpr(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitUnaryMinusExpr"):
                return visitor.visitUnaryMinusExpr(self)
            else:
                return visitor.visitChildren(self)


    class MultiplicationExprContext(ExprContext):

        def __init__(self, parser, ctx): # actually a ExpressionParser.ExprContext)
            super(ExpressionParser.MultiplicationExprContext, self).__init__(parser)
            self.op = None # Token
            self.copyFrom(ctx)

        def expr(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(ExpressionParser.ExprContext)
            else:
                return self.getTypedRuleContext(ExpressionParser.ExprContext,i)

        def MULT(self):
            return self.getToken(ExpressionParser.MULT, 0)
        def DIV(self):
            return self.getToken(ExpressionParser.DIV, 0)
        def MOD(self):
            return self.getToken(ExpressionParser.MOD, 0)

        def enterRule(self, listener):
            if hasattr(listener, "enterMultiplicationExpr"):
                listener.enterMultiplicationExpr(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitMultiplicationExpr"):
                listener.exitMultiplicationExpr(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitMultiplicationExpr"):
                return visitor.visitMultiplicationExpr(self)
            else:
                return visitor.visitChildren(self)


    class AtomExprContext(ExprContext):

        def __init__(self, parser, ctx): # actually a ExpressionParser.ExprContext)
            super(ExpressionParser.AtomExprContext, self).__init__(parser)
            self.copyFrom(ctx)

        def atom(self):
            return self.getTypedRuleContext(ExpressionParser.AtomContext,0)


        def enterRule(self, listener):
            if hasattr(listener, "enterAtomExpr"):
                listener.enterAtomExpr(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitAtomExpr"):
                listener.exitAtomExpr(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitAtomExpr"):
                return visitor.visitAtomExpr(self)
            else:
                return visitor.visitChildren(self)


    class OrExprContext(ExprContext):

        def __init__(self, parser, ctx): # actually a ExpressionParser.ExprContext)
            super(ExpressionParser.OrExprContext, self).__init__(parser)
            self.copyFrom(ctx)

        def expr(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(ExpressionParser.ExprContext)
            else:
                return self.getTypedRuleContext(ExpressionParser.ExprContext,i)

        def MOSAIC_OR(self):
            return self.getToken(ExpressionParser.MOSAIC_OR, 0)

        def enterRule(self, listener):
            if hasattr(listener, "enterOrExpr"):
                listener.enterOrExpr(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitOrExpr"):
                listener.exitOrExpr(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitOrExpr"):
                return visitor.visitOrExpr(self)
            else:
                return visitor.visitChildren(self)


    class AdditiveExprContext(ExprContext):

        def __init__(self, parser, ctx): # actually a ExpressionParser.ExprContext)
            super(ExpressionParser.AdditiveExprContext, self).__init__(parser)
            self.op = None # Token
            self.copyFrom(ctx)

        def expr(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(ExpressionParser.ExprContext)
            else:
                return self.getTypedRuleContext(ExpressionParser.ExprContext,i)

        def PLUS(self):
            return self.getToken(ExpressionParser.PLUS, 0)
        def MINUS(self):
            return self.getToken(ExpressionParser.MINUS, 0)

        def enterRule(self, listener):
            if hasattr(listener, "enterAdditiveExpr"):
                listener.enterAdditiveExpr(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitAdditiveExpr"):
                listener.exitAdditiveExpr(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitAdditiveExpr"):
                return visitor.visitAdditiveExpr(self)
            else:
                return visitor.visitChildren(self)


    class PowExprContext(ExprContext):

        def __init__(self, parser, ctx): # actually a ExpressionParser.ExprContext)
            super(ExpressionParser.PowExprContext, self).__init__(parser)
            self.copyFrom(ctx)

        def expr(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(ExpressionParser.ExprContext)
            else:
                return self.getTypedRuleContext(ExpressionParser.ExprContext,i)

        def POW(self):
            return self.getToken(ExpressionParser.POW, 0)

        def enterRule(self, listener):
            if hasattr(listener, "enterPowExpr"):
                listener.enterPowExpr(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitPowExpr"):
                listener.exitPowExpr(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitPowExpr"):
                return visitor.visitPowExpr(self)
            else:
                return visitor.visitChildren(self)


    class RelationalExprContext(ExprContext):

        def __init__(self, parser, ctx): # actually a ExpressionParser.ExprContext)
            super(ExpressionParser.RelationalExprContext, self).__init__(parser)
            self.op = None # Token
            self.copyFrom(ctx)

        def expr(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(ExpressionParser.ExprContext)
            else:
                return self.getTypedRuleContext(ExpressionParser.ExprContext,i)

        def LTEQ(self):
            return self.getToken(ExpressionParser.LTEQ, 0)
        def GTEQ(self):
            return self.getToken(ExpressionParser.GTEQ, 0)
        def LT(self):
            return self.getToken(ExpressionParser.LT, 0)
        def GT(self):
            return self.getToken(ExpressionParser.GT, 0)

        def enterRule(self, listener):
            if hasattr(listener, "enterRelationalExpr"):
                listener.enterRelationalExpr(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitRelationalExpr"):
                listener.exitRelationalExpr(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitRelationalExpr"):
                return visitor.visitRelationalExpr(self)
            else:
                return visitor.visitChildren(self)


    class EqualityExprContext(ExprContext):

        def __init__(self, parser, ctx): # actually a ExpressionParser.ExprContext)
            super(ExpressionParser.EqualityExprContext, self).__init__(parser)
            self.op = None # Token
            self.copyFrom(ctx)

        def expr(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(ExpressionParser.ExprContext)
            else:
                return self.getTypedRuleContext(ExpressionParser.ExprContext,i)

        def EQ(self):
            return self.getToken(ExpressionParser.EQ, 0)
        def NEQ(self):
            return self.getToken(ExpressionParser.NEQ, 0)

        def enterRule(self, listener):
            if hasattr(listener, "enterEqualityExpr"):
                listener.enterEqualityExpr(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitEqualityExpr"):
                listener.exitEqualityExpr(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitEqualityExpr"):
                return visitor.visitEqualityExpr(self)
            else:
                return visitor.visitChildren(self)


    class FunctionExprContext(ExprContext):

        def __init__(self, parser, ctx): # actually a ExpressionParser.ExprContext)
            super(ExpressionParser.FunctionExprContext, self).__init__(parser)
            self.copyFrom(ctx)

        def functions(self):
            return self.getTypedRuleContext(ExpressionParser.FunctionsContext,0)


        def enterRule(self, listener):
            if hasattr(listener, "enterFunctionExpr"):
                listener.enterFunctionExpr(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitFunctionExpr"):
                listener.exitFunctionExpr(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitFunctionExpr"):
                return visitor.visitFunctionExpr(self)
            else:
                return visitor.visitChildren(self)


    class AndExprContext(ExprContext):

        def __init__(self, parser, ctx): # actually a ExpressionParser.ExprContext)
            super(ExpressionParser.AndExprContext, self).__init__(parser)
            self.copyFrom(ctx)

        def expr(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(ExpressionParser.ExprContext)
            else:
                return self.getTypedRuleContext(ExpressionParser.ExprContext,i)

        def MOSAIC_AND(self):
            return self.getToken(ExpressionParser.MOSAIC_AND, 0)

        def enterRule(self, listener):
            if hasattr(listener, "enterAndExpr"):
                listener.enterAndExpr(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitAndExpr"):
                listener.exitAndExpr(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitAndExpr"):
                return visitor.visitAndExpr(self)
            else:
                return visitor.visitChildren(self)



    def expr(self, _p=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = ExpressionParser.ExprContext(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 18
        self.enterRecursionRule(localctx, 18, self.RULE_expr, _p)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 112
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [ExpressionParser.FUNCTIONNAME_PARAM0, ExpressionParser.FUNCTIONNAME_PARAM1, ExpressionParser.FUNCTIONNAME_PARAM2, ExpressionParser.FUNCTIONNAME_PARAM3, ExpressionParser.FUNCTIONNAME_PARAM4, ExpressionParser.FUNCTIONNAME_PARAM_N]:
                localctx = ExpressionParser.FunctionExprContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx

                self.state = 106
                self.functions()
                pass
            elif token in [ExpressionParser.MINUS]:
                localctx = ExpressionParser.UnaryMinusExprContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 107
                self.match(ExpressionParser.MINUS)
                self.state = 108
                self.expr(9)
                pass
            elif token in [ExpressionParser.NOT]:
                localctx = ExpressionParser.NotExprContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 109
                self.match(ExpressionParser.NOT)
                self.state = 110
                self.expr(8)
                pass
            elif token in [ExpressionParser.LPAREN, ExpressionParser.BEGL, ExpressionParser.MOSAIC_TRUE, ExpressionParser.MOSAIC_FALSE, ExpressionParser.NIL, ExpressionParser.ID, ExpressionParser.INT, ExpressionParser.DOUBLE, ExpressionParser.STRING]:
                localctx = ExpressionParser.AtomExprContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 111
                self.atom()
                pass
            else:
                raise NoViableAltException(self)

            self._ctx.stop = self._input.LT(-1)
            self.state = 137
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,7,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    self.state = 135
                    self._errHandler.sync(self)
                    la_ = self._interp.adaptivePredict(self._input,6,self._ctx)
                    if la_ == 1:
                        localctx = ExpressionParser.PowExprContext(self, ExpressionParser.ExprContext(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr)
                        self.state = 114
                        if not self.precpred(self._ctx, 10):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 10)")
                        self.state = 115
                        self.match(ExpressionParser.POW)
                        self.state = 116
                        self.expr(11)
                        pass

                    elif la_ == 2:
                        localctx = ExpressionParser.MultiplicationExprContext(self, ExpressionParser.ExprContext(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr)
                        self.state = 117
                        if not self.precpred(self._ctx, 7):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 7)")
                        self.state = 118
                        localctx.op = self._input.LT(1)
                        _la = self._input.LA(1)
                        if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << ExpressionParser.MULT) | (1 << ExpressionParser.DIV) | (1 << ExpressionParser.MOD))) != 0)):
                            localctx.op = self._errHandler.recoverInline(self)
                        else:
                            self._errHandler.reportMatch(self)
                            self.consume()
                        self.state = 119
                        self.expr(8)
                        pass

                    elif la_ == 3:
                        localctx = ExpressionParser.AdditiveExprContext(self, ExpressionParser.ExprContext(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr)
                        self.state = 120
                        if not self.precpred(self._ctx, 6):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 6)")
                        self.state = 121
                        localctx.op = self._input.LT(1)
                        _la = self._input.LA(1)
                        if not(_la==ExpressionParser.PLUS or _la==ExpressionParser.MINUS):
                            localctx.op = self._errHandler.recoverInline(self)
                        else:
                            self._errHandler.reportMatch(self)
                            self.consume()
                        self.state = 122
                        self.expr(7)
                        pass

                    elif la_ == 4:
                        localctx = ExpressionParser.RelationalExprContext(self, ExpressionParser.ExprContext(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr)
                        self.state = 123
                        if not self.precpred(self._ctx, 5):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 5)")
                        self.state = 124
                        localctx.op = self._input.LT(1)
                        _la = self._input.LA(1)
                        if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << ExpressionParser.GT) | (1 << ExpressionParser.LT) | (1 << ExpressionParser.GTEQ) | (1 << ExpressionParser.LTEQ))) != 0)):
                            localctx.op = self._errHandler.recoverInline(self)
                        else:
                            self._errHandler.reportMatch(self)
                            self.consume()
                        self.state = 125
                        self.expr(6)
                        pass

                    elif la_ == 5:
                        localctx = ExpressionParser.EqualityExprContext(self, ExpressionParser.ExprContext(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr)
                        self.state = 126
                        if not self.precpred(self._ctx, 4):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 4)")
                        self.state = 127
                        localctx.op = self._input.LT(1)
                        _la = self._input.LA(1)
                        if not(_la==ExpressionParser.EQ or _la==ExpressionParser.NEQ):
                            localctx.op = self._errHandler.recoverInline(self)
                        else:
                            self._errHandler.reportMatch(self)
                            self.consume()
                        self.state = 128
                        self.expr(5)
                        pass

                    elif la_ == 6:
                        localctx = ExpressionParser.AndExprContext(self, ExpressionParser.ExprContext(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr)
                        self.state = 129
                        if not self.precpred(self._ctx, 3):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 3)")
                        self.state = 130
                        self.match(ExpressionParser.MOSAIC_AND)
                        self.state = 131
                        self.expr(4)
                        pass

                    elif la_ == 7:
                        localctx = ExpressionParser.OrExprContext(self, ExpressionParser.ExprContext(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr)
                        self.state = 132
                        if not self.precpred(self._ctx, 2):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 2)")
                        self.state = 133
                        self.match(ExpressionParser.MOSAIC_OR)
                        self.state = 134
                        self.expr(3)
                        pass

             
                self.state = 139
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,7,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx

    class FunctionsContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(ExpressionParser.FunctionsContext, self).__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return ExpressionParser.RULE_functions

     
        def copyFrom(self, ctx):
            super(ExpressionParser.FunctionsContext, self).copyFrom(ctx)



    class TwoParamterFunctionsContext(FunctionsContext):

        def __init__(self, parser, ctx): # actually a ExpressionParser.FunctionsContext)
            super(ExpressionParser.TwoParamterFunctionsContext, self).__init__(parser)
            self.copyFrom(ctx)

        def functionParam2(self):
            return self.getTypedRuleContext(ExpressionParser.FunctionParam2Context,0)


        def enterRule(self, listener):
            if hasattr(listener, "enterTwoParamterFunctions"):
                listener.enterTwoParamterFunctions(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitTwoParamterFunctions"):
                listener.exitTwoParamterFunctions(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitTwoParamterFunctions"):
                return visitor.visitTwoParamterFunctions(self)
            else:
                return visitor.visitChildren(self)


    class FourParamterFunctionsContext(FunctionsContext):

        def __init__(self, parser, ctx): # actually a ExpressionParser.FunctionsContext)
            super(ExpressionParser.FourParamterFunctionsContext, self).__init__(parser)
            self.copyFrom(ctx)

        def functionParam4(self):
            return self.getTypedRuleContext(ExpressionParser.FunctionParam4Context,0)


        def enterRule(self, listener):
            if hasattr(listener, "enterFourParamterFunctions"):
                listener.enterFourParamterFunctions(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitFourParamterFunctions"):
                listener.exitFourParamterFunctions(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitFourParamterFunctions"):
                return visitor.visitFourParamterFunctions(self)
            else:
                return visitor.visitChildren(self)


    class NParametersFunctionsContext(FunctionsContext):

        def __init__(self, parser, ctx): # actually a ExpressionParser.FunctionsContext)
            super(ExpressionParser.NParametersFunctionsContext, self).__init__(parser)
            self.copyFrom(ctx)

        def functionParamN(self):
            return self.getTypedRuleContext(ExpressionParser.FunctionParamNContext,0)


        def enterRule(self, listener):
            if hasattr(listener, "enterNParametersFunctions"):
                listener.enterNParametersFunctions(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitNParametersFunctions"):
                listener.exitNParametersFunctions(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitNParametersFunctions"):
                return visitor.visitNParametersFunctions(self)
            else:
                return visitor.visitChildren(self)


    class SingleParamterFunctionsContext(FunctionsContext):

        def __init__(self, parser, ctx): # actually a ExpressionParser.FunctionsContext)
            super(ExpressionParser.SingleParamterFunctionsContext, self).__init__(parser)
            self.copyFrom(ctx)

        def functionParam1(self):
            return self.getTypedRuleContext(ExpressionParser.FunctionParam1Context,0)


        def enterRule(self, listener):
            if hasattr(listener, "enterSingleParamterFunctions"):
                listener.enterSingleParamterFunctions(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitSingleParamterFunctions"):
                listener.exitSingleParamterFunctions(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitSingleParamterFunctions"):
                return visitor.visitSingleParamterFunctions(self)
            else:
                return visitor.visitChildren(self)


    class ZeroParamterFunctionsContext(FunctionsContext):

        def __init__(self, parser, ctx): # actually a ExpressionParser.FunctionsContext)
            super(ExpressionParser.ZeroParamterFunctionsContext, self).__init__(parser)
            self.copyFrom(ctx)

        def functionParam0(self):
            return self.getTypedRuleContext(ExpressionParser.FunctionParam0Context,0)


        def enterRule(self, listener):
            if hasattr(listener, "enterZeroParamterFunctions"):
                listener.enterZeroParamterFunctions(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitZeroParamterFunctions"):
                listener.exitZeroParamterFunctions(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitZeroParamterFunctions"):
                return visitor.visitZeroParamterFunctions(self)
            else:
                return visitor.visitChildren(self)


    class ThreeParamterFunctionsContext(FunctionsContext):

        def __init__(self, parser, ctx): # actually a ExpressionParser.FunctionsContext)
            super(ExpressionParser.ThreeParamterFunctionsContext, self).__init__(parser)
            self.copyFrom(ctx)

        def functionParam3(self):
            return self.getTypedRuleContext(ExpressionParser.FunctionParam3Context,0)


        def enterRule(self, listener):
            if hasattr(listener, "enterThreeParamterFunctions"):
                listener.enterThreeParamterFunctions(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitThreeParamterFunctions"):
                listener.exitThreeParamterFunctions(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitThreeParamterFunctions"):
                return visitor.visitThreeParamterFunctions(self)
            else:
                return visitor.visitChildren(self)



    def functions(self):

        localctx = ExpressionParser.FunctionsContext(self, self._ctx, self.state)
        self.enterRule(localctx, 20, self.RULE_functions)
        try:
            self.state = 146
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [ExpressionParser.FUNCTIONNAME_PARAM0]:
                localctx = ExpressionParser.ZeroParamterFunctionsContext(self, localctx)
                self.enterOuterAlt(localctx, 1)
                self.state = 140
                self.functionParam0()
                pass
            elif token in [ExpressionParser.FUNCTIONNAME_PARAM1]:
                localctx = ExpressionParser.SingleParamterFunctionsContext(self, localctx)
                self.enterOuterAlt(localctx, 2)
                self.state = 141
                self.functionParam1()
                pass
            elif token in [ExpressionParser.FUNCTIONNAME_PARAM2]:
                localctx = ExpressionParser.TwoParamterFunctionsContext(self, localctx)
                self.enterOuterAlt(localctx, 3)
                self.state = 142
                self.functionParam2()
                pass
            elif token in [ExpressionParser.FUNCTIONNAME_PARAM3]:
                localctx = ExpressionParser.ThreeParamterFunctionsContext(self, localctx)
                self.enterOuterAlt(localctx, 4)
                self.state = 143
                self.functionParam3()
                pass
            elif token in [ExpressionParser.FUNCTIONNAME_PARAM4]:
                localctx = ExpressionParser.FourParamterFunctionsContext(self, localctx)
                self.enterOuterAlt(localctx, 5)
                self.state = 144
                self.functionParam4()
                pass
            elif token in [ExpressionParser.FUNCTIONNAME_PARAM_N]:
                localctx = ExpressionParser.NParametersFunctionsContext(self, localctx)
                self.enterOuterAlt(localctx, 6)
                self.state = 145
                self.functionParamN()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class FunctionParam0Context(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(ExpressionParser.FunctionParam0Context, self).__init__(parent, invokingState)
            self.parser = parser

        def FUNCTIONNAME_PARAM0(self):
            return self.getToken(ExpressionParser.FUNCTIONNAME_PARAM0, 0)

        def LPAREN(self):
            return self.getToken(ExpressionParser.LPAREN, 0)

        def RPAREN(self):
            return self.getToken(ExpressionParser.RPAREN, 0)

        def getRuleIndex(self):
            return ExpressionParser.RULE_functionParam0

        def enterRule(self, listener):
            if hasattr(listener, "enterFunctionParam0"):
                listener.enterFunctionParam0(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitFunctionParam0"):
                listener.exitFunctionParam0(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitFunctionParam0"):
                return visitor.visitFunctionParam0(self)
            else:
                return visitor.visitChildren(self)




    def functionParam0(self):

        localctx = ExpressionParser.FunctionParam0Context(self, self._ctx, self.state)
        self.enterRule(localctx, 22, self.RULE_functionParam0)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 148
            self.match(ExpressionParser.FUNCTIONNAME_PARAM0)
            self.state = 149
            self.match(ExpressionParser.LPAREN)
            self.state = 150
            self.match(ExpressionParser.RPAREN)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class FunctionParam1Context(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(ExpressionParser.FunctionParam1Context, self).__init__(parent, invokingState)
            self.parser = parser

        def FUNCTIONNAME_PARAM1(self):
            return self.getToken(ExpressionParser.FUNCTIONNAME_PARAM1, 0)

        def LPAREN(self):
            return self.getToken(ExpressionParser.LPAREN, 0)

        def arguments1(self):
            return self.getTypedRuleContext(ExpressionParser.Arguments1Context,0)


        def RPAREN(self):
            return self.getToken(ExpressionParser.RPAREN, 0)

        def getRuleIndex(self):
            return ExpressionParser.RULE_functionParam1

        def enterRule(self, listener):
            if hasattr(listener, "enterFunctionParam1"):
                listener.enterFunctionParam1(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitFunctionParam1"):
                listener.exitFunctionParam1(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitFunctionParam1"):
                return visitor.visitFunctionParam1(self)
            else:
                return visitor.visitChildren(self)




    def functionParam1(self):

        localctx = ExpressionParser.FunctionParam1Context(self, self._ctx, self.state)
        self.enterRule(localctx, 24, self.RULE_functionParam1)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 152
            self.match(ExpressionParser.FUNCTIONNAME_PARAM1)
            self.state = 153
            self.match(ExpressionParser.LPAREN)
            self.state = 154
            self.arguments1()
            self.state = 155
            self.match(ExpressionParser.RPAREN)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class FunctionParam2Context(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(ExpressionParser.FunctionParam2Context, self).__init__(parent, invokingState)
            self.parser = parser

        def FUNCTIONNAME_PARAM2(self):
            return self.getToken(ExpressionParser.FUNCTIONNAME_PARAM2, 0)

        def LPAREN(self):
            return self.getToken(ExpressionParser.LPAREN, 0)

        def arguments2(self):
            return self.getTypedRuleContext(ExpressionParser.Arguments2Context,0)


        def RPAREN(self):
            return self.getToken(ExpressionParser.RPAREN, 0)

        def getRuleIndex(self):
            return ExpressionParser.RULE_functionParam2

        def enterRule(self, listener):
            if hasattr(listener, "enterFunctionParam2"):
                listener.enterFunctionParam2(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitFunctionParam2"):
                listener.exitFunctionParam2(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitFunctionParam2"):
                return visitor.visitFunctionParam2(self)
            else:
                return visitor.visitChildren(self)




    def functionParam2(self):

        localctx = ExpressionParser.FunctionParam2Context(self, self._ctx, self.state)
        self.enterRule(localctx, 26, self.RULE_functionParam2)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 157
            self.match(ExpressionParser.FUNCTIONNAME_PARAM2)
            self.state = 158
            self.match(ExpressionParser.LPAREN)
            self.state = 159
            self.arguments2()
            self.state = 160
            self.match(ExpressionParser.RPAREN)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class FunctionParam3Context(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(ExpressionParser.FunctionParam3Context, self).__init__(parent, invokingState)
            self.parser = parser

        def FUNCTIONNAME_PARAM3(self):
            return self.getToken(ExpressionParser.FUNCTIONNAME_PARAM3, 0)

        def LPAREN(self):
            return self.getToken(ExpressionParser.LPAREN, 0)

        def arguments3(self):
            return self.getTypedRuleContext(ExpressionParser.Arguments3Context,0)


        def RPAREN(self):
            return self.getToken(ExpressionParser.RPAREN, 0)

        def getRuleIndex(self):
            return ExpressionParser.RULE_functionParam3

        def enterRule(self, listener):
            if hasattr(listener, "enterFunctionParam3"):
                listener.enterFunctionParam3(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitFunctionParam3"):
                listener.exitFunctionParam3(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitFunctionParam3"):
                return visitor.visitFunctionParam3(self)
            else:
                return visitor.visitChildren(self)




    def functionParam3(self):

        localctx = ExpressionParser.FunctionParam3Context(self, self._ctx, self.state)
        self.enterRule(localctx, 28, self.RULE_functionParam3)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 162
            self.match(ExpressionParser.FUNCTIONNAME_PARAM3)
            self.state = 163
            self.match(ExpressionParser.LPAREN)
            self.state = 164
            self.arguments3()
            self.state = 165
            self.match(ExpressionParser.RPAREN)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class FunctionParam4Context(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(ExpressionParser.FunctionParam4Context, self).__init__(parent, invokingState)
            self.parser = parser

        def FUNCTIONNAME_PARAM4(self):
            return self.getToken(ExpressionParser.FUNCTIONNAME_PARAM4, 0)

        def LPAREN(self):
            return self.getToken(ExpressionParser.LPAREN, 0)

        def arguments4(self):
            return self.getTypedRuleContext(ExpressionParser.Arguments4Context,0)


        def RPAREN(self):
            return self.getToken(ExpressionParser.RPAREN, 0)

        def getRuleIndex(self):
            return ExpressionParser.RULE_functionParam4

        def enterRule(self, listener):
            if hasattr(listener, "enterFunctionParam4"):
                listener.enterFunctionParam4(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitFunctionParam4"):
                listener.exitFunctionParam4(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitFunctionParam4"):
                return visitor.visitFunctionParam4(self)
            else:
                return visitor.visitChildren(self)




    def functionParam4(self):

        localctx = ExpressionParser.FunctionParam4Context(self, self._ctx, self.state)
        self.enterRule(localctx, 30, self.RULE_functionParam4)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 167
            self.match(ExpressionParser.FUNCTIONNAME_PARAM4)
            self.state = 168
            self.match(ExpressionParser.LPAREN)
            self.state = 169
            self.arguments4()
            self.state = 170
            self.match(ExpressionParser.RPAREN)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class FunctionParamNContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(ExpressionParser.FunctionParamNContext, self).__init__(parent, invokingState)
            self.parser = parser

        def FUNCTIONNAME_PARAM_N(self):
            return self.getToken(ExpressionParser.FUNCTIONNAME_PARAM_N, 0)

        def LPAREN(self):
            return self.getToken(ExpressionParser.LPAREN, 0)

        def argumentsN(self):
            return self.getTypedRuleContext(ExpressionParser.ArgumentsNContext,0)


        def RPAREN(self):
            return self.getToken(ExpressionParser.RPAREN, 0)

        def getRuleIndex(self):
            return ExpressionParser.RULE_functionParamN

        def enterRule(self, listener):
            if hasattr(listener, "enterFunctionParamN"):
                listener.enterFunctionParamN(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitFunctionParamN"):
                listener.exitFunctionParamN(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitFunctionParamN"):
                return visitor.visitFunctionParamN(self)
            else:
                return visitor.visitChildren(self)




    def functionParamN(self):

        localctx = ExpressionParser.FunctionParamNContext(self, self._ctx, self.state)
        self.enterRule(localctx, 32, self.RULE_functionParamN)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 172
            self.match(ExpressionParser.FUNCTIONNAME_PARAM_N)
            self.state = 173
            self.match(ExpressionParser.LPAREN)
            self.state = 174
            self.argumentsN()
            self.state = 175
            self.match(ExpressionParser.RPAREN)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class ArgumentsNContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(ExpressionParser.ArgumentsNContext, self).__init__(parent, invokingState)
            self.parser = parser

        def expr(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(ExpressionParser.ExprContext)
            else:
                return self.getTypedRuleContext(ExpressionParser.ExprContext,i)


        def getRuleIndex(self):
            return ExpressionParser.RULE_argumentsN

        def enterRule(self, listener):
            if hasattr(listener, "enterArgumentsN"):
                listener.enterArgumentsN(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitArgumentsN"):
                listener.exitArgumentsN(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitArgumentsN"):
                return visitor.visitArgumentsN(self)
            else:
                return visitor.visitChildren(self)




    def argumentsN(self):

        localctx = ExpressionParser.ArgumentsNContext(self, self._ctx, self.state)
        self.enterRule(localctx, 34, self.RULE_argumentsN)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 178
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << ExpressionParser.FUNCTIONNAME_PARAM0) | (1 << ExpressionParser.FUNCTIONNAME_PARAM1) | (1 << ExpressionParser.FUNCTIONNAME_PARAM2) | (1 << ExpressionParser.FUNCTIONNAME_PARAM3) | (1 << ExpressionParser.FUNCTIONNAME_PARAM4) | (1 << ExpressionParser.FUNCTIONNAME_PARAM_N) | (1 << ExpressionParser.MINUS) | (1 << ExpressionParser.NOT) | (1 << ExpressionParser.LPAREN) | (1 << ExpressionParser.BEGL) | (1 << ExpressionParser.MOSAIC_TRUE) | (1 << ExpressionParser.MOSAIC_FALSE) | (1 << ExpressionParser.NIL) | (1 << ExpressionParser.ID) | (1 << ExpressionParser.INT) | (1 << ExpressionParser.DOUBLE) | (1 << ExpressionParser.STRING))) != 0):
                self.state = 177
                self.expr(0)


            self.state = 186
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==ExpressionParser.COMMA:
                self.state = 180
                self.match(ExpressionParser.COMMA)
                self.state = 182
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << ExpressionParser.FUNCTIONNAME_PARAM0) | (1 << ExpressionParser.FUNCTIONNAME_PARAM1) | (1 << ExpressionParser.FUNCTIONNAME_PARAM2) | (1 << ExpressionParser.FUNCTIONNAME_PARAM3) | (1 << ExpressionParser.FUNCTIONNAME_PARAM4) | (1 << ExpressionParser.FUNCTIONNAME_PARAM_N) | (1 << ExpressionParser.MINUS) | (1 << ExpressionParser.NOT) | (1 << ExpressionParser.LPAREN) | (1 << ExpressionParser.BEGL) | (1 << ExpressionParser.MOSAIC_TRUE) | (1 << ExpressionParser.MOSAIC_FALSE) | (1 << ExpressionParser.NIL) | (1 << ExpressionParser.ID) | (1 << ExpressionParser.INT) | (1 << ExpressionParser.DOUBLE) | (1 << ExpressionParser.STRING))) != 0):
                    self.state = 181
                    self.expr(0)


                self.state = 188
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Arguments1Context(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(ExpressionParser.Arguments1Context, self).__init__(parent, invokingState)
            self.parser = parser
            self.param1 = None # ExprContext

        def expr(self):
            return self.getTypedRuleContext(ExpressionParser.ExprContext,0)


        def getRuleIndex(self):
            return ExpressionParser.RULE_arguments1

        def enterRule(self, listener):
            if hasattr(listener, "enterArguments1"):
                listener.enterArguments1(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitArguments1"):
                listener.exitArguments1(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitArguments1"):
                return visitor.visitArguments1(self)
            else:
                return visitor.visitChildren(self)




    def arguments1(self):

        localctx = ExpressionParser.Arguments1Context(self, self._ctx, self.state)
        self.enterRule(localctx, 36, self.RULE_arguments1)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 190
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << ExpressionParser.FUNCTIONNAME_PARAM0) | (1 << ExpressionParser.FUNCTIONNAME_PARAM1) | (1 << ExpressionParser.FUNCTIONNAME_PARAM2) | (1 << ExpressionParser.FUNCTIONNAME_PARAM3) | (1 << ExpressionParser.FUNCTIONNAME_PARAM4) | (1 << ExpressionParser.FUNCTIONNAME_PARAM_N) | (1 << ExpressionParser.MINUS) | (1 << ExpressionParser.NOT) | (1 << ExpressionParser.LPAREN) | (1 << ExpressionParser.BEGL) | (1 << ExpressionParser.MOSAIC_TRUE) | (1 << ExpressionParser.MOSAIC_FALSE) | (1 << ExpressionParser.NIL) | (1 << ExpressionParser.ID) | (1 << ExpressionParser.INT) | (1 << ExpressionParser.DOUBLE) | (1 << ExpressionParser.STRING))) != 0):
                self.state = 189
                localctx.param1 = self.expr(0)


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Arguments2Context(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(ExpressionParser.Arguments2Context, self).__init__(parent, invokingState)
            self.parser = parser
            self.param1 = None # ExprContext
            self.param2 = None # ExprContext

        def COMMA(self):
            return self.getToken(ExpressionParser.COMMA, 0)

        def expr(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(ExpressionParser.ExprContext)
            else:
                return self.getTypedRuleContext(ExpressionParser.ExprContext,i)


        def getRuleIndex(self):
            return ExpressionParser.RULE_arguments2

        def enterRule(self, listener):
            if hasattr(listener, "enterArguments2"):
                listener.enterArguments2(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitArguments2"):
                listener.exitArguments2(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitArguments2"):
                return visitor.visitArguments2(self)
            else:
                return visitor.visitChildren(self)




    def arguments2(self):

        localctx = ExpressionParser.Arguments2Context(self, self._ctx, self.state)
        self.enterRule(localctx, 38, self.RULE_arguments2)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 193
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << ExpressionParser.FUNCTIONNAME_PARAM0) | (1 << ExpressionParser.FUNCTIONNAME_PARAM1) | (1 << ExpressionParser.FUNCTIONNAME_PARAM2) | (1 << ExpressionParser.FUNCTIONNAME_PARAM3) | (1 << ExpressionParser.FUNCTIONNAME_PARAM4) | (1 << ExpressionParser.FUNCTIONNAME_PARAM_N) | (1 << ExpressionParser.MINUS) | (1 << ExpressionParser.NOT) | (1 << ExpressionParser.LPAREN) | (1 << ExpressionParser.BEGL) | (1 << ExpressionParser.MOSAIC_TRUE) | (1 << ExpressionParser.MOSAIC_FALSE) | (1 << ExpressionParser.NIL) | (1 << ExpressionParser.ID) | (1 << ExpressionParser.INT) | (1 << ExpressionParser.DOUBLE) | (1 << ExpressionParser.STRING))) != 0):
                self.state = 192
                localctx.param1 = self.expr(0)


            self.state = 195
            self.match(ExpressionParser.COMMA)
            self.state = 197
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << ExpressionParser.FUNCTIONNAME_PARAM0) | (1 << ExpressionParser.FUNCTIONNAME_PARAM1) | (1 << ExpressionParser.FUNCTIONNAME_PARAM2) | (1 << ExpressionParser.FUNCTIONNAME_PARAM3) | (1 << ExpressionParser.FUNCTIONNAME_PARAM4) | (1 << ExpressionParser.FUNCTIONNAME_PARAM_N) | (1 << ExpressionParser.MINUS) | (1 << ExpressionParser.NOT) | (1 << ExpressionParser.LPAREN) | (1 << ExpressionParser.BEGL) | (1 << ExpressionParser.MOSAIC_TRUE) | (1 << ExpressionParser.MOSAIC_FALSE) | (1 << ExpressionParser.NIL) | (1 << ExpressionParser.ID) | (1 << ExpressionParser.INT) | (1 << ExpressionParser.DOUBLE) | (1 << ExpressionParser.STRING))) != 0):
                self.state = 196
                localctx.param2 = self.expr(0)


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Arguments3Context(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(ExpressionParser.Arguments3Context, self).__init__(parent, invokingState)
            self.parser = parser
            self.param1 = None # ExprContext
            self.param2 = None # ExprContext
            self.param3 = None # ExprContext

        def COMMA(self, i=None):
            if i is None:
                return self.getTokens(ExpressionParser.COMMA)
            else:
                return self.getToken(ExpressionParser.COMMA, i)

        def expr(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(ExpressionParser.ExprContext)
            else:
                return self.getTypedRuleContext(ExpressionParser.ExprContext,i)


        def getRuleIndex(self):
            return ExpressionParser.RULE_arguments3

        def enterRule(self, listener):
            if hasattr(listener, "enterArguments3"):
                listener.enterArguments3(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitArguments3"):
                listener.exitArguments3(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitArguments3"):
                return visitor.visitArguments3(self)
            else:
                return visitor.visitChildren(self)




    def arguments3(self):

        localctx = ExpressionParser.Arguments3Context(self, self._ctx, self.state)
        self.enterRule(localctx, 40, self.RULE_arguments3)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 200
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << ExpressionParser.FUNCTIONNAME_PARAM0) | (1 << ExpressionParser.FUNCTIONNAME_PARAM1) | (1 << ExpressionParser.FUNCTIONNAME_PARAM2) | (1 << ExpressionParser.FUNCTIONNAME_PARAM3) | (1 << ExpressionParser.FUNCTIONNAME_PARAM4) | (1 << ExpressionParser.FUNCTIONNAME_PARAM_N) | (1 << ExpressionParser.MINUS) | (1 << ExpressionParser.NOT) | (1 << ExpressionParser.LPAREN) | (1 << ExpressionParser.BEGL) | (1 << ExpressionParser.MOSAIC_TRUE) | (1 << ExpressionParser.MOSAIC_FALSE) | (1 << ExpressionParser.NIL) | (1 << ExpressionParser.ID) | (1 << ExpressionParser.INT) | (1 << ExpressionParser.DOUBLE) | (1 << ExpressionParser.STRING))) != 0):
                self.state = 199
                localctx.param1 = self.expr(0)


            self.state = 202
            self.match(ExpressionParser.COMMA)
            self.state = 204
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << ExpressionParser.FUNCTIONNAME_PARAM0) | (1 << ExpressionParser.FUNCTIONNAME_PARAM1) | (1 << ExpressionParser.FUNCTIONNAME_PARAM2) | (1 << ExpressionParser.FUNCTIONNAME_PARAM3) | (1 << ExpressionParser.FUNCTIONNAME_PARAM4) | (1 << ExpressionParser.FUNCTIONNAME_PARAM_N) | (1 << ExpressionParser.MINUS) | (1 << ExpressionParser.NOT) | (1 << ExpressionParser.LPAREN) | (1 << ExpressionParser.BEGL) | (1 << ExpressionParser.MOSAIC_TRUE) | (1 << ExpressionParser.MOSAIC_FALSE) | (1 << ExpressionParser.NIL) | (1 << ExpressionParser.ID) | (1 << ExpressionParser.INT) | (1 << ExpressionParser.DOUBLE) | (1 << ExpressionParser.STRING))) != 0):
                self.state = 203
                localctx.param2 = self.expr(0)


            self.state = 206
            self.match(ExpressionParser.COMMA)
            self.state = 208
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << ExpressionParser.FUNCTIONNAME_PARAM0) | (1 << ExpressionParser.FUNCTIONNAME_PARAM1) | (1 << ExpressionParser.FUNCTIONNAME_PARAM2) | (1 << ExpressionParser.FUNCTIONNAME_PARAM3) | (1 << ExpressionParser.FUNCTIONNAME_PARAM4) | (1 << ExpressionParser.FUNCTIONNAME_PARAM_N) | (1 << ExpressionParser.MINUS) | (1 << ExpressionParser.NOT) | (1 << ExpressionParser.LPAREN) | (1 << ExpressionParser.BEGL) | (1 << ExpressionParser.MOSAIC_TRUE) | (1 << ExpressionParser.MOSAIC_FALSE) | (1 << ExpressionParser.NIL) | (1 << ExpressionParser.ID) | (1 << ExpressionParser.INT) | (1 << ExpressionParser.DOUBLE) | (1 << ExpressionParser.STRING))) != 0):
                self.state = 207
                localctx.param3 = self.expr(0)


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Arguments4Context(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(ExpressionParser.Arguments4Context, self).__init__(parent, invokingState)
            self.parser = parser
            self.param1 = None # ExprContext
            self.param2 = None # ExprContext
            self.param3 = None # ExprContext
            self.param4 = None # ExprContext

        def COMMA(self, i=None):
            if i is None:
                return self.getTokens(ExpressionParser.COMMA)
            else:
                return self.getToken(ExpressionParser.COMMA, i)

        def expr(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(ExpressionParser.ExprContext)
            else:
                return self.getTypedRuleContext(ExpressionParser.ExprContext,i)


        def getRuleIndex(self):
            return ExpressionParser.RULE_arguments4

        def enterRule(self, listener):
            if hasattr(listener, "enterArguments4"):
                listener.enterArguments4(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitArguments4"):
                listener.exitArguments4(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitArguments4"):
                return visitor.visitArguments4(self)
            else:
                return visitor.visitChildren(self)




    def arguments4(self):

        localctx = ExpressionParser.Arguments4Context(self, self._ctx, self.state)
        self.enterRule(localctx, 42, self.RULE_arguments4)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 211
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << ExpressionParser.FUNCTIONNAME_PARAM0) | (1 << ExpressionParser.FUNCTIONNAME_PARAM1) | (1 << ExpressionParser.FUNCTIONNAME_PARAM2) | (1 << ExpressionParser.FUNCTIONNAME_PARAM3) | (1 << ExpressionParser.FUNCTIONNAME_PARAM4) | (1 << ExpressionParser.FUNCTIONNAME_PARAM_N) | (1 << ExpressionParser.MINUS) | (1 << ExpressionParser.NOT) | (1 << ExpressionParser.LPAREN) | (1 << ExpressionParser.BEGL) | (1 << ExpressionParser.MOSAIC_TRUE) | (1 << ExpressionParser.MOSAIC_FALSE) | (1 << ExpressionParser.NIL) | (1 << ExpressionParser.ID) | (1 << ExpressionParser.INT) | (1 << ExpressionParser.DOUBLE) | (1 << ExpressionParser.STRING))) != 0):
                self.state = 210
                localctx.param1 = self.expr(0)


            self.state = 213
            self.match(ExpressionParser.COMMA)
            self.state = 215
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << ExpressionParser.FUNCTIONNAME_PARAM0) | (1 << ExpressionParser.FUNCTIONNAME_PARAM1) | (1 << ExpressionParser.FUNCTIONNAME_PARAM2) | (1 << ExpressionParser.FUNCTIONNAME_PARAM3) | (1 << ExpressionParser.FUNCTIONNAME_PARAM4) | (1 << ExpressionParser.FUNCTIONNAME_PARAM_N) | (1 << ExpressionParser.MINUS) | (1 << ExpressionParser.NOT) | (1 << ExpressionParser.LPAREN) | (1 << ExpressionParser.BEGL) | (1 << ExpressionParser.MOSAIC_TRUE) | (1 << ExpressionParser.MOSAIC_FALSE) | (1 << ExpressionParser.NIL) | (1 << ExpressionParser.ID) | (1 << ExpressionParser.INT) | (1 << ExpressionParser.DOUBLE) | (1 << ExpressionParser.STRING))) != 0):
                self.state = 214
                localctx.param2 = self.expr(0)


            self.state = 217
            self.match(ExpressionParser.COMMA)
            self.state = 219
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << ExpressionParser.FUNCTIONNAME_PARAM0) | (1 << ExpressionParser.FUNCTIONNAME_PARAM1) | (1 << ExpressionParser.FUNCTIONNAME_PARAM2) | (1 << ExpressionParser.FUNCTIONNAME_PARAM3) | (1 << ExpressionParser.FUNCTIONNAME_PARAM4) | (1 << ExpressionParser.FUNCTIONNAME_PARAM_N) | (1 << ExpressionParser.MINUS) | (1 << ExpressionParser.NOT) | (1 << ExpressionParser.LPAREN) | (1 << ExpressionParser.BEGL) | (1 << ExpressionParser.MOSAIC_TRUE) | (1 << ExpressionParser.MOSAIC_FALSE) | (1 << ExpressionParser.NIL) | (1 << ExpressionParser.ID) | (1 << ExpressionParser.INT) | (1 << ExpressionParser.DOUBLE) | (1 << ExpressionParser.STRING))) != 0):
                self.state = 218
                localctx.param3 = self.expr(0)


            self.state = 221
            self.match(ExpressionParser.COMMA)
            self.state = 223
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << ExpressionParser.FUNCTIONNAME_PARAM0) | (1 << ExpressionParser.FUNCTIONNAME_PARAM1) | (1 << ExpressionParser.FUNCTIONNAME_PARAM2) | (1 << ExpressionParser.FUNCTIONNAME_PARAM3) | (1 << ExpressionParser.FUNCTIONNAME_PARAM4) | (1 << ExpressionParser.FUNCTIONNAME_PARAM_N) | (1 << ExpressionParser.MINUS) | (1 << ExpressionParser.NOT) | (1 << ExpressionParser.LPAREN) | (1 << ExpressionParser.BEGL) | (1 << ExpressionParser.MOSAIC_TRUE) | (1 << ExpressionParser.MOSAIC_FALSE) | (1 << ExpressionParser.NIL) | (1 << ExpressionParser.ID) | (1 << ExpressionParser.INT) | (1 << ExpressionParser.DOUBLE) | (1 << ExpressionParser.STRING))) != 0):
                self.state = 222
                localctx.param4 = self.expr(0)


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class AtomContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(ExpressionParser.AtomContext, self).__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return ExpressionParser.RULE_atom

     
        def copyFrom(self, ctx):
            super(ExpressionParser.AtomContext, self).copyFrom(ctx)



    class ParExprContext(AtomContext):

        def __init__(self, parser, ctx): # actually a ExpressionParser.AtomContext)
            super(ExpressionParser.ParExprContext, self).__init__(parser)
            self.copyFrom(ctx)

        def LPAREN(self):
            return self.getToken(ExpressionParser.LPAREN, 0)
        def expr(self):
            return self.getTypedRuleContext(ExpressionParser.ExprContext,0)

        def RPAREN(self):
            return self.getToken(ExpressionParser.RPAREN, 0)

        def enterRule(self, listener):
            if hasattr(listener, "enterParExpr"):
                listener.enterParExpr(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitParExpr"):
                listener.exitParExpr(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitParExpr"):
                return visitor.visitParExpr(self)
            else:
                return visitor.visitChildren(self)


    class BooleanAtomContext(AtomContext):

        def __init__(self, parser, ctx): # actually a ExpressionParser.AtomContext)
            super(ExpressionParser.BooleanAtomContext, self).__init__(parser)
            self.copyFrom(ctx)

        def MOSAIC_TRUE(self):
            return self.getToken(ExpressionParser.MOSAIC_TRUE, 0)
        def MOSAIC_FALSE(self):
            return self.getToken(ExpressionParser.MOSAIC_FALSE, 0)

        def enterRule(self, listener):
            if hasattr(listener, "enterBooleanAtom"):
                listener.enterBooleanAtom(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitBooleanAtom"):
                listener.exitBooleanAtom(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitBooleanAtom"):
                return visitor.visitBooleanAtom(self)
            else:
                return visitor.visitChildren(self)


    class IdAtomContext(AtomContext):

        def __init__(self, parser, ctx): # actually a ExpressionParser.AtomContext)
            super(ExpressionParser.IdAtomContext, self).__init__(parser)
            self.copyFrom(ctx)

        def ID(self):
            return self.getToken(ExpressionParser.ID, 0)

        def enterRule(self, listener):
            if hasattr(listener, "enterIdAtom"):
                listener.enterIdAtom(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitIdAtom"):
                listener.exitIdAtom(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitIdAtom"):
                return visitor.visitIdAtom(self)
            else:
                return visitor.visitChildren(self)


    class ArrayAtomContext(AtomContext):

        def __init__(self, parser, ctx): # actually a ExpressionParser.AtomContext)
            super(ExpressionParser.ArrayAtomContext, self).__init__(parser)
            self.copyFrom(ctx)

        def value_list(self):
            return self.getTypedRuleContext(ExpressionParser.Value_listContext,0)


        def enterRule(self, listener):
            if hasattr(listener, "enterArrayAtom"):
                listener.enterArrayAtom(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitArrayAtom"):
                listener.exitArrayAtom(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitArrayAtom"):
                return visitor.visitArrayAtom(self)
            else:
                return visitor.visitChildren(self)


    class StringAtomContext(AtomContext):

        def __init__(self, parser, ctx): # actually a ExpressionParser.AtomContext)
            super(ExpressionParser.StringAtomContext, self).__init__(parser)
            self.copyFrom(ctx)

        def STRING(self):
            return self.getToken(ExpressionParser.STRING, 0)

        def enterRule(self, listener):
            if hasattr(listener, "enterStringAtom"):
                listener.enterStringAtom(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitStringAtom"):
                listener.exitStringAtom(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitStringAtom"):
                return visitor.visitStringAtom(self)
            else:
                return visitor.visitChildren(self)


    class NilAtomContext(AtomContext):

        def __init__(self, parser, ctx): # actually a ExpressionParser.AtomContext)
            super(ExpressionParser.NilAtomContext, self).__init__(parser)
            self.copyFrom(ctx)

        def NIL(self):
            return self.getToken(ExpressionParser.NIL, 0)

        def enterRule(self, listener):
            if hasattr(listener, "enterNilAtom"):
                listener.enterNilAtom(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitNilAtom"):
                listener.exitNilAtom(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitNilAtom"):
                return visitor.visitNilAtom(self)
            else:
                return visitor.visitChildren(self)


    class NumberAtomContext(AtomContext):

        def __init__(self, parser, ctx): # actually a ExpressionParser.AtomContext)
            super(ExpressionParser.NumberAtomContext, self).__init__(parser)
            self.copyFrom(ctx)

        def INT(self):
            return self.getToken(ExpressionParser.INT, 0)
        def DOUBLE(self):
            return self.getToken(ExpressionParser.DOUBLE, 0)

        def enterRule(self, listener):
            if hasattr(listener, "enterNumberAtom"):
                listener.enterNumberAtom(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitNumberAtom"):
                listener.exitNumberAtom(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitNumberAtom"):
                return visitor.visitNumberAtom(self)
            else:
                return visitor.visitChildren(self)



    def atom(self):

        localctx = ExpressionParser.AtomContext(self, self._ctx, self.state)
        self.enterRule(localctx, 44, self.RULE_atom)
        self._la = 0 # Token type
        try:
            self.state = 235
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [ExpressionParser.LPAREN]:
                localctx = ExpressionParser.ParExprContext(self, localctx)
                self.enterOuterAlt(localctx, 1)
                self.state = 225
                self.match(ExpressionParser.LPAREN)
                self.state = 226
                self.expr(0)
                self.state = 227
                self.match(ExpressionParser.RPAREN)
                pass
            elif token in [ExpressionParser.BEGL]:
                localctx = ExpressionParser.ArrayAtomContext(self, localctx)
                self.enterOuterAlt(localctx, 2)
                self.state = 229
                self.value_list()
                pass
            elif token in [ExpressionParser.INT, ExpressionParser.DOUBLE]:
                localctx = ExpressionParser.NumberAtomContext(self, localctx)
                self.enterOuterAlt(localctx, 3)
                self.state = 230
                _la = self._input.LA(1)
                if not(_la==ExpressionParser.INT or _la==ExpressionParser.DOUBLE):
                    self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                pass
            elif token in [ExpressionParser.MOSAIC_TRUE, ExpressionParser.MOSAIC_FALSE]:
                localctx = ExpressionParser.BooleanAtomContext(self, localctx)
                self.enterOuterAlt(localctx, 4)
                self.state = 231
                _la = self._input.LA(1)
                if not(_la==ExpressionParser.MOSAIC_TRUE or _la==ExpressionParser.MOSAIC_FALSE):
                    self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                pass
            elif token in [ExpressionParser.ID]:
                localctx = ExpressionParser.IdAtomContext(self, localctx)
                self.enterOuterAlt(localctx, 5)
                self.state = 232
                self.match(ExpressionParser.ID)
                pass
            elif token in [ExpressionParser.STRING]:
                localctx = ExpressionParser.StringAtomContext(self, localctx)
                self.enterOuterAlt(localctx, 6)
                self.state = 233
                self.match(ExpressionParser.STRING)
                pass
            elif token in [ExpressionParser.NIL]:
                localctx = ExpressionParser.NilAtomContext(self, localctx)
                self.enterOuterAlt(localctx, 7)
                self.state = 234
                self.match(ExpressionParser.NIL)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Value_listContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(ExpressionParser.Value_listContext, self).__init__(parent, invokingState)
            self.parser = parser

        def BEGL(self):
            return self.getToken(ExpressionParser.BEGL, 0)

        def ENDL(self):
            return self.getToken(ExpressionParser.ENDL, 0)

        def array(self):
            return self.getTypedRuleContext(ExpressionParser.ArrayContext,0)


        def getRuleIndex(self):
            return ExpressionParser.RULE_value_list

        def enterRule(self, listener):
            if hasattr(listener, "enterValue_list"):
                listener.enterValue_list(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitValue_list"):
                listener.exitValue_list(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitValue_list"):
                return visitor.visitValue_list(self)
            else:
                return visitor.visitChildren(self)




    def value_list(self):

        localctx = ExpressionParser.Value_listContext(self, self._ctx, self.state)
        self.enterRule(localctx, 46, self.RULE_value_list)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 237
            self.match(ExpressionParser.BEGL)
            self.state = 239
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,23,self._ctx)
            if la_ == 1:
                self.state = 238
                self.array()


            self.state = 241
            self.match(ExpressionParser.ENDL)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class ArrayContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(ExpressionParser.ArrayContext, self).__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return ExpressionParser.RULE_array

     
        def copyFrom(self, ctx):
            super(ExpressionParser.ArrayContext, self).copyFrom(ctx)



    class ArrayValuesContext(ArrayContext):

        def __init__(self, parser, ctx): # actually a ExpressionParser.ArrayContext)
            super(ExpressionParser.ArrayValuesContext, self).__init__(parser)
            self.copyFrom(ctx)

        def arrayElement(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(ExpressionParser.ArrayElementContext)
            else:
                return self.getTypedRuleContext(ExpressionParser.ArrayElementContext,i)

        def COMMA(self, i=None):
            if i is None:
                return self.getTokens(ExpressionParser.COMMA)
            else:
                return self.getToken(ExpressionParser.COMMA, i)

        def enterRule(self, listener):
            if hasattr(listener, "enterArrayValues"):
                listener.enterArrayValues(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitArrayValues"):
                listener.exitArrayValues(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitArrayValues"):
                return visitor.visitArrayValues(self)
            else:
                return visitor.visitChildren(self)



    def array(self):

        localctx = ExpressionParser.ArrayContext(self, self._ctx, self.state)
        self.enterRule(localctx, 48, self.RULE_array)
        self._la = 0 # Token type
        try:
            localctx = ExpressionParser.ArrayValuesContext(self, localctx)
            self.enterOuterAlt(localctx, 1)
            self.state = 244
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << ExpressionParser.LPAREN) | (1 << ExpressionParser.BEGL) | (1 << ExpressionParser.MOSAIC_TRUE) | (1 << ExpressionParser.MOSAIC_FALSE) | (1 << ExpressionParser.NIL) | (1 << ExpressionParser.ID) | (1 << ExpressionParser.INT) | (1 << ExpressionParser.DOUBLE) | (1 << ExpressionParser.STRING))) != 0):
                self.state = 243
                self.arrayElement()


            self.state = 252
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==ExpressionParser.COMMA:
                self.state = 246
                self.match(ExpressionParser.COMMA)
                self.state = 248
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << ExpressionParser.LPAREN) | (1 << ExpressionParser.BEGL) | (1 << ExpressionParser.MOSAIC_TRUE) | (1 << ExpressionParser.MOSAIC_FALSE) | (1 << ExpressionParser.NIL) | (1 << ExpressionParser.ID) | (1 << ExpressionParser.INT) | (1 << ExpressionParser.DOUBLE) | (1 << ExpressionParser.STRING))) != 0):
                    self.state = 247
                    self.arrayElement()


                self.state = 254
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class ArrayElementContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(ExpressionParser.ArrayElementContext, self).__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return ExpressionParser.RULE_arrayElement

     
        def copyFrom(self, ctx):
            super(ExpressionParser.ArrayElementContext, self).copyFrom(ctx)



    class ArrayElementTypesContext(ArrayElementContext):

        def __init__(self, parser, ctx): # actually a ExpressionParser.ArrayElementContext)
            super(ExpressionParser.ArrayElementTypesContext, self).__init__(parser)
            self.copyFrom(ctx)

        def atom(self):
            return self.getTypedRuleContext(ExpressionParser.AtomContext,0)


        def enterRule(self, listener):
            if hasattr(listener, "enterArrayElementTypes"):
                listener.enterArrayElementTypes(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitArrayElementTypes"):
                listener.exitArrayElementTypes(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitArrayElementTypes"):
                return visitor.visitArrayElementTypes(self)
            else:
                return visitor.visitChildren(self)



    def arrayElement(self):

        localctx = ExpressionParser.ArrayElementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 50, self.RULE_arrayElement)
        try:
            localctx = ExpressionParser.ArrayElementTypesContext(self, localctx)
            self.enterOuterAlt(localctx, 1)
            self.state = 255
            self.atom()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx



    def sempred(self, localctx, ruleIndex, predIndex):
        if self._predicates == None:
            self._predicates = dict()
        self._predicates[9] = self.expr_sempred
        pred = self._predicates.get(ruleIndex, None)
        if pred is None:
            raise Exception("No predicate with index:" + str(ruleIndex))
        else:
            return pred(localctx, predIndex)

    def expr_sempred(self, localctx, predIndex):
            if predIndex == 0:
                return self.precpred(self._ctx, 10)
         

            if predIndex == 1:
                return self.precpred(self._ctx, 7)
         

            if predIndex == 2:
                return self.precpred(self._ctx, 6)
         

            if predIndex == 3:
                return self.precpred(self._ctx, 5)
         

            if predIndex == 4:
                return self.precpred(self._ctx, 4)
         

            if predIndex == 5:
                return self.precpred(self._ctx, 3)
         

            if predIndex == 6:
                return self.precpred(self._ctx, 2)
         




