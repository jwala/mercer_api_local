package com.lti.mosaic.derby;

import java.sql.Connection;
import java.util.List;
import java.util.concurrent.RecursiveAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.lti.mosaic.db.MysqlConnectionMercer;

/**
 * @author rushikesh
 *
 */
public class DerbyLoader extends RecursiveAction {

  private static final Logger logger = LoggerFactory.getLogger(DerbyLoader.class);

  private static final long serialVersionUID = 1L;
  private List<String> allTables;

  /**
   * 
   */
  public DerbyLoader(List<String> tableList) {
    super();
    this.allTables = tableList;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.util.concurrent.RecursiveAction#compute()
   */
  @Override
  protected void compute() {
    if (allTables.size() == 0) {
      // DO NOTHING
    } else if (allTables.size() == 1) {

      for (String tableName : allTables) {

        Connection mysqlConn = null;
        try {
          mysqlConn = MysqlConnectionMercer.getConnection();
        } catch (Exception e) {
          e.printStackTrace();
          logger.error("Error while getting mysql connection {}", e);
        }


        try {

          Connection newDerbyConnection = MercerDBCP.getDataSource().getConnection();
          
          DerbyUtils.insertDataIntoDerbyTable(newDerbyConnection, mysqlConn, tableName);
          
          newDerbyConnection.close();

        } catch (Exception e) {
          logger.error("Error while inserting data into table {} {}", e, tableName);

        }

      }
    } else {

      int midpoint = allTables.size() / 2;
      List<String> list1 = allTables.subList(0, midpoint);
      List<String> list2 = allTables.subList(midpoint, allTables.size());
      DerbyLoader derbyLoader1 = new DerbyLoader(list1);
      DerbyLoader derbyLoader2 = new DerbyLoader(list2);
      invokeAll(derbyLoader1, derbyLoader2);
    }

  }

}
