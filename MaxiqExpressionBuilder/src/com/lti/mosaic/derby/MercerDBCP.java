package com.lti.mosaic.derby;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.dbcp2.BasicDataSource;

import com.lti.mosaic.cache.Cache;
import com.lti.mosaic.parser.constants.CacheConstants;

public class MercerDBCP {

    private static String driver = DerbyConstants.DRIVER;
    private static String dbName = Cache.getProperty(CacheConstants.MERCER_DB_NAME);
    private static String protocol = DerbyConstants.PROTOCOL + dbName + ";create=true";
    private static String userName = "mercer";
    private static String privateKey = "mercer";

    private static MercerDBCP datasource;
    private BasicDataSource basicDs;

    private MercerDBCP(){
        basicDs = new BasicDataSource();
        basicDs.setDriverClassName(driver);
        basicDs.setUrl(protocol);
        basicDs.setUsername(userName);
        basicDs.setPassword(privateKey);
        
        // the settings below are optional -- dbcp can work with defaults
        basicDs.setMaxTotal(100);
        basicDs.setMinIdle(5);
        basicDs.setMaxIdle(100);
        basicDs.setMaxOpenPreparedStatements(180);

    }

     public static MercerDBCP getDataSource() {
            if (datasource == null) {
                datasource = new MercerDBCP();
                return datasource;
            } else {
                return datasource;
            }
        }
     
     public Connection getConnection() throws SQLException {
            return this.basicDs.getConnection();
        }
     
     public Map<String, String> printStatus() {
       
       Map<String,String> map = new HashMap<>();
       map.put("Max",basicDs.getMaxTotal()+"");
       map.put("Active",basicDs.getNumActive() + "");
       map.put("Idle",basicDs.getNumIdle()+"");
       
       return map;
       
   }


}