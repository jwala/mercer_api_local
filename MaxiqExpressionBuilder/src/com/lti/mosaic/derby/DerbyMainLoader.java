package com.lti.mosaic.derby;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ForkJoinPool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.lti.mosaic.db.MysqlConnectionMercer;
import com.lti.mosaic.parser.constants.LoggerConstants;

/**
 * @author rushikesh
 *
 */
public class DerbyMainLoader {

  private static final Logger logger = LoggerFactory.getLogger(DerbyMainLoader.class);

  private DerbyMainLoader() {
  }
  /**
   * This method uses fork-join to load data of all tables from mysql to derby
   */
  public static void load() {

    Connection mysqlConn = null;
    try {
      mysqlConn = MysqlConnectionMercer.getConnection();
    } catch (Exception e) {
      logger.error(LoggerConstants.LOG_MAXIQAPI+"Error while getting mysql Connection {}", e);
    }
    List<String> allTables = new ArrayList<>();
    try {
      allTables = DerbyUtils.getAllTables(mysqlConn);
    } catch (Exception e) {
      logger.error(LoggerConstants.LOG_MAXIQAPI+"Error while getting information of all tables {}", e);
    }

    int nThreads = Runtime.getRuntime().availableProcessors();
    DerbyLoader derbyLoader = new DerbyLoader(allTables);
    ForkJoinPool pool = new ForkJoinPool(nThreads);
    pool.invoke(derbyLoader);

    logger.info("{} Derby Loading completed",LoggerConstants.LOG_MAXIQAPI);

  }
  
}
