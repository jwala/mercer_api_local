package in.lnt.exceptions;

/**
 * 
 * @author Nikhil Kshirsagar
 * @since 25-09-2017
 * @version 1.0
 */
public class MercerSystemException extends Exception {
	private static final long serialVersionUID = 3700564391580133843L;

	public MercerSystemException() {
		super();
	}

	public MercerSystemException(String msg) {
		super(msg);
	}

	public MercerSystemException(String msg, Throwable thr) {
		super(msg, thr);
	}

}
