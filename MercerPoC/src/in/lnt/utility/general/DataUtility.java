package in.lnt.utility.general;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.util.Streams;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.ibm.icu.text.DecimalFormat;
import com.ibm.icu.text.DecimalFormatSymbols;

import in.lnt.constants.Constants;
import in.lnt.validations.evaluator.APIExpressionEvaluator;

/**
 * 
 * @author Sarang Gandhi
 * @version 0.1
 * @since 28 Aug 2017.
 *
 */
public class DataUtility {
	
	private static final Logger logger = LoggerFactory.getLogger(DataUtility.class);
	
	private DataUtility() {}

	public static boolean isStringNotNullAndBlank(String str) {
		return StringUtils.isNotEmpty(str);
	}
	
	/** 
	 * 
	 * @return timestamp for storing in DB
	 */
	public static Timestamp currentTimestamp() {
		Date today = new Date(); 
		return new Timestamp(today.getTime());
	}

	/**
	 *  
	 * @param string
	 * @return boolean
	 */
	public static boolean isDecimal(String string) {
		return !StringUtils.isEmpty(string) && string.matches("(\\+|-)?([0-9]*+(\\.[0-9]+)?)");
	}

	/**
	 * 
	 * 2,50,000.20   ,	10,000,00.00 ,  4,50,0000.00 , 1,000.00 , 23,000.00 
	 * 55,00,000.45 ,10000.56 ,  1,100,000.00  , 60,00,000.23 , 60,00,000.23 
     * 1,20,000,00.45 , 10,00,00,000.23 
	 * @param string
	 * @return boolean
	 */
	public static boolean isDecimal(String string, ArrayNode decimalArray) {
		// For 2 decimals ("^\\d+\\.\\d{2}$")
		// For loosely typed use regex:^[\d.]+$
		String aDecimalFormatPattern = Constants.DEFAULT_DECIMAL_FORMAT;
		String thousandsSeparator = ",";
		String decimalSeperator = ".";

		DecimalFormatSymbols decimalFormatSymbols = null;

		DecimalFormat decimalFormat = null;
		ParsePosition parsePosition = null;
		Object object = null;
	
				boolean flag = false;
		if (!StringUtils.isEmpty(string)) {
			if (decimalArray.size() > 0) {
				for (JsonNode jsNode : decimalArray) {
					if (!JsonUtils.isNullOrBlankOrNullNode(jsNode.get(Constants.PATTERN))
							&& !StringUtils.isEmpty(jsNode.get(Constants.PATTERN).asText())) {
						aDecimalFormatPattern = jsNode.get(Constants.PATTERN).asText();
					}

					if (!JsonUtils.isNullOrBlankOrNullNode(jsNode.get(Constants.THOUSANDSSEPARATOR ))
							&& !StringUtils.isEmpty(jsNode.get(Constants.THOUSANDSSEPARATOR).asText())) {
						thousandsSeparator = jsNode.get(Constants.THOUSANDSSEPARATOR).asText();
					}

					if (!JsonUtils.isNullOrBlankOrNullNode(jsNode.get(Constants.DECIMALSEPARATOR))
							&& !StringUtils.isEmpty(jsNode.get(Constants.DECIMALSEPARATOR).asText())) {
						decimalSeperator = jsNode.get(Constants.DECIMALSEPARATOR).asText();
					}
				}
				try {
					decimalFormatSymbols = new DecimalFormatSymbols();
					decimalFormatSymbols.setGroupingSeparator(thousandsSeparator.charAt(0));
					decimalFormatSymbols.setDecimalSeparator(decimalSeperator.charAt(0));

					decimalFormat = new DecimalFormat(aDecimalFormatPattern, decimalFormatSymbols);
					parsePosition = new ParsePosition(0);
					//PE-9144 This will parse strict pattern and thousand separator and decimal separator.
					decimalFormat.setParseStrict(true);
					object = decimalFormat.parse(string, parsePosition);

					if (object != null && parsePosition.getIndex() >= string.length()) {
						flag = true;
					}
				} catch (Exception ex) {
					logger.error("Exception reason is: {}",ex.getMessage());
					return  false;
				}
			} else {
					flag = string.matches("(\\+|-)?([0-9]*+(\\.[0-9]+)?)");
			}
		}
		return flag;
	}

	/**
	 * 
	 * @param string
	 * @param decimalArray
	 * @return String
	 */
	public static String toDecimal(String string, ArrayNode decimalArray) {
		// For 2 decimals ("^\\d+\\.\\d{2}$")
		// For loosely typed use regex:^[\d.]+$
		String aDecimalFormatPattern = Constants.DEFAULT_DECIMAL_FORMAT;
		String thousandsSeparator = ",";
		String decimalSeperator = ".";

		DecimalFormatSymbols symbols = null;

		DecimalFormat format = null;
		ParsePosition parsePosition = null;
		Object object = null;

		String resultDecimal = null;
		if (!StringUtils.isEmpty(string) && decimalArray.size() > 0) {
			try {
				symbols = new DecimalFormatSymbols();
				symbols.setGroupingSeparator(thousandsSeparator.charAt(0));
				symbols.setDecimalSeparator(decimalSeperator.charAt(0));

					format = new DecimalFormat(aDecimalFormatPattern, symbols);
					parsePosition = new ParsePosition(0);
					object = format.parse(string, parsePosition);

				if (object != null && parsePosition.getIndex() >= string.length()) {
					resultDecimal = new BigDecimal(object.toString()).toString();
				}
			} catch (Exception ex) {
				logger.error("Exception is {}",ex.getMessage());
				return  resultDecimal;
			}
		}
		return resultDecimal;
	}

	/**
	 * 
	 * @param string
	 * @return boolean
	 */
	public static boolean isNumeric(String string) {
		return !StringUtils.isEmpty(string) && string.matches("(\\+|-)?([0-9]+)");
	}

	/**
	 * 
	 * @param string
	 * @return boolean
	 */
	public static boolean isNumeric(String string, ArrayNode numberArray) {

		// For 2 decimals ("^\\d+\\.\\d{2}$")
		// For loosely typed use regex:^[\d.]+$
		String aDecimalFormatPattern = "###,###.###";
		String thousandsSeparator = ",";
		String decimalSeperator = ".";

		DecimalFormatSymbols decimalFormatSymbols = null;

		DecimalFormat decimalFormat = null;
		ParsePosition parsePosition = null;
		Object object = null;

		boolean flag = false;
		if (!StringUtils.isEmpty(string) && numberArray.size() > 0) {
			for (JsonNode jsNode : numberArray) {
				if (!JsonUtils.isNullOrBlankOrNullNode(jsNode.get(Constants.PATTERN))
						&& !StringUtils.isEmpty(jsNode.get(Constants.PATTERN).asText())) {
					aDecimalFormatPattern = jsNode.get(Constants.PATTERN).asText();
				}

				if (!JsonUtils.isNullOrBlankOrNullNode(jsNode.get(Constants.THOUSANDSSEPARATOR))
						&& !StringUtils.isEmpty(jsNode.get(Constants.THOUSANDSSEPARATOR).asText())) {
					thousandsSeparator = jsNode.get(Constants.THOUSANDSSEPARATOR).asText();
				}

				if (!JsonUtils.isNullOrBlankOrNullNode(jsNode.get(Constants.DECIMALSEPARATOR))
						&& !StringUtils.isEmpty(jsNode.get(Constants.DECIMALSEPARATOR).asText())) {
					decimalSeperator = jsNode.get(Constants.DECIMALSEPARATOR).asText();
				}
			}
			try {
				decimalFormatSymbols = new DecimalFormatSymbols();
				decimalFormatSymbols.setGroupingSeparator(thousandsSeparator.charAt(0));
				decimalFormatSymbols.setDecimalSeparator(decimalSeperator.charAt(0));

				decimalFormat = new DecimalFormat(aDecimalFormatPattern, decimalFormatSymbols);
				parsePosition = new ParsePosition(0);
				//PE-9144 This will parse strict pattern and thousand separator and decimal separator.
				decimalFormat.setParseStrict(true);
				object = decimalFormat.parse(string, parsePosition);

				if (object != null && parsePosition.getIndex() >= string.length()) {
						flag = true;
				}
			} catch (Exception ex) {
				logger.error("Exception is {}",ex.getMessage());
				return  false;
			}
		}
		return flag;
	}

	/**
	 * @param String
	 * @return String
	 */
	public static String toNumber(String string, ArrayNode numberArray) {

		// For 2 decimals ("^\\d+\\.\\d{2}$")
		// For loosely typed use regex:^[\d.]+$
		String aDecimalFormatPattern = "###,###.###";
		String thousandsSeparator = ",";
		String decimalSeperator = ".";

		DecimalFormatSymbols symbols = null;

		DecimalFormat format = null;
		ParsePosition parsePosition = null;
		Object object = null;

		String  numberString = null;
		if (!StringUtils.isEmpty(string) && numberArray.size() > 0) {
			try {
				symbols = new DecimalFormatSymbols();
				symbols.setGroupingSeparator(thousandsSeparator.charAt(0));
				symbols.setDecimalSeparator(decimalSeperator.charAt(0));

				format = new DecimalFormat(aDecimalFormatPattern, symbols);
				parsePosition = new ParsePosition(0);
				object = format.parse(string, parsePosition);

				if (object != null && parsePosition.getIndex() >= string.length()) {
					numberString = new BigDecimal(object.toString()).toString();
				}
			} catch (Exception ex) {
				logger.error("Exception is {}",ex.getMessage());
				return  numberString;
			}
		}
		return numberString;
	}
	
	
	/**
	 * 
	 * @param inDate
	 * @return boolean
	 */
	public static boolean isValidDate(String inDate) {
		//YYYY-MM-DDThh:mm:ss.sTZD  if requires we need to add support for timestamp too.
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		dateFormat.setLenient(false);
		try {
			dateFormat.parse(inDate.trim());
		} catch (ParseException pe) {
			logger.error("Parse Exception is {}",pe.getMessage());
			return false;
		}
		return true;
	}

	/**
	 * PE - 7257
	 * 
	 * @param inDate
	 * @return boolean
	 */
	public static boolean isValidDate(String inputDate, ArrayNode inputDateArray) {
		boolean returnVal = false;
		if( StringUtils.isEmpty(inputDate)) {
			return returnVal;
		}
		/* Loop through array of formats and validate using checkDate function. */
		
		for (int i = 0; i < inputDateArray.size(); i++) {
			returnVal = checkDate(inputDate, inputDateArray.get(i).asText());
			if(returnVal) {
				break;
			}
		}
		return returnVal;
	}

	
  /**
   * 
   * @param inDate
   * @param format
   * @return boolean
   * This method will check the inDate is matching with the format.
   */
	public static boolean checkDate(String inDate, String format) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(format.replaceAll("\"", "").trim());
		dateFormat.setLenient(false);
		//2 digit year is not allowed , issue has been raised in internal test scenarios.
		// Calendar is added to fetch year and check it is 4 digit.
		Calendar cal = Calendar.getInstance();
		int year ;
		try {
			 Date date = dateFormat.parse(inDate.trim());
			 cal.setTime(date);
			 year = cal.get(Calendar.YEAR);
		} catch (ParseException pe) {
			return false;
		}
		return year >= 999;	
	}
	
	/**
	 * 
	 * @param dateToBeParse
	 * @param inputDateArray
	 * @return String with yyyy-MM-dd format
	 */
	public static String customDateConvertor(String dateToBeParse, ArrayNode inputDateArray) {
		DateFormat defaultDateFormat = new SimpleDateFormat("MM/dd/yyyy");
		DateFormat dateFormatNeeded = new SimpleDateFormat("yyyy-MM-dd");
		Date date;
		String convertedDate = null;
		try {
			for (JsonNode dtFormatEle : inputDateArray) {
				if (!JsonUtils.isNullOrBlankOrNullNode(dtFormatEle) &&
						checkDate(dateToBeParse, dtFormatEle.asText())) {
					defaultDateFormat = new SimpleDateFormat(dtFormatEle.asText());
					date = defaultDateFormat.parse(dateToBeParse);
					convertedDate = dateFormatNeeded.format(date);
					break;
				} 
			}
		} catch (ParseException pe) {
			logger.error("Parse Exception is {}",pe.getMessage());
		}
		return convertedDate;
	}
	

	/**
	 * PE - 7056
	 * 
	 * @author Sarang
	 * @since 9 Dec 2017
	 * 
	 * @param apiExpressionEvaluator
	 * @return metaDataColumnsOnly (List of Strings)
	 */

	public static List<String> extractColumnsFromMetaData(APIExpressionEvaluator apiExpressionEvaluator) {
		List<String> metaDataColumnsOnly = null;
		metaDataColumnsOnly = new ArrayList<>();
		if (null != apiExpressionEvaluator && null != apiExpressionEvaluator.getColumnDataTypeMapping()) {
			metaDataColumnsOnly = apiExpressionEvaluator.getColumnDataTypeMapping().keySet().stream()
					.collect(Collectors.toList());
		}
		return metaDataColumnsOnly;
	}
	
	public static boolean checkMDT(FileItem metaData,List<String> header,String sheetName,int workBookSize)
	{
		JSONObject validationDataObject;
		boolean found = true;
		String metaDataSheet="";
		String entityCode = "";
		HashMap<String,String> columnMap = null;
		try {
		validationDataObject = new JSONObject(Streams.asString(metaData.getInputStream(), Constants.UTF_8));
		ObjectMapper mapper = new ObjectMapper();
		JsonNode columnNode = null;
		String entitiesJson = validationDataObject.get(Constants.ENTITIES).toString();
		ArrayNode entitiesNode = mapper.readValue(entitiesJson, ArrayNode.class);
		for (int i = 0; i < entitiesNode.size(); i++) {
		if(workBookSize==1)
		{
			metaDataSheet=sheetName;
		}
		else if (entitiesNode.get(i).get(Constants.CONTEXTDATAKEY).get(Constants.COMPANYNAME) != null
				&& entitiesNode.get(i).get(Constants.CONTEXTDATAKEY).get(Constants.CTRYCDOE) != null) {
			metaDataSheet = entitiesNode.get(i).get(Constants.CONTEXTDATAKEY).get(Constants.COMPANYNAME).asText() + "_"
					+ entitiesNode.get(i).get(Constants.CONTEXTDATAKEY).get(Constants.CTRYCDOE).asText();
		}
		if(metaDataSheet.equalsIgnoreCase(sheetName))
		{
			columnMap = new HashMap<>();
			columnNode = entitiesNode.get(i).get(Constants.SECTION_STRUCTURE).get(Constants.COLUMNS);
			for (JsonNode node : columnNode) {
				entityCode = node.get("code").asText();
				columnMap.put(entityCode,entityCode);
			}
			if (header != null) {
				for(String headerName : header)
				{
					if(columnMap.get(headerName)==null)
					{
						found=false;
						break;
					}
				}
			}
		}
		}
		}
		catch(Exception e)
		{
			logger.error(" General exception caught" + e.getMessage());
		}
	return found;	
	}
	public static Map<String,String> checkUniqueEEIDForRequestDataJson(String dataAsJson,Map<String,String> empIdMap) throws IOException
	{
		ObjectMapper mapper = new ObjectMapper();
		JsonNode dataNode = mapper.readValue(dataAsJson, JsonNode.class).get("data");
		JsonNode node = null;
		String eeidCol = null;
		if (dataNode != null) {
			Set<String> eeidSet = new HashSet<>();
			for (int k = 0; k < dataNode.size(); k++) {
				node = dataNode.get(k);
				if (empIdMap.get(Constants.EMPID_COL) != null) {
					eeidCol = empIdMap.get(Constants.EMPID_COL);
				} else {
					eeidCol = Constants.EMPLOYEE_EEID;
				}

				if (JsonUtils.isBlankNode(node.get(eeidCol))) {
					String eeId = node.get(eeidCol).asText();
					if (!eeidSet.add(eeId)) {
						eeidSet.clear();
						empIdMap.put(Constants.ISUNIQUE, Constants.DUPLICATE);
						break;
					}
				} else {
					empIdMap.put(Constants.ISUNIQUE, Constants.MISSING);
				}
			}
		}
		return empIdMap;
	}
	
	/**
	 * @param dataMap
	 * @param dataNode
	 * @param obj
	 * @param code
	 * @param dataType
	 * @param s
	 * @param a
	 * @return
	 */
	public static ObjectNode roundOffMoneyQuestionType(Map<String, JsonNode> dataMap, JsonNode dataNode, ObjectNode obj,
			String code, String dataType, String colStringVal, double colDoubleVal) {
		String[] str=colStringVal.split("\\.");
		if(str[1].length() > 2) {
			colDoubleVal = Math.round(colDoubleVal * 100);
			colDoubleVal = colDoubleVal/100;
			((ObjectNode) (dataNode)).put(code, colDoubleVal);
			if (!JsonUtils.isNullOrBlankOrNullNode(dataNode.get(code))) {
				dataMap.put(code,dataNode.get(code));
				obj = APIExpressionEvaluator.prepareMoneyAutocorrectObj(code, colStringVal, dataType, Constants.MONEY);

			}
		}
		return obj;
	}

}