package in.lnt.utility.poi;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.fileupload.FileItem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import in.lnt.constants.Constants;
import in.lnt.exceptions.CustomStatusException;
import in.lnt.utility.constants.ErrorCacheConstant;
import in.lnt.utility.general.Cache;
import in.lnt.utility.general.DataUtility;

/**
 * 
 * @author Sarang Gandhi
 * 
 *         This is POI utility class.
 */
public class POIUtility {
	
	private POIUtility()
	{
	}
	private static final Logger _logger = LoggerFactory.getLogger(POIUtility.class);


	/**
	 * @author Jwala Pradhan
	 * @param Fileitem
	 * @return hashmap
	 */

	public static HashMap<String, Object> prseXslxFile(FileItem item,FileItem metaData) throws Exception{

		_logger.info("{}{}",Constants.INTIATE,Constants.LOG_PARSEXSLXFILE);
		InputStream inpustStream = null;
		XSSFWorkbook workbook = null;
		HashMap<String, Object> map1 = null;
		boolean isMDT=false;
		HashMap<String, Object> workBookMap = new HashMap<>();
		StringBuilder cellValue = null;
		DataFormatter dataFormatter = new DataFormatter();
		
		// Ref : http://www.codejava.net/coding/working-with-formula-cells-in-excel-using-apache-poi
		FormulaEvaluator formulaEvaluator = null;
		try {
			inpustStream = item.getInputStream();
			workbook = new XSSFWorkbook(inpustStream);
			formulaEvaluator = workbook.getCreationHelper().createFormulaEvaluator();
			if (workbook != null) {
				for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
					isMDT=false;
					Sheet sheet = workbook.getSheetAt(i);
					if (i == 0){
						workBookMap.put(Constants.FIRSTSHEETNAME, workbook.getSheetName(i));
					}
					//PE-9868 Ignore sheet with name "Guide"
					if (workbook.getSheetName(i).equalsIgnoreCase(Constants.GUIDE)) {
						continue;
					}
					
					if(workBookMap.get(Constants.FIRSTSHEETNAME).toString().equalsIgnoreCase(Constants.GUIDE)){
						workBookMap.put(Constants.FIRSTSHEETNAME, workbook.getSheetName(i));
					}
					List<String> header = new ArrayList<>();
					List<ArrayList<String>> rows = new ArrayList<ArrayList<String>>();
					map1 = new HashMap<String, Object>();
					Iterator<Row> iterator = sheet.iterator();
					int j = 0;
					int ignoreRowCount = 0;
					while (iterator.hasNext()) {
						Row nextRow = iterator.next();

						Iterator<Cell> cellIterator = nextRow.cellIterator();
						if (j == 0) {											// HEADER : ROW(0)
							while (cellIterator.hasNext()) {
								Cell cell = cellIterator.next();
								// PE-9287: Error when column name is number-'Cannot get a text value from a numeric cell'
								cell.setCellType ( Cell.CELL_TYPE_STRING );
								header.add(cell.getStringCellValue());
							}
							//PE 9230 : Fetch data form 2nd in case of Non MDT file
							isMDT = DataUtility.checkMDT(metaData,header,sheet.getSheetName(),workbook.getNumberOfSheets());

						} else if (j == 1 && isMDT) {									// IGNORE : ROW(1)
							j++;
							continue;
							
						} else {												// For ROW(>1)
							ArrayList<String> tempRow = new ArrayList<>();
							
							// PE-6713
							// "ignoreRowCount" variable is used to count non-empty cells in a row.
							
							//PE-8575: Reject the file in case of Header is missing for one or multiple columns
							if(nextRow.getLastCellNum()>header.size())
							{
								throw new CustomStatusException(Constants.HTTPSTATUS_400,
										Cache.getPropertyFromError(ErrorCacheConstant.ERR_064));
							}
							ignoreRowCount = 0;
							
							for (int k = 0; k < header.size(); k++) {
								
								
								Cell cell = nextRow.getCell(k);
								
								// First : check if cell is null 
								if (cell == null) {
									tempRow.add(null);//PE -6874
									continue;
								}
								
								// Second : check the datatype and fetch data
								switch (cell.getCellType()) {
								case Cell.CELL_TYPE_BLANK:
									tempRow.add("");
									break;
								case Cell.CELL_TYPE_STRING:
									tempRow.add(cell.getStringCellValue());
									ignoreRowCount++;
									break;
								case Cell.CELL_TYPE_BOOLEAN:
									tempRow.add(""+cell.getBooleanCellValue());
									ignoreRowCount++;
									break;
								case Cell.CELL_TYPE_NUMERIC:
									if (DateUtil.isCellDateFormatted(cell)) { 		// Handles Date type here
										if(null != cellValue){
											cellValue.delete(0, cellValue.length());
											
											// Get date in Excel format
											cellValue.append(dataFormatter.formatCellValue(cell));
										}
								
									} else {
										cellValue = new StringBuilder();
										// //Numeric
										cellValue.append(dataFormatter.formatCellValue(cell));
									}
									tempRow.add(cellValue + "");
									ignoreRowCount++;
									break;
								
								case Cell.CELL_TYPE_FORMULA:
							        switch(cell.getCachedFormulaResultType()) {

									case Cell.CELL_TYPE_NUMERIC:
										if(null != cellValue){
											cellValue.delete(0, cellValue.length());
										}

										// Date
										if (DateUtil.isCellDateFormatted(cell)) {
											if(null != cellValue){
												cellValue.append(dataFormatter.formatCellValue(cell, formulaEvaluator));
											}
											// Numeric
										} else {
											cellValue = new StringBuilder();
											cellValue.append(dataFormatter.formatCellValue(cell, formulaEvaluator));
										}
										tempRow.add(cellValue + "");
										break;

									case Cell.CELL_TYPE_STRING:
										tempRow.add(cell.getStringCellValue());
										break;
									default : tempRow.add("");
									}
									break;
									case Cell.CELL_TYPE_ERROR:
										tempRow.add(null);
										break;
									default : tempRow.add("");
							}
							}
							if(ignoreRowCount != 0) {
								rows.add(tempRow);
							}
						}
						j++;
					}
					map1.put("header", header);
					map1.put("data", rows);
					workBookMap.put(sheet.getSheetName(), map1);
				}
			}
		}

		 finally {
			workbook = null;
			try {
				inpustStream.close();
			} catch (IOException e) {
				_logger.info("Exception :: "+ e.getMessage());
			}
		}
		_logger.info("{}{}",Constants.EXIT,Constants.LOG_PARSEXSLXFILE);
		return workBookMap;
	}
}