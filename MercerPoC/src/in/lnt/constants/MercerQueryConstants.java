package in.lnt.constants;

/**
 * 
 * @author Nikhil Kshirsagar
 * @since 25-09-2017
 * @version 1.0
 */
public class MercerQueryConstants {
	private MercerQueryConstants(){
		
	}
	public static final String LOG_MERCERPOCWEB = "MercerPoCWeb ";

	public static class DbExceptions {
		private DbExceptions(){
			
		}
		public static final String DRIVER_NOT_LOADED = "It looks like driver not loaded...";
		public static final String SQL_EXCEPTION = "Error while connecting to mysql...";
		
	}
}
