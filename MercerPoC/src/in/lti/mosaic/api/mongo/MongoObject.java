package in.lti.mosaic.api.mongo;

import java.util.Date;

/**
 * @author rushikesh
 *
 */
public class MongoObject {
  
  private String requestId;
  private String objectType;
  private String objectJson;
  private Date timeStamp;

  
  /**
   * @return the requestId
   */
  public String getRequestId() {
    return requestId;
  }
  /**
   * @param requestId the requestId to set
   */
  public void setRequestId(String requestId) {
    this.requestId = requestId;
  }
  /**
   * @return the objectType
   */
  public String getObjectType() {
    return objectType;
  }
  /**
   * @param objectType the objectType to set
   */
  public void setObjectType(String objectType) {
    this.objectType = objectType;
  }
  /**
   * @return the objectJson
   */
  public String getObjectJson() {
    return objectJson;
  }
  /**
   * @param objectJson the objectJson to set
   */
  public void setObjectJson(String objectJson) {
    this.objectJson = objectJson;
  }
/**
 * @return the timeStamp
 */
public Date getTimeStamp() {
	return timeStamp;
}
/**
 * @param timeStamp the timeStamp to set
 */
public void setTimeStamp(Date timeStamp) {
	this.timeStamp = timeStamp;
}

  
}
