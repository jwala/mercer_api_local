package in.lti.mosaic.api.mongo;

import java.util.HashMap;
import java.util.Map;

import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mongodb.DBCollection;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

import in.lnt.utility.constants.CacheConstats;
import in.lnt.utility.general.Cache;

/**
 * @author rushikesh
 *
 */
public class MongoConnector {

  private static final Logger logger = LoggerFactory.getLogger(MongoConnector.class);

  public static MongoDatabase db;
  public static DBCollection seqCollection;
  private static String serverIp = Cache.getMLProperty(CacheConstats.MONGO_SERVER_IP);
  private static Integer port = Integer.parseInt(Cache.getMLProperty(CacheConstats.MONGO_SERVER_PORT));
  private static String dbName = Cache.getMLProperty(CacheConstats.MONGO_DB_NAME);
  private static String userName = Cache.getMLProperty(CacheConstats.MONGODB_USERNAME);
  private static String password = Cache.getMLProperty(CacheConstats.MONGODB_SECRETWORD);
  public static MongoClient mongoClient;

  private static Map<String, DBCollection> collections = new HashMap<String, DBCollection>();

  public static MongoDatabase getMongoDBConnection() {
      if (db == null) {
          try {
              connectToDB();
          } catch (Exception e) {
              e.printStackTrace();
          }
      }
      return db;
  }

	public static boolean connectToDB() throws Exception {
		if (mongoClient == null) {
			// m = new Mongo(serverIp, port);
			// new MongoClientURI("mongodb://" + serverIp + ":" + port)
			/*MongoCredential credential = MongoCredential.createCredential(userName, dbName, password.toCharArray());
			MongoClientOptions options = MongoClientOptions.builder().sslEnabled(true).build();
			mongoClient = new MongoClient(new ServerAddress(serverIp, port), Arrays.asList(credential));*/
			String url = 
					"mongodb://" + userName + ":" + password + "@" + serverIp + ":" + port + "/?authSource=admin";
			logger.debug(url);
			MongoClientURI uri = new MongoClientURI(url);
			mongoClient = new MongoClient(uri);
		}
		db = mongoClient.getDatabase(dbName);
		return true;
	}
	
	public static void main(String[] args) {
		String requestCollectionName = Cache
				.getMLProperty(in.lnt.utility.constants.CacheConstats.MONGO_REQUEST_COLLECTION_NAME);
		System.out.println("requestCollectionName : " + requestCollectionName);
		MongoCollection<Document> collection = MongoConnector.getMongoDBConnection()
				.getCollection(requestCollectionName);
		System.out.println("COUNT : " + collection.count());
		Document doc = new Document();
		doc.put("request_id", 1 + "");
		collection.insertOne(doc);
		System.out.println("Done");
	}
}
