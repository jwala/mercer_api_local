package in.lti.mosaic.api.base.message.producer;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeoutException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.Address;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import in.lnt.utility.constants.LoggerConstants;
import in.lti.mosaic.api.base.configs.Cache;
import in.lti.mosaic.api.base.configs.CacheConstants;

/**
 * 
 * @author rushi
 *
 */
public class MessageProducer {

  public static ConnectionFactory factory = new ConnectionFactory();
  public static String EXCHANGE_NAME = Cache.getProperty(CacheConstants.EXCHANGE_NAME);
  private static List<Address> addresses = new ArrayList<Address>();
  
  private static final Logger logger = LoggerFactory.getLogger(MessageProducer.class);

  public static void init() throws Exception {

    factory.setUsername(Cache.getProperty(CacheConstants.RABBITMQ_USER_NAME));
    factory.setPassword(Cache.getProperty(CacheConstants.RABBITMQ_CREDENTIAL));

    for (String address : StringUtils.splitByWholeSeparatorPreserveAllTokens(
        Cache.getProperty(CacheConstants.RABBITMQ_ADRESSES), ",")) {

      Address addressOfRabbit = new Address(StringUtils.substringBefore(address, ":"),
          Integer.valueOf(StringUtils.substringAfter(address, ":")));

      addresses.add(addressOfRabbit);
    }
    
    logger.info(LoggerConstants.LOG_MAXIQAPI +"Rabbit Mq init");
  }

  static {
    try {
      init();
    } catch (Exception e) {
      logger.error(LoggerConstants.LOG_MAXIQAPI +"Error in rabbit init {}",e);
      e.printStackTrace();
    }
  }

  /**
   * 
   * @param topic
   * @param message
   * @param priority
   * @return
   * @throws IOException
   * @throws TimeoutException
   * @throws UnsupportedEncodingException
   */
  public static String rabbitMessageSendWithPriority(String topic, String message, Integer priority)
      throws IOException, TimeoutException, UnsupportedEncodingException {
    Connection connection = null;
    Channel channel = null;
    
    //Dont uncomment this code
   /* try {
      channel.queueDeclare(topic, false, false, false, null);
      channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.DIRECT,true);
    } catch (Exception e){
      logger.error("Error while declaring queue {}",e);
    }*/
    try {
    	connection = factory.newConnection(addresses);
    	channel = connection.createChannel();
    BasicProperties basicProperties = new BasicProperties(null, null, null, 2, priority, null,
        null, null, null, null, null, null, null, null);
    channel.basicPublish(EXCHANGE_NAME, topic, basicProperties, message.getBytes("UTF-8"));
    } catch (Exception e){
    }
    finally {
    	if(connection != null){
    		connection.close();
    	}
    }
    return message;
  }

}
