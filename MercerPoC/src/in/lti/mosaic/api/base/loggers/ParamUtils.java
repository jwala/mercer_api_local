package in.lti.mosaic.api.base.loggers;

public class ParamUtils {
	public static String getString(Object... values) {
		if (values == null)
			return "";

		StringBuffer rec = new StringBuffer("");
		try {
			for (Object value : values) {
				if (value != null)
					rec.append(value.toString()).append(" ");
			}	
		} catch (Throwable e) {
			
		}
		
		return rec.toString();
	}

}
