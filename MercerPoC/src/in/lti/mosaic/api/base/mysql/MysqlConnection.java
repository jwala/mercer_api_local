/**
 * 
 */
package in.lti.mosaic.api.base.mysql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import in.lnt.utility.constants.LoggerConstants;
import in.lti.mosaic.api.base.configs.Cache;
import in.lti.mosaic.api.base.configs.CacheConstants;
import in.lti.mosaic.api.base.exceptions.ExceptionsMessanger;
import in.lti.mosaic.api.base.exceptions.SystemException;
import in.lti.mosaic.api.base.loggers.ParamUtils;

/**
 * @author rushi
 *
 */
class MysqlConnection {

  private static final Logger logger = LoggerFactory.getLogger(MysqlConnection.class);
  
  private static String jdbcDriver = Cache.getProperty(CacheConstants.JDBC_DRIVER);
  private static String dbAddress = Cache.getProperty(CacheConstants.DB_ADDRESS);
  private static String dbName = Cache.getProperty(CacheConstants.DB_NAME);
  private static String userName = Cache.getProperty(CacheConstants.USER_NAME);
  private static String password = Cache.getProperty(CacheConstants.CREDENTIAL);
  
  private static Connection cn = null;
  private static Long lastGivenAt = 0L;

  public static Connection getConnection() throws SystemException {
      logger.debug(LoggerConstants.LOG_MAXIQAPI+" : >> getConnection()");
      Long currentTime = System.currentTimeMillis();

      if (cn == null || (currentTime-lastGivenAt)/1000 > 30) {
          
          try {
              if (cn != null)
                  cn.close();
              
              cn = null;
              lastGivenAt = currentTime;
              
              Class.forName(jdbcDriver);
              Connection connection = DriverManager.getConnection(dbAddress + dbName+"?zeroDateTimeBehavior=convertToNull", userName,
                      password);
              logger.debug(LoggerConstants.LOG_MAXIQAPI," : << getConnection()" + ParamUtils.getString(connection));
              return connection;
          } catch (ClassNotFoundException e) {
              logger.error(LoggerConstants.LOG_MAXIQAPI+ e );
              e.printStackTrace();
              ExceptionsMessanger.throwException(new SystemException(), "ERR_002", null);
          } catch (SQLException e) {
              logger.error(LoggerConstants.LOG_MAXIQAPI+ e );
              e.printStackTrace();
              ExceptionsMessanger.throwException(new SystemException(), "ERR_003", null);
          } catch (Exception e) {
              logger.error(LoggerConstants.LOG_MAXIQAPI+ e );
              e.printStackTrace();
              ExceptionsMessanger.throwException(new SystemException(), "ERR_003", null);
          }
      }
      logger.debug(LoggerConstants.LOG_MAXIQAPI," : << getConnection()" + ParamUtils.getString(cn));
      return cn;
  }
}
