package in.lti.mosaic.api.base.mysql;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import in.lnt.utility.constants.ErrorCacheConstant;
import in.lnt.utility.constants.LoggerConstants;
import in.lti.mosaic.api.base.exceptions.ExceptionsMessanger;
import in.lti.mosaic.api.base.exceptions.SystemException;
import in.lti.mosaic.api.base.loggers.ParamUtils;

/**
 * 
 * @author rushi
 *
 */
public class MysqlOperations {

  private static final Logger logger = LoggerFactory.getLogger(MysqlOperations.class);


  /**
   * 
   * @param object
   * @throws Exception
   */
  public static void insert(Object object) throws Exception {
    logger.debug(LoggerConstants.LOG_MAXIQAPI + " : >> insert()" + ParamUtils.getString(object));

    if (null != object) {
      String tableName = MysqlUtility.getTableNameForObject(object);

      Connection connection = null;
      java.sql.PreparedStatement insertStatement = null;

      try {
        connection = MysqlConnection.getConnection();

        // Setting auto-commit false so that dependent queries are
        // commit at once.
        connection.setAutoCommit(false);
        String insertQuery = MysqlUtility.generateInsertQuery(object, tableName);
        PreparedStatement stmt = connection.prepareStatement(insertQuery);
        stmt = MysqlUtility.generateInsertQueryData(stmt, object);
        stmt.executeUpdate();
        connection.commit();

      } catch (Exception e) {
        logger.error("Error while insert {}", e);

        try {
          connection.rollback();
        } catch (Exception ex) {
          logger.error("Error while rollback {}", e);
        }

        logger
            .debug(LoggerConstants.LOG_MAXIQAPI + LoggerConstants.INSERT + ParamUtils.getString(object));
        ExceptionsMessanger.throwException(new SystemException(), ErrorCacheConstant.ERR_017, e.getMessage());
      } finally {

        try {

          if (insertStatement != null) {
            insertStatement.close();
          }

          if (connection != null) {
            connection.close();
          }

        } catch (Exception e) {
          logger.debug(
              LoggerConstants.LOG_MAXIQAPI + LoggerConstants.INSERT + ParamUtils.getString(object));
          ExceptionsMessanger.throwException(new SystemException(), ErrorCacheConstant.ERR_017, e.getMessage());
        }
      }

    }
    logger.debug(LoggerConstants.LOG_MAXIQAPI + LoggerConstants.INSERT + ParamUtils.getString(object));
  }

  /**
   * Takes the argument class and filter conditions to fetch the data from the Mysql into list of
   * type input class. Pass the filter null if we do not want to put any conditions
   * 
   * @param entityClass
   * @param filter
   * @return
   * @throws SystemException
   */
  public static <T> List<T> scanForQuery(Class<T> entityClass, Map<String, Object> query)
      throws SystemException {

    String sql = MysqlUtility.generateQuery(entityClass, query);

    List<T> list = null;
    try (Connection connection = MysqlConnection.getConnection();
        PreparedStatement stmtment =
            MysqlUtility.generateSqlStatmentQueryData(connection.prepareStatement(sql), query);
        ResultSet resultSet = stmtment.executeQuery()) {
      list = new ArrayList<>();

      while (resultSet.next()) {
        list.add(MysqlUtility.buildObject(entityClass, resultSet));
      }

    } catch (SQLException | ClassNotFoundException | IOException e) {
      logger.error(LoggerConstants.LOG_MAXIQAPI + "scanForQuery {}", e);
      ExceptionsMessanger.throwException(new SystemException(), "ERR_018", e.getMessage());
    }

    return list;

  }

  /**
   * Saves the object with primary key into Mysql table.It uses the annotation {@link TableName} of
   * the bean object to determine the table name
   * 
   * @param object
   * @param primaryKeyAutoIncrement should be null
   * @return
   * @throws SystemException
   * @throws SQLException
   */

  public static Long insert(Object object, String primaryKeyAutoIncrement) throws Exception {
    logger.debug(LoggerConstants.LOG_MAXIQAPI + " : >> insert()" + ParamUtils.getString(object));
    Long generatedKey = 0L;
    if (null != object) {
      String tableName = MysqlUtility.getTableNameForObject(object);

      Map<String, Object> map = null;

      // deleteRow(object);
      ResultSet rs = null;
      PreparedStatement prepareStatement = null;
      try (Connection connection = MysqlConnection.getConnection();
          Statement statement = connection.createStatement();) {

        connection.setAutoCommit(false);

        map = MysqlUtility.getMapForBean(object);

        String insertStatement = MysqlUtility.generateInsertQueryForAutoIncrementedPrimaryKey(
            primaryKeyAutoIncrement, tableName, map);

        if (map.get(primaryKeyAutoIncrement) != null) {
          insert(object);
          return Long.parseLong(String.valueOf(map.get(primaryKeyAutoIncrement)));
        }

        logger.debug("@@@Insert : " + tableName + " Query " + insertStatement);

        prepareStatement = connection.prepareStatement(insertStatement, new String[] {primaryKeyAutoIncrement});
        MysqlUtility.generateInsertQueryDataWithPK(primaryKeyAutoIncrement, prepareStatement,
            object);

        prepareStatement.executeUpdate();

        connection.commit();

        rs = prepareStatement.getGeneratedKeys();
        if (rs != null && rs.next()) {
          generatedKey = rs.getLong(1);
        }

        //connection.close();
      } catch (SQLException e) {
        logger.error(LoggerConstants.LOG_MAXIQAPI + " : << insert() {} ", e);
        ExceptionsMessanger.throwException(new SystemException(), ErrorCacheConstant.ERR_017, e.getMessage());
      } finally {
    	  if(rs != null){
    		  rs.close();
    	  }
    	  if(prepareStatement != null){
    		  prepareStatement.close();
    	  }
      }

    }
    logger.debug(LoggerConstants.LOG_MAXIQAPI + LoggerConstants.INSERT + ParamUtils.getString(object));
    return generatedKey;
  }

  /**
   * Takes the argument class and Query (filter condition) to fetch the data from the Mysql into
   * object of input class. Pass the filter null if we do not want to put filter any conditions
   * 
   * @param entityClass
   * @param query
   * @return
   * @throws SystemException
   */

  public static <T> T scanOneForQuery(Class<T> entityClass, Map<String, Object> query)
      throws SystemException {
    logger.debug(LoggerConstants.LOG_MAXIQAPI + " : >> scanOneForQuery()"
        + ParamUtils.getString(entityClass, query));

    T buildObject = null;
    String sql = MysqlUtility.generateQuery(entityClass, query);
    logger.debug("scanOneForQuery : " + sql);
    try (Connection connection = MysqlConnection.getConnection();
        PreparedStatement stmtWithData =
            MysqlUtility.generateSqlStatmentQueryData(connection.prepareStatement(sql), query);
        ResultSet resultSet = stmtWithData.executeQuery()) {

      while (resultSet.next()) {

        buildObject = MysqlUtility.buildObject(entityClass, resultSet);
      }

    } catch (SQLException | ClassNotFoundException | IOException e) {
      logger.error(LoggerConstants.LOG_MAXIQAPI + " : << scanOneForQuery() {}", e);
      ExceptionsMessanger.throwException(new SystemException(), "ERR_018", e.getMessage());
    }
    logger.debug(LoggerConstants.LOG_MAXIQAPI + " : << scanOneForQuery()"
        + ParamUtils.getString(buildObject));
    return buildObject;

  }
}
