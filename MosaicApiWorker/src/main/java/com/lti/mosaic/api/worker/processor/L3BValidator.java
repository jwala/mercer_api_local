package com.lti.mosaic.api.worker.processor;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;

import in.lnt.constants.Constants;
import in.lnt.exceptions.CustomStatusException;
import in.lnt.validations.PerFormL3BValidations;

/**
 * 
 * @author rushi
 *
 */
public class L3BValidator extends BaseValidator{

  private static final Logger logger = LoggerFactory.getLogger(L3BValidator.class);

  public L3BValidator(Map<String, String> map) {
    super(map);
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.lti.mosaic.api.worker.processor.BaseValidator#processJsonNode(com.fasterxml.jackson.
   * databind.JsonNode)
   */
  @Override
  public JsonNode process(Object inputJsonNode) throws CustomStatusException{
	  logger.info(">> L3BValidator {}", "{}");

	  JsonNode performL3B = null;
	  try {
	   performL3B = PerFormL3BValidations.performL3B((String)inputJsonNode, false);
	  } catch (Exception e) {
			StringWriter stringWriter = new StringWriter();
			e.printStackTrace(new PrintWriter(stringWriter));
			String stackTrace = stringWriter.toString();
	  logger.error("Exception occured in L3BValidator  stak trace is: "
			 + stackTrace +" message:" + e.getMessage()+ " cause:" +e.getCause());
	  throw new CustomStatusException(Constants.HTTPSTATUS_400, e.getMessage());
	  }
	  return performL3B;
  }

}